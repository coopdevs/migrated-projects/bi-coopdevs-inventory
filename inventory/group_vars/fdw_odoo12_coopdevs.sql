

drop foreign table if exists odoo12_account_account;
create FOREIGN TABLE odoo12_account_account("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"currency_id" int4
  ,"code" varchar(64) NOT NULL
  ,"deprecated" bool
  ,"user_type_id" int4 NOT NULL
  ,"internal_type" varchar
  ,"last_time_entries_checked" timestamp
  ,"reconcile" bool
  ,"note" text
  ,"company_id" int4 NOT NULL
  ,"group_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"centralized" bool
  ,"internal_group" varchar
  ,"asset_profile_id" int4
  ,"hide_in_cash_flow" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account');



drop foreign table if exists odoo12_account_account_account_group_rel;


create FOREIGN TABLE odoo12_account_account_account_group_rel("account_group_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_account_group_rel');



drop foreign table if exists odoo12_account_account_account_tag;


create FOREIGN TABLE odoo12_account_account_account_tag("account_account_id" int4 NOT NULL
  ,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_account_tag');



drop foreign table if exists odoo12_account_account_aged_partner_balance_wizard_rel;


create FOREIGN TABLE odoo12_account_account_aged_partner_balance_wizard_rel("aged_partner_balance_wizard_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_aged_partner_balance_wizard_rel');



drop foreign table if exists odoo12_account_account_financial_report;


create FOREIGN TABLE odoo12_account_account_financial_report("report_line_id" int4 NOT NULL
  ,"account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_financial_report');



drop foreign table if exists odoo12_account_account_financial_report_type;


create FOREIGN TABLE odoo12_account_account_financial_report_type("report_id" int4 NOT NULL
  ,"account_type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_financial_report_type');



drop foreign table if exists odoo12_account_account_general_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_general_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_account_open_items_report_wizard_rel;


create FOREIGN TABLE odoo12_account_account_open_items_report_wizard_rel("open_items_report_wizard_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_open_items_report_wizard_rel');



drop foreign table if exists odoo12_account_account_report_aged_partner_balance_rel;


create FOREIGN TABLE odoo12_account_account_report_aged_partner_balance_rel("report_aged_partner_balance_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_aged_partner_balance_rel');



drop foreign table if exists odoo12_account_account_report_general_ledger_rel;


create FOREIGN TABLE odoo12_account_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_general_ledger_rel');



drop foreign table if exists odoo12_account_account_report_open_items_rel;


create FOREIGN TABLE odoo12_account_account_report_open_items_rel("report_open_items_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_open_items_rel');



drop foreign table if exists odoo12_account_account_report_trial_balance_account_rel;


create FOREIGN TABLE odoo12_account_account_report_trial_balance_account_rel("report_trial_balance_account_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_account_rel');



drop foreign table if exists odoo12_account_account_report_trial_balance_rel;


create FOREIGN TABLE odoo12_account_account_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_rel');



drop foreign table if exists odoo12_account_account_tag;


create FOREIGN TABLE odoo12_account_account_tag("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"applicability" varchar NOT NULL
  ,"color" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tag');



drop foreign table if exists odoo12_account_account_tag_account_tax_template_rel;


create FOREIGN TABLE odoo12_account_account_tag_account_tax_template_rel("account_tax_template_id" int4 NOT NULL
  ,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tag_account_tax_template_rel');



drop foreign table if exists odoo12_account_account_tax_default_rel;


create FOREIGN TABLE odoo12_account_account_tax_default_rel("account_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tax_default_rel');



drop foreign table if exists odoo12_account_account_template;


create FOREIGN TABLE odoo12_account_account_template("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"currency_id" int4
  ,"code" varchar(64) NOT NULL
  ,"user_type_id" int4 NOT NULL
  ,"reconcile" bool
  ,"note" text
  ,"nocreate" bool
  ,"chart_template_id" int4
  ,"group_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template');



drop foreign table if exists odoo12_account_account_template_account_tag;


create FOREIGN TABLE odoo12_account_account_template_account_tag("account_account_template_id" int4 NOT NULL
  ,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template_account_tag');



drop foreign table if exists odoo12_account_account_template_tax_rel;


create FOREIGN TABLE odoo12_account_account_template_tax_rel("account_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template_tax_rel');



drop foreign table if exists odoo12_account_account_trial_balance_report_wizard_rel;


create FOREIGN TABLE odoo12_account_account_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
  ,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_trial_balance_report_wizard_rel');



drop foreign table if exists odoo12_account_account_type;


create FOREIGN TABLE odoo12_account_account_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"include_initial_balance" bool
  ,"type" varchar NOT NULL
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"internal_group" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type');



drop foreign table if exists odoo12_account_account_type_general_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_account_type_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"account_account_type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type_general_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_account_type_rel;


create FOREIGN TABLE odoo12_account_account_type_rel("journal_id" int4 NOT NULL
  ,"account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type_rel');



drop foreign table if exists odoo12_account_aged_trial_balance;


create FOREIGN TABLE odoo12_account_aged_trial_balance("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"result_selection" varchar NOT NULL
  ,"period_length" int4 NOT NULL
  ,"date_from" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_aged_trial_balance');



drop foreign table if exists odoo12_account_aged_trial_balance_account_journal_rel;


create FOREIGN TABLE odoo12_account_aged_trial_balance_account_journal_rel("account_aged_trial_balance_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_aged_trial_balance_account_journal_rel');



drop foreign table if exists odoo12_account_analytic_account;


create FOREIGN TABLE odoo12_account_analytic_account("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"code" varchar
  ,"active" bool
  ,"company_id" int4
  ,"partner_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"pricelist_id" int4
  ,"recurring_rule_type" varchar
  ,"recurring_invoicing_type" varchar
  ,"recurring_interval" int4
  ,"journal_id" int4
  ,"contract_template_id" int4
  ,"date_start" date
  ,"date_end" date
  ,"recurring_invoices" bool
  ,"recurring_next_date" date
  ,"user_id" int4
  ,"skip_zero_qty" bool
  ,"invoicing_sales" bool
  ,"message_main_attachment_id" int4
  ,"group_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account');



drop foreign table if exists odoo12_account_analytic_account_general_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_analytic_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account_general_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_analytic_account_report_general_ledger_rel;


create FOREIGN TABLE odoo12_account_analytic_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
  ,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account_report_general_ledger_rel');



drop foreign table if exists odoo12_account_analytic_account_tag_rel;


create FOREIGN TABLE odoo12_account_analytic_account_tag_rel("account_id" int4 NOT NULL
  ,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account_tag_rel');



drop foreign table if exists odoo12_account_analytic_default;


create FOREIGN TABLE odoo12_account_analytic_default("id" int4 NOT NULL
  ,"sequence" int4
  ,"analytic_id" int4
  ,"product_id" int4
  ,"partner_id" int4
  ,"user_id" int4
  ,"company_id" int4
  ,"date_start" date
  ,"date_stop" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_default');



drop foreign table if exists odoo12_account_analytic_default_account_analytic_tag_rel;


create FOREIGN TABLE odoo12_account_analytic_default_account_analytic_tag_rel("account_analytic_default_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_default_account_analytic_tag_rel');



drop foreign table if exists odoo12_account_analytic_distribution;


create FOREIGN TABLE odoo12_account_analytic_distribution("id" int4 NOT NULL
  ,"account_id" int4 NOT NULL
  ,"percentage" float8 NOT NULL
  ,"tag_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_distribution');



drop foreign table if exists odoo12_account_analytic_group;


create FOREIGN TABLE odoo12_account_analytic_group("id" int4 NOT NULL
  ,"parent_path" varchar
  ,"name" varchar NOT NULL
  ,"description" text
  ,"parent_id" int4
  ,"complete_name" varchar
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_group');



drop foreign table if exists odoo12_account_analytic_line;


create FOREIGN TABLE odoo12_account_analytic_line("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date" date NOT NULL
  ,"amount" numeric NOT NULL
  ,"unit_amount" float8
  ,"account_id" int4 NOT NULL
  ,"partner_id" int4
  ,"user_id" int4
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"product_uom_id" int4
  ,"product_id" int4
  ,"general_account_id" int4
  ,"move_id" int4
  ,"code" varchar(8)
  ,"ref" varchar
  ,"currency_id" int4
  ,"amount_currency" numeric
  ,"so_line" int4
  ,"task_id" int4
  ,"project_id" int4
  ,"employee_id" int4
  ,"department_id" int4
  ,"holiday_id" int4
  ,"timesheet_invoice_type" varchar
  ,"timesheet_invoice_id" int4
  ,"timesheet_revenue" numeric
  ,"openupgrade_legacy_12_0_company_id" int4
  ,"group_id" int4
  ,"date_time" timestamp
  ,"lead_id" int4
  ,"ticket_id" int4
  ,"ticket_partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_line');



drop foreign table if exists odoo12_account_analytic_line_tag_rel;


create FOREIGN TABLE odoo12_account_analytic_line_tag_rel("line_id" int4 NOT NULL
  ,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_line_tag_rel');



drop foreign table if exists odoo12_account_analytic_tag;


create FOREIGN TABLE odoo12_account_analytic_tag("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"active_analytic_distribution" bool
  ,"company_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag');



drop foreign table if exists odoo12_account_analytic_tag_account_invoice_line_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_account_invoice_line_rel("account_invoice_line_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_line_rel');



drop foreign table if exists odoo12_account_analytic_tag_account_invoice_tax_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_account_invoice_tax_rel("account_invoice_tax_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_tax_rel');



drop foreign table if exists odoo12_account_analytic_tag_account_move_line_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_account_move_line_rel("account_move_line_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_move_line_rel');



drop foreign table if exists odoo12_account_analytic_tag_account_reconcile_model_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_reconcile_model_rel');



drop foreign table if exists odoo12_account_analytic_tag_contract_line_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_contract_line_rel("contract_line_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_contract_line_rel');



drop foreign table if exists odoo12_account_analytic_tag_general_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_general_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_analytic_tag_hr_expense_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_hr_expense_rel("hr_expense_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_hr_expense_rel');



drop foreign table if exists odoo12_account_analytic_tag_mis_budget_by_account_item_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_mis_budget_by_account_item_rel("mis_budget_by_account_item_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_by_account_item_rel');



drop foreign table if exists odoo12_account_analytic_tag_mis_budget_item_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_mis_budget_item_rel("mis_budget_item_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_item_rel');



drop foreign table if exists odoo12_account_analytic_tag_mis_report_instance_period_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_mis_report_instance_period_rel("mis_report_instance_period_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_period_rel');



drop foreign table if exists odoo12_account_analytic_tag_mis_report_instance_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_mis_report_instance_rel("mis_report_instance_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_rel');



drop foreign table if exists odoo12_account_analytic_tag_purchase_order_line_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_purchase_order_line_rel("purchase_order_line_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_purchase_order_line_rel');



drop foreign table if exists odoo12_account_analytic_tag_report_general_ledger_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_report_general_ledger_rel');



drop foreign table if exists odoo12_account_analytic_tag_sale_order_line_rel;


create FOREIGN TABLE odoo12_account_analytic_tag_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
  ,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_sale_order_line_rel');



drop foreign table if exists odoo12_account_asset;


create FOREIGN TABLE odoo12_account_asset("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"code" varchar(32)
  ,"company_currency_id" int4
  ,"company_id" int4 NOT NULL
  ,"note" text
  ,"profile_id" int4 NOT NULL
  ,"date_start" date NOT NULL
  ,"state" varchar NOT NULL
  ,"active" bool
  ,"partner_id" int4
  ,"method" varchar NOT NULL
  ,"method_number" int4
  ,"openupgrade_legacy_12_0_method_period" int4
  ,"method_end" date
  ,"method_progress_factor" numeric
  ,"method_time" varchar NOT NULL
  ,"prorata" bool
  ,"salvage_value" numeric
  ,"invoice_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"move_end_period" bool
  ,"method_percentage" numeric
  ,"annual_percentage" numeric
  ,"start_depreciation_date" date
  ,"openupgrade_legacy_12_0_method_number" int4
  ,"openupgrade_legacy_12_0_method_time" varchar
  ,"purchase_value" float8 NOT NULL
  ,"depreciation_base" numeric
  ,"value_residual" numeric
  ,"value_depreciated" numeric
  ,"date_remove" date
  ,"method_period" varchar NOT NULL
  ,"days_calc" bool
  ,"use_leap_years" bool
  ,"account_analytic_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset');



drop foreign table if exists odoo12_account_asset_compute;


create FOREIGN TABLE odoo12_account_asset_compute("id" int4 NOT NULL
  ,"date_end" date NOT NULL
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_compute');



drop foreign table if exists odoo12_account_asset_group;


create FOREIGN TABLE odoo12_account_asset_group("id" int4 NOT NULL
  ,"parent_path" varchar
  ,"name" varchar(64) NOT NULL
  ,"code" varchar
  ,"company_id" int4 NOT NULL
  ,"parent_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_group');



drop foreign table if exists odoo12_account_asset_group_rel;


create FOREIGN TABLE odoo12_account_asset_group_rel("asset_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_group_rel');



drop foreign table if exists odoo12_account_asset_line;


create FOREIGN TABLE odoo12_account_asset_line("id" int4 NOT NULL
  ,"name" varchar
  ,"sequence" int4
  ,"asset_id" int4 NOT NULL
  ,"amount" numeric NOT NULL
  ,"remaining_value" numeric
  ,"depreciated_value" numeric
  ,"line_date" date NOT NULL
  ,"move_id" int4
  ,"move_check" bool
  ,"move_posted_check" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"previous_id" int4
  ,"line_days" int4
  ,"type" varchar
  ,"init_entry" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_line');



drop foreign table if exists odoo12_account_asset_profile;


create FOREIGN TABLE odoo12_account_asset_profile("id" int4 NOT NULL
  ,"active" bool
  ,"name" varchar NOT NULL
  ,"account_analytic_id" int4
  ,"account_asset_id" int4 NOT NULL
  ,"account_depreciation_id" int4 NOT NULL
  ,"account_expense_depreciation_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"method" varchar NOT NULL
  ,"method_number" int4
  ,"openupgrade_legacy_12_0_method_period" int4
  ,"method_progress_factor" numeric
  ,"method_time" varchar NOT NULL
  ,"method_end" date
  ,"prorata" bool
  ,"open_asset" bool
  ,"group_entries" bool
  ,"type" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"method_percentage" numeric
  ,"openupgrade_legacy_12_0_method_number" int4
  ,"openupgrade_legacy_12_0_method_time" varchar
  ,"note" text
  ,"account_plus_value_id" int4
  ,"account_min_value_id" int4
  ,"account_residual_value_id" int4
  ,"method_period" varchar NOT NULL
  ,"days_calc" bool
  ,"use_leap_years" bool
  ,"asset_product_item" bool
  ,"annual_percentage" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_profile');



drop foreign table if exists odoo12_account_asset_profile_group_rel;


create FOREIGN TABLE odoo12_account_asset_profile_group_rel("profile_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_profile_group_rel');



drop foreign table if exists odoo12_account_asset_recompute_trigger;


create FOREIGN TABLE odoo12_account_asset_recompute_trigger("id" int4 NOT NULL
  ,"reason" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"date_trigger" timestamp
  ,"date_completed" timestamp
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_recompute_trigger');



drop foreign table if exists odoo12_account_asset_remove;


create FOREIGN TABLE odoo12_account_asset_remove("id" int4 NOT NULL
  ,"date_remove" date NOT NULL
  ,"force_date" date
  ,"sale_value" float8
  ,"account_sale_id" int4
  ,"account_plus_value_id" int4
  ,"account_min_value_id" int4
  ,"account_residual_value_id" int4
  ,"posting_regime" varchar NOT NULL
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_remove');



drop foreign table if exists odoo12_account_balance_report;


create FOREIGN TABLE odoo12_account_balance_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"display_account" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_balance_report');



drop foreign table if exists odoo12_account_balance_report_journal_rel;


create FOREIGN TABLE odoo12_account_balance_report_journal_rel("account_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_balance_report_journal_rel');



drop foreign table if exists odoo12_account_bank_accounts_wizard;


create FOREIGN TABLE odoo12_account_bank_accounts_wizard("id" int4 NOT NULL
  ,"acc_name" varchar NOT NULL
  ,"bank_account_id" int4 NOT NULL
  ,"currency_id" int4
  ,"account_type" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_accounts_wizard');



drop foreign table if exists odoo12_account_bank_statement;


create FOREIGN TABLE odoo12_account_bank_statement("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar
  ,"reference" varchar
  ,"date" date NOT NULL
  ,"date_done" timestamp
  ,"balance_start" numeric
  ,"balance_end_real" numeric
  ,"state" varchar NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"company_id" int4
  ,"total_entry_encoding" numeric
  ,"balance_end" numeric
  ,"difference" numeric
  ,"user_id" int4
  ,"cashbox_start_id" int4
  ,"cashbox_end_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4
  ,"accounting_date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement');



drop foreign table if exists odoo12_account_bank_statement_cashbox;


create FOREIGN TABLE odoo12_account_bank_statement_cashbox("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_cashbox');



drop foreign table if exists odoo12_account_bank_statement_closebalance;


create FOREIGN TABLE odoo12_account_bank_statement_closebalance("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_closebalance');



drop foreign table if exists odoo12_account_bank_statement_import;


create FOREIGN TABLE odoo12_account_bank_statement_import("id" int4 NOT NULL
  ,"data_file" bytea NOT NULL
  ,"filename" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_import');



drop foreign table if exists odoo12_account_bank_statement_import_journal_creation;


create FOREIGN TABLE odoo12_account_bank_statement_import_journal_creation("id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_import_journal_creation');



drop foreign table if exists odoo12_account_bank_statement_line;


create FOREIGN TABLE odoo12_account_bank_statement_line("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date" date NOT NULL
  ,"amount" numeric
  ,"partner_id" int4
  ,"bank_account_id" int4
  ,"account_id" int4
  ,"statement_id" int4 NOT NULL
  ,"journal_id" int4
  ,"partner_name" varchar
  ,"ref" varchar
  ,"note" text
  ,"sequence" int4
  ,"company_id" int4
  ,"amount_currency" numeric
  ,"currency_id" int4
  ,"move_name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"unique_import_id" varchar
  ,"account_number" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_line');



drop foreign table if exists odoo12_account_banking_mandate;


create FOREIGN TABLE odoo12_account_banking_mandate("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"format" varchar NOT NULL
  ,"type" varchar
  ,"partner_bank_id" int4
  ,"partner_id" int4
  ,"company_id" int4 NOT NULL
  ,"unique_mandate_reference" varchar
  ,"signature_date" date
  ,"last_debit_date" date
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"recurrent_sequence_type" varchar
  ,"scheme" varchar
  ,"display_name" varchar
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_banking_mandate');



drop foreign table if exists odoo12_account_cash_rounding;


create FOREIGN TABLE odoo12_account_cash_rounding("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"rounding" float8 NOT NULL
  ,"strategy" varchar NOT NULL
  ,"account_id" int4
  ,"rounding_method" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_cash_rounding');



drop foreign table if exists odoo12_account_cashbox_line;


create FOREIGN TABLE odoo12_account_cashbox_line("id" int4 NOT NULL
  ,"coin_value" numeric NOT NULL
  ,"number" int4
  ,"cashbox_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_cashbox_line');



drop foreign table if exists odoo12_account_chart_template;


create FOREIGN TABLE odoo12_account_chart_template("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"openupgrade_legacy_12_0_company_id" int4
  ,"parent_id" int4
  ,"code_digits" int4 NOT NULL
  ,"visible" bool
  ,"currency_id" int4 NOT NULL
  ,"use_anglo_saxon" bool
  ,"complete_tax_set" bool
  ,"bank_account_code_prefix" varchar NOT NULL
  ,"cash_account_code_prefix" varchar NOT NULL
  ,"openupgrade_legacy_12_0_transfer_account_id" int4
  ,"income_currency_exchange_account_id" int4
  ,"expense_currency_exchange_account_id" int4
  ,"property_account_receivable_id" int4
  ,"property_account_payable_id" int4
  ,"property_account_expense_categ_id" int4
  ,"property_account_income_categ_id" int4
  ,"property_account_expense_id" int4
  ,"property_account_income_id" int4
  ,"property_stock_account_input_categ_id" int4
  ,"property_stock_account_output_categ_id" int4
  ,"property_stock_valuation_account_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"transfer_account_code_prefix" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_chart_template');



drop foreign table if exists odoo12_account_common_account_report;


create FOREIGN TABLE odoo12_account_common_account_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"display_account" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_account_report');



drop foreign table if exists odoo12_account_common_account_report_account_journal_rel;


create FOREIGN TABLE odoo12_account_common_account_report_account_journal_rel("account_common_account_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_account_report_account_journal_rel');



drop foreign table if exists odoo12_account_common_journal_report;


create FOREIGN TABLE odoo12_account_common_journal_report("id" int4 NOT NULL
  ,"amount_currency" bool
  ,"company_id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_journal_report');



drop foreign table if exists odoo12_account_common_journal_report_account_journal_rel;


create FOREIGN TABLE odoo12_account_common_journal_report_account_journal_rel("account_common_journal_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_journal_report_account_journal_rel');



drop foreign table if exists odoo12_account_common_partner_report;


create FOREIGN TABLE odoo12_account_common_partner_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"result_selection" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_partner_report');



drop foreign table if exists odoo12_account_common_partner_report_account_journal_rel;


create FOREIGN TABLE odoo12_account_common_partner_report_account_journal_rel("account_common_partner_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_partner_report_account_journal_rel');



drop foreign table if exists odoo12_account_common_report;


create FOREIGN TABLE odoo12_account_common_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_report');



drop foreign table if exists odoo12_account_common_report_account_journal_rel;


create FOREIGN TABLE odoo12_account_common_report_account_journal_rel("account_common_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_report_account_journal_rel');



drop foreign table if exists odoo12_account_financial_report;


create FOREIGN TABLE odoo12_account_financial_report("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"parent_id" int4
  ,"sequence" int4
  ,"level" int4
  ,"type" varchar
  ,"account_report_id" int4
  ,"sign" int4 NOT NULL
  ,"display_detail" varchar
  ,"style_overwrite" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_financial_report');



drop foreign table if exists odoo12_account_financial_year_op;


create FOREIGN TABLE odoo12_account_financial_year_op("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_financial_year_op');



drop foreign table if exists odoo12_account_fiscal_position;


create FOREIGN TABLE odoo12_account_fiscal_position("id" int4 NOT NULL
  ,"sequence" int4
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"note" text
  ,"auto_apply" bool
  ,"vat_required" bool
  ,"country_id" int4
  ,"country_group_id" int4
  ,"zip_from" int4
  ,"zip_to" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position');



drop foreign table if exists odoo12_account_fiscal_position_account;


create FOREIGN TABLE odoo12_account_fiscal_position_account("id" int4 NOT NULL
  ,"position_id" int4 NOT NULL
  ,"account_src_id" int4 NOT NULL
  ,"account_dest_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account');



drop foreign table if exists odoo12_account_fiscal_position_account_template;


create FOREIGN TABLE odoo12_account_fiscal_position_account_template("id" int4 NOT NULL
  ,"position_id" int4 NOT NULL
  ,"account_src_id" int4 NOT NULL
  ,"account_dest_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account_template');



drop foreign table if exists odoo12_account_fiscal_position_res_country_state_rel;


create FOREIGN TABLE odoo12_account_fiscal_position_res_country_state_rel("account_fiscal_position_id" int4 NOT NULL
  ,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_res_country_state_rel');



drop foreign table if exists odoo12_account_fiscal_position_tax;


create FOREIGN TABLE odoo12_account_fiscal_position_tax("id" int4 NOT NULL
  ,"position_id" int4 NOT NULL
  ,"tax_src_id" int4 NOT NULL
  ,"tax_dest_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax');



drop foreign table if exists odoo12_account_fiscal_position_tax_template;


create FOREIGN TABLE odoo12_account_fiscal_position_tax_template("id" int4 NOT NULL
  ,"position_id" int4 NOT NULL
  ,"tax_src_id" int4 NOT NULL
  ,"tax_dest_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax_template');



drop foreign table if exists odoo12_account_fiscal_position_template;


create FOREIGN TABLE odoo12_account_fiscal_position_template("id" int4 NOT NULL
  ,"sequence" int4
  ,"name" varchar NOT NULL
  ,"chart_template_id" int4 NOT NULL
  ,"note" text
  ,"auto_apply" bool
  ,"vat_required" bool
  ,"country_id" int4
  ,"country_group_id" int4
  ,"zip_from" int4
  ,"zip_to" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template');



drop foreign table if exists odoo12_account_fiscal_position_template_res_country_state_rel;


create FOREIGN TABLE odoo12_account_fiscal_position_template_res_country_state_rel("account_fiscal_position_template_id" int4 NOT NULL
  ,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template_res_country_state_rel');



drop foreign table if exists odoo12_account_fiscal_year;


create FOREIGN TABLE odoo12_account_fiscal_year("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_year');



drop foreign table if exists odoo12_account_full_reconcile;


create FOREIGN TABLE odoo12_account_full_reconcile("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"exchange_move_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_full_reconcile');



drop foreign table if exists odoo12_account_group;


create FOREIGN TABLE odoo12_account_group("id" int4 NOT NULL
  ,"parent_left" int4
  ,"parent_right" int4
  ,"parent_id" int4
  ,"name" varchar NOT NULL
  ,"code_prefix" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"parent_path" varchar
  ,"level" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_group');



drop foreign table if exists odoo12_account_incoterms;


create FOREIGN TABLE odoo12_account_incoterms("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar(3) NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_incoterms');



drop foreign table if exists odoo12_account_invoice;
create FOREIGN TABLE odoo12_account_invoice("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar
  ,"origin" varchar
  ,"type" varchar
  ,"access_token" varchar
  ,"refund_invoice_id" int4
  ,"number" varchar
  ,"move_name" varchar
  ,"reference" varchar
  ,"openupgrade_legacy_12_0_reference_type" varchar
  ,"comment" text
  ,"state" varchar
  ,"sent" bool
  ,"date_invoice" date
  ,"date_due" date
  ,"partner_id" int4
  ,"payment_term_id" int4
  ,"date" date
  ,"account_id" int4
  ,"move_id" int4
  ,"amount_untaxed" numeric
  ,"amount_untaxed_signed" numeric
  ,"amount_tax" numeric
  ,"amount_total" numeric
  ,"amount_total_signed" numeric
  ,"amount_total_company_signed" numeric
  ,"currency_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"reconciled" bool
  ,"partner_bank_id" int4
  ,"residual" numeric
  ,"residual_signed" numeric
  ,"residual_company_signed" numeric
  ,"user_id" int4
  ,"fiscal_position_id" int4
  ,"commercial_partner_id" int4
  ,"cash_rounding_id" int4
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"team_id" int4
  ,"partner_shipping_id" int4
  ,"old_contract_id" int4
  ,"payment_mode_id" int4
  ,"purchase_id" int4
  ,"invoice_number" varchar
  ,"campaign_id" int4
  ,"source_id" int4
  ,"medium_id" int4
  ,"incoterms_id" int4
  ,"mandate_id" int4
  ,"eu_triangular_deal" bool
  ,"message_main_attachment_id" int4
  ,"vendor_bill_id" int4
  ,"incoterm_id" int4
  ,"source_email" varchar
  ,"vendor_display_name" varchar
  ,"vendor_bill_purchase_id" int4
  ,"reference_type" varchar NOT NULL
  ,"not_in_mod347" bool
  ,"returned_payment" bool
)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice');



drop foreign table if exists odoo12_account_invoice_account_invoice_send_rel;


create FOREIGN TABLE odoo12_account_invoice_account_invoice_send_rel("account_invoice_send_id" int4 NOT NULL
  ,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_invoice_send_rel');



drop foreign table if exists odoo12_account_invoice_account_move_line_rel;


create FOREIGN TABLE odoo12_account_invoice_account_move_line_rel("account_invoice_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_move_line_rel');



drop foreign table if exists odoo12_account_invoice_account_register_payments_rel;


create FOREIGN TABLE odoo12_account_invoice_account_register_payments_rel("account_register_payments_id" int4 NOT NULL
  ,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_register_payments_rel');



drop foreign table if exists odoo12_account_invoice_confirm;


create FOREIGN TABLE odoo12_account_invoice_confirm("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_confirm');



drop foreign table if exists odoo12_account_invoice_import_wizard;


create FOREIGN TABLE odoo12_account_invoice_import_wizard("id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard');



drop foreign table if exists odoo12_account_invoice_import_wizard_ir_attachment_rel;


create FOREIGN TABLE odoo12_account_invoice_import_wizard_ir_attachment_rel("account_invoice_import_wizard_id" int4 NOT NULL
  ,"ir_attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard_ir_attachment_rel');



drop foreign table if exists odoo12_account_invoice_line;


create FOREIGN TABLE odoo12_account_invoice_line("id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"origin" varchar
  ,"sequence" int4
  ,"invoice_id" int4
  ,"uom_id" int4
  ,"product_id" int4
  ,"account_id" int4
  ,"price_unit" numeric NOT NULL
  ,"price_subtotal" numeric
  ,"price_total" numeric
  ,"price_subtotal_signed" numeric
  ,"quantity" numeric NOT NULL
  ,"discount" numeric
  ,"account_analytic_id" int4
  ,"company_id" int4
  ,"partner_id" int4
  ,"currency_id" int4
  ,"is_rounding_line" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"layout_category_id" int4
  ,"layout_category_sequence" int4
  ,"asset_profile_id" int4
  ,"asset_start_date" date
  ,"asset_end_date" date
  ,"asset_mrr" numeric
  ,"purchase_line_id" int4
  ,"display_type" varchar
  ,"asset_id" int4
  ,"contract_line_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_line');



drop foreign table if exists odoo12_account_invoice_line_tax;


create FOREIGN TABLE odoo12_account_invoice_line_tax("invoice_line_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_line_tax');



drop foreign table if exists odoo12_account_invoice_payment_line_multi;


create FOREIGN TABLE odoo12_account_invoice_payment_line_multi("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_payment_line_multi');



drop foreign table if exists odoo12_account_invoice_payment_rel;


create FOREIGN TABLE odoo12_account_invoice_payment_rel("payment_id" int4 NOT NULL
  ,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_payment_rel');



drop foreign table if exists odoo12_account_invoice_purchase_order_rel;


create FOREIGN TABLE odoo12_account_invoice_purchase_order_rel("purchase_order_id" int4 NOT NULL
  ,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_purchase_order_rel');



drop foreign table if exists odoo12_account_invoice_refund;


create FOREIGN TABLE odoo12_account_invoice_refund("id" int4 NOT NULL
  ,"date_invoice" date NOT NULL
  ,"date" date
  ,"description" varchar NOT NULL
  ,"filter_refund" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_refund');



drop foreign table if exists odoo12_account_invoice_report;


create FOREIGN TABLE odoo12_account_invoice_report("id" int4
  ,"number" varchar
  ,"date" date
  ,"product_id" int4
  ,"partner_id" int4
  ,"country_id" int4
  ,"account_analytic_id" int4
  ,"payment_term_id" int4
  ,"uom_name" varchar
  ,"currency_id" int4
  ,"journal_id" int4
  ,"fiscal_position_id" int4
  ,"user_id" int4
  ,"company_id" int4
  ,"nbr" int4
  ,"invoice_id" int4
  ,"type" varchar
  ,"state" varchar
  ,"categ_id" int4
  ,"date_due" date
  ,"account_id" int4
  ,"account_line_id" int4
  ,"partner_bank_id" int4
  ,"product_qty" numeric
  ,"price_total" numeric
  ,"price_average" numeric
  ,"amount_total" numeric
  ,"currency_rate" numeric
  ,"residual" numeric
  ,"commercial_partner_id" int4
  ,"team_id" int4
  ,"release_capital_request" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_report');



drop foreign table if exists odoo12_account_invoice_send;


create FOREIGN TABLE odoo12_account_invoice_send("id" int4 NOT NULL
  ,"is_email" bool
  ,"is_print" bool
  ,"printed" bool
  ,"composer_id" int4 NOT NULL
  ,"template_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_send');



drop foreign table if exists odoo12_account_invoice_tax;


create FOREIGN TABLE odoo12_account_invoice_tax("id" int4 NOT NULL
  ,"invoice_id" int4
  ,"name" varchar NOT NULL
  ,"tax_id" int4
  ,"account_id" int4 NOT NULL
  ,"account_analytic_id" int4
  ,"amount" numeric
  ,"amount_rounding" numeric
  ,"manual" bool
  ,"sequence" int4
  ,"company_id" int4
  ,"currency_id" int4
  ,"base" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_tax');



drop foreign table if exists odoo12_account_invoice_transaction_rel;


create FOREIGN TABLE odoo12_account_invoice_transaction_rel("transaction_id" int4 NOT NULL
  ,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_transaction_rel');



drop foreign table if exists odoo12_account_journal;
create FOREIGN TABLE odoo12_account_journal("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar(5) NOT NULL
  ,"active" bool
  ,"type" varchar NOT NULL
  ,"default_credit_account_id" int4
  ,"default_debit_account_id" int4
  ,"update_posted" bool
  ,"group_invoice_lines" bool
  ,"sequence_id" int4 NOT NULL
  ,"refund_sequence_id" int4
  ,"sequence" int4
  ,"currency_id" int4
  ,"company_id" int4 NOT NULL
  ,"refund_sequence" bool
  ,"at_least_one_inbound" bool
  ,"at_least_one_outbound" bool
  ,"profit_account_id" int4
  ,"loss_account_id" int4
  ,"bank_account_id" int4
  ,"bank_statements_source" varchar
  ,"show_on_dashboard" bool
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"n43_date_type" varchar
  ,"invoice_sequence_id" int4
  ,"refund_inv_sequence_id" int4
  ,"openupgrade_legacy_12_0_bank_statements_source" varchar
  ,"post_at_bank_rec" bool
  ,"alias_id" int4
  ,"inbound_payment_order_only" bool
  ,"outbound_payment_order_only" bool
  ,"default_expense_account_id" int4
  ,"default_expense_partner_id" int4
  ,"return_auto_reconcile" bool
  ,"get_cooperator_payment" bool
  ,"get_general_payment" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal');



drop foreign table if exists odoo12_account_journal_account_payment_line_create_rel;


create FOREIGN TABLE odoo12_account_journal_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_line_create_rel');



drop foreign table if exists odoo12_account_journal_account_payment_mode_rel;


create FOREIGN TABLE odoo12_account_journal_account_payment_mode_rel("account_payment_mode_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_mode_rel');



drop foreign table if exists odoo12_account_journal_account_print_journal_rel;


create FOREIGN TABLE odoo12_account_journal_account_print_journal_rel("account_print_journal_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_print_journal_rel');



drop foreign table if exists odoo12_account_journal_account_reconcile_model_rel;


create FOREIGN TABLE odoo12_account_journal_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_rel');



drop foreign table if exists odoo12_account_journal_account_reconcile_model_template_rel;


create FOREIGN TABLE odoo12_account_journal_account_reconcile_model_template_rel("account_reconcile_model_template_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_template_rel');



drop foreign table if exists odoo12_account_journal_account_report_partner_ledger_rel;


create FOREIGN TABLE odoo12_account_journal_account_report_partner_ledger_rel("account_report_partner_ledger_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_report_partner_ledger_rel');



drop foreign table if exists odoo12_account_journal_account_tax_report_rel;


create FOREIGN TABLE odoo12_account_journal_account_tax_report_rel("account_tax_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_tax_report_rel');



drop foreign table if exists odoo12_account_journal_accounting_report_rel;


create FOREIGN TABLE odoo12_account_journal_accounting_report_rel("accounting_report_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_accounting_report_rel');



drop foreign table if exists odoo12_account_journal_general_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_journal_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_general_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_journal_inbound_payment_method_rel;


create FOREIGN TABLE odoo12_account_journal_inbound_payment_method_rel("journal_id" int4 NOT NULL
  ,"inbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_inbound_payment_method_rel');



drop foreign table if exists odoo12_account_journal_journal_ledger_report_wizard_rel;


create FOREIGN TABLE odoo12_account_journal_journal_ledger_report_wizard_rel("journal_ledger_report_wizard_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_journal_ledger_report_wizard_rel');



drop foreign table if exists odoo12_account_journal_outbound_payment_method_rel;


create FOREIGN TABLE odoo12_account_journal_outbound_payment_method_rel("journal_id" int4 NOT NULL
  ,"outbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_outbound_payment_method_rel');



drop foreign table if exists odoo12_account_journal_report_general_ledger_rel;


create FOREIGN TABLE odoo12_account_journal_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_general_ledger_rel');



drop foreign table if exists odoo12_account_journal_report_journal_ledger_rel;


create FOREIGN TABLE odoo12_account_journal_report_journal_ledger_rel("report_journal_ledger_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_journal_ledger_rel');



drop foreign table if exists odoo12_account_journal_report_trial_balance_rel;


create FOREIGN TABLE odoo12_account_journal_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_trial_balance_rel');



drop foreign table if exists odoo12_account_journal_trial_balance_report_wizard_rel;


create FOREIGN TABLE odoo12_account_journal_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
  ,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_trial_balance_report_wizard_rel');



drop foreign table if exists odoo12_account_journal_type_rel;


create FOREIGN TABLE odoo12_account_journal_type_rel("journal_id" int4 NOT NULL
  ,"type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_type_rel');



drop foreign table if exists odoo12_account_move;


create FOREIGN TABLE odoo12_account_move("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"ref" varchar
  ,"date" date NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"currency_id" int4
  ,"state" varchar NOT NULL
  ,"partner_id" int4
  ,"amount" numeric
  ,"narration" text
  ,"company_id" int4
  ,"matched_percentage" numeric
  ,"tax_cash_basis_rec_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"move_type" varchar
  ,"stock_move_id" int4
  ,"payment_order_id" int4
  ,"auto_reverse" bool
  ,"reverse_date" date
  ,"reverse_entry_id" int4
  ,"message_main_attachment_id" int4
  ,"not_in_mod347" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move');



drop foreign table if exists odoo12_account_move_line;


create FOREIGN TABLE odoo12_account_move_line("id" int4 NOT NULL
  ,"name" varchar
  ,"quantity" numeric
  ,"product_uom_id" int4
  ,"product_id" int4
  ,"debit" numeric
  ,"credit" numeric
  ,"balance" numeric
  ,"debit_cash_basis" numeric
  ,"credit_cash_basis" numeric
  ,"balance_cash_basis" numeric
  ,"amount_currency" numeric
  ,"company_currency_id" int4
  ,"currency_id" int4
  ,"amount_residual" numeric
  ,"amount_residual_currency" numeric
  ,"tax_base_amount" numeric
  ,"account_id" int4 NOT NULL
  ,"move_id" int4 NOT NULL
  ,"ref" varchar
  ,"payment_id" int4
  ,"statement_line_id" int4
  ,"statement_id" int4
  ,"reconciled" bool
  ,"full_reconcile_id" int4
  ,"journal_id" int4
  ,"blocked" bool
  ,"date_maturity" date NOT NULL
  ,"date" date
  ,"tax_line_id" int4
  ,"analytic_account_id" int4
  ,"company_id" int4
  ,"invoice_id" int4
  ,"partner_id" int4
  ,"user_type_id" int4
  ,"tax_exigible" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"stored_invoice_id" int4
  ,"invoice_user_id" int4
  ,"expense_id" int4
  ,"payment_mode_id" int4
  ,"partner_bank_id" int4
  ,"bank_payment_line_id" int4
  ,"mandate_id" int4
  ,"l10n_es_aeat_349_operation_key" varchar
  ,"asset_profile_id" int4
  ,"asset_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line');



drop foreign table if exists odoo12_account_move_line_account_payment_line_create_rel;


create FOREIGN TABLE odoo12_account_move_line_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_account_payment_line_create_rel');



drop foreign table if exists odoo12_account_move_line_account_tax_rel;


create FOREIGN TABLE odoo12_account_move_line_account_tax_rel("account_move_line_id" int4 NOT NULL
  ,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_account_tax_rel');



drop foreign table if exists odoo12_account_move_line_l10n_es_aeat_mod347_partner_record_rel;


create FOREIGN TABLE odoo12_account_move_line_l10n_es_aeat_mod347_partner_record_rel("l10n_es_aeat_mod347_partner_record_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_mod347_partner_record_rel');



drop foreign table if exists odoo12_account_move_line_l10n_es_aeat_tax_line_rel;


create FOREIGN TABLE odoo12_account_move_line_l10n_es_aeat_tax_line_rel("l10n_es_aeat_tax_line_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_tax_line_rel');



drop foreign table if exists odoo12_account_move_line_l10n_es_vat_book_line_tax_rel;


create FOREIGN TABLE odoo12_account_move_line_l10n_es_vat_book_line_tax_rel("l10n_es_vat_book_line_tax_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_vat_book_line_tax_rel');



drop foreign table if exists odoo12_account_move_line_payment_return_line_rel;


create FOREIGN TABLE odoo12_account_move_line_payment_return_line_rel("payment_return_line_id" int4 NOT NULL
  ,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_payment_return_line_rel');



drop foreign table if exists odoo12_account_move_line_reconcile;


create FOREIGN TABLE odoo12_account_move_line_reconcile("id" int4 NOT NULL
  ,"trans_nbr" int4
  ,"credit" numeric
  ,"debit" numeric
  ,"writeoff" numeric
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_reconcile');



drop foreign table if exists odoo12_account_move_line_reconcile_writeoff;


create FOREIGN TABLE odoo12_account_move_line_reconcile_writeoff("id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"writeoff_acc_id" int4 NOT NULL
  ,"date_p" date
  ,"comment" varchar NOT NULL
  ,"analytic_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_reconcile_writeoff');



drop foreign table if exists odoo12_account_move_reversal;


create FOREIGN TABLE odoo12_account_move_reversal("id" int4 NOT NULL
  ,"date" date NOT NULL
  ,"journal_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_reversal');



drop foreign table if exists odoo12_account_multicompany_bank_wiz;


create FOREIGN TABLE odoo12_account_multicompany_bank_wiz("id" int4 NOT NULL
  ,"acc_number" varchar NOT NULL
  ,"sanitized_acc_number" varchar
  ,"partner_id" int4
  ,"bank_id" int4
  ,"sequence" int4
  ,"currency_id" int4
  ,"company_id" int4
  ,"wizard_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"acc_type" varchar
  ,"acc_holder_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_multicompany_bank_wiz');



drop foreign table if exists odoo12_account_multicompany_easy_creation_wiz;


create FOREIGN TABLE odoo12_account_multicompany_easy_creation_wiz("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"chart_template_id" int4
  ,"accounts_code_digits" int4
  ,"new_company_id" int4
  ,"smart_search_product_tax" bool
  ,"update_default_taxes" bool
  ,"default_sale_tax_id" int4
  ,"force_sale_tax" bool
  ,"default_purchase_tax_id" int4
  ,"force_purchase_tax" bool
  ,"smart_search_specific_account" bool
  ,"smart_search_fiscal_position" bool
  ,"update_default_accounts" bool
  ,"account_receivable_id" int4
  ,"account_payable_id" int4
  ,"account_income_categ_id" int4
  ,"account_expense_categ_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_multicompany_easy_creation_wiz');



drop foreign table if exists odoo12_account_multicompany_easy_creation_wiz_ir_sequence_rel;


create FOREIGN TABLE odoo12_account_multicompany_easy_creation_wiz_ir_sequence_rel("account_multicompany_easy_creation_wiz_id" int4 NOT NULL
  ,"ir_sequence_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_multicompany_easy_creation_wiz_ir_sequence_rel');



drop foreign table if exists odoo12_account_multicompany_easy_creation_wiz_res_users_rel;


create FOREIGN TABLE odoo12_account_multicompany_easy_creation_wiz_res_users_rel("account_multicompany_easy_creation_wiz_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_multicompany_easy_creation_wiz_res_users_rel');



drop foreign table if exists odoo12_account_opening;
create FOREIGN TABLE odoo12_account_opening("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_opening');



drop foreign table if exists odoo12_account_partial_reconcile;


create FOREIGN TABLE odoo12_account_partial_reconcile("id" int4 NOT NULL
  ,"debit_move_id" int4 NOT NULL
  ,"credit_move_id" int4 NOT NULL
  ,"amount" numeric
  ,"amount_currency" numeric
  ,"currency_id" int4
  ,"company_id" int4
  ,"full_reconcile_id" int4
  ,"max_date" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_partial_reconcile');



drop foreign table if exists odoo12_account_partial_reconcile_account_move_line_rel;


create FOREIGN TABLE odoo12_account_partial_reconcile_account_move_line_rel("partial_reconcile_id" int4 NOT NULL
  ,"move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_partial_reconcile_account_move_line_rel');



drop foreign table if exists odoo12_account_payment;
create FOREIGN TABLE odoo12_account_payment("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"payment_method_id" int4 NOT NULL
  ,"partner_type" varchar
  ,"partner_id" int4
  ,"amount" numeric NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"payment_date" date NOT NULL
  ,"communication" varchar
  ,"journal_id" int4 NOT NULL
  ,"company_id" int4
  ,"name" varchar
  ,"state" varchar
  ,"payment_type" varchar NOT NULL
  ,"payment_reference" varchar
  ,"move_name" varchar
  ,"destination_journal_id" int4
  ,"payment_difference_handling" varchar
  ,"writeoff_account_id" int4
  ,"writeoff_label" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"payment_transaction_id" int4
  ,"payment_token_id" int4
  ,"message_main_attachment_id" int4
  ,"multi" bool
  ,"partner_bank_account_id" int4
  ,"expense_sheet_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment');



drop foreign table if exists odoo12_account_payment_line;


create FOREIGN TABLE odoo12_account_payment_line("id" int4 NOT NULL
  ,"name" varchar
  ,"order_id" int4
  ,"company_id" int4
  ,"company_currency_id" int4
  ,"payment_type" varchar
  ,"state" varchar
  ,"move_line_id" int4
  ,"currency_id" int4 NOT NULL
  ,"amount_currency" numeric
  ,"partner_id" int4 NOT NULL
  ,"partner_bank_id" int4
  ,"date" date
  ,"communication" varchar NOT NULL
  ,"communication_type" varchar NOT NULL
  ,"bank_line_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"mandate_id" int4
  ,"priority" varchar
  ,"local_instrument" varchar
  ,"category_purpose" varchar
  ,"purpose" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line');



drop foreign table if exists odoo12_account_payment_line_create;


create FOREIGN TABLE odoo12_account_payment_line_create("id" int4 NOT NULL
  ,"order_id" int4
  ,"target_move" varchar
  ,"allow_blocked" bool
  ,"invoice" bool
  ,"date_type" varchar NOT NULL
  ,"due_date" date
  ,"move_date" date
  ,"payment_mode" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line_create');



drop foreign table if exists odoo12_account_payment_line_create_res_partner_rel;


create FOREIGN TABLE odoo12_account_payment_line_create_res_partner_rel("account_payment_line_create_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line_create_res_partner_rel');



drop foreign table if exists odoo12_account_payment_method;


create FOREIGN TABLE odoo12_account_payment_method("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"payment_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"active" bool
  ,"bank_account_required" bool
  ,"mandate_required" bool
  ,"pain_version" varchar
  ,"convert_to_ascii" bool
  ,"payment_order_only" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_method');



drop foreign table if exists odoo12_account_payment_mode;


create FOREIGN TABLE odoo12_account_payment_mode("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"bank_account_link" varchar NOT NULL
  ,"fixed_journal_id" int4
  ,"payment_method_id" int4 NOT NULL
  ,"payment_type" varchar
  ,"payment_method_code" varchar
  ,"active" bool
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"payment_order_ok" bool
  ,"no_debit_before_maturity" bool
  ,"default_payment_mode" varchar
  ,"default_invoice" bool
  ,"default_target_move" varchar
  ,"default_date_type" varchar
  ,"default_date_prefered" varchar
  ,"group_lines" bool
  ,"generate_move" bool
  ,"offsetting_account" varchar
  ,"transfer_account_id" int4
  ,"transfer_journal_id" int4
  ,"move_option" varchar
  ,"post_move" bool
  ,"initiating_party_issuer" varchar(35)
  ,"initiating_party_identifier" varchar(35)
  ,"sepa_creditor_identifier" varchar(35)
  ,"show_bank_account" varchar
  ,"show_bank_account_from_journal" bool
  ,"show_bank_account_chars" int4
  ,"initiating_party_scheme" varchar(35))SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_mode');



drop foreign table if exists odoo12_account_payment_mode_variable_journal_rel;


create FOREIGN TABLE odoo12_account_payment_mode_variable_journal_rel("payment_mode_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_mode_variable_journal_rel');



drop foreign table if exists odoo12_account_payment_order;


create FOREIGN TABLE odoo12_account_payment_order("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar
  ,"payment_mode_id" int4 NOT NULL
  ,"payment_type" varchar NOT NULL
  ,"payment_method_id" int4
  ,"company_id" int4
  ,"company_currency_id" int4
  ,"journal_id" int4
  ,"state" varchar
  ,"date_prefered" varchar NOT NULL
  ,"date_scheduled" date
  ,"date_generated" date
  ,"date_uploaded" date
  ,"date_done" date
  ,"generated_user_id" int4
  ,"total_company_currency" numeric
  ,"description" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"charge_bearer" varchar
  ,"batch_booking" bool
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_order');



drop foreign table if exists odoo12_account_payment_term;


create FOREIGN TABLE odoo12_account_payment_term("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"note" text
  ,"company_id" int4 NOT NULL
  ,"sequence" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_term');



drop foreign table if exists odoo12_account_payment_term_line;


create FOREIGN TABLE odoo12_account_payment_term_line("id" int4 NOT NULL
  ,"value" varchar NOT NULL
  ,"value_amount" numeric
  ,"days" int4 NOT NULL
  ,"option" varchar NOT NULL
  ,"payment_id" int4 NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"openupgrade_legacy_12_0_option" varchar
  ,"day_of_the_month" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_term_line');



drop foreign table if exists odoo12_account_print_journal;


create FOREIGN TABLE odoo12_account_print_journal("id" int4 NOT NULL
  ,"sort_selection" varchar NOT NULL
  ,"amount_currency" bool
  ,"company_id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_print_journal');



drop foreign table if exists odoo12_account_reconcile_model;


create FOREIGN TABLE odoo12_account_reconcile_model("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4 NOT NULL
  ,"has_second_line" bool
  ,"company_id" int4 NOT NULL
  ,"account_id" int4
  ,"journal_id" int4
  ,"label" varchar
  ,"amount_type" varchar NOT NULL
  ,"amount" numeric NOT NULL
  ,"tax_id" int4
  ,"analytic_account_id" int4
  ,"second_account_id" int4
  ,"second_journal_id" int4
  ,"second_label" varchar
  ,"second_amount_type" varchar NOT NULL
  ,"second_amount" numeric NOT NULL
  ,"second_tax_id" int4
  ,"second_analytic_account_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"rule_type" varchar NOT NULL
  ,"auto_reconcile" bool
  ,"match_nature" varchar NOT NULL
  ,"match_amount" varchar
  ,"match_amount_min" float8
  ,"match_amount_max" float8
  ,"match_label" varchar
  ,"match_label_param" varchar
  ,"match_same_currency" bool
  ,"match_total_amount" bool
  ,"match_total_amount_param" float8
  ,"match_partner" bool
  ,"force_tax_included" bool
  ,"force_second_tax_included" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model');



drop foreign table if exists odoo12_account_reconcile_model_res_partner_category_rel;


create FOREIGN TABLE odoo12_account_reconcile_model_res_partner_category_rel("account_reconcile_model_id" int4 NOT NULL
  ,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_category_rel');



drop foreign table if exists odoo12_account_reconcile_model_res_partner_rel;


create FOREIGN TABLE odoo12_account_reconcile_model_res_partner_rel("account_reconcile_model_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_rel');



drop foreign table if exists odoo12_account_reconcile_model_template;


create FOREIGN TABLE odoo12_account_reconcile_model_template("id" int4 NOT NULL
  ,"chart_template_id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4 NOT NULL
  ,"has_second_line" bool
  ,"account_id" int4
  ,"label" varchar
  ,"amount_type" varchar NOT NULL
  ,"amount" numeric NOT NULL
  ,"tax_id" int4
  ,"second_account_id" int4
  ,"second_label" varchar
  ,"second_amount_type" varchar NOT NULL
  ,"second_amount" numeric NOT NULL
  ,"second_tax_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"rule_type" varchar NOT NULL
  ,"auto_reconcile" bool
  ,"match_nature" varchar NOT NULL
  ,"match_amount" varchar
  ,"match_amount_min" float8
  ,"match_amount_max" float8
  ,"match_label" varchar
  ,"match_label_param" varchar
  ,"match_same_currency" bool
  ,"match_total_amount" bool
  ,"match_total_amount_param" float8
  ,"match_partner" bool
  ,"force_tax_included" bool
  ,"force_second_tax_included" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template');



drop foreign table if exists odoo12_account_reconcile_model_template_res_partner_category_rel;


create FOREIGN TABLE odoo12_account_reconcile_model_template_res_partner_category_rel("account_reconcile_model_template_id" int4 NOT NULL
  ,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_category_rel');



drop foreign table if exists odoo12_account_reconcile_model_template_res_partner_rel;


create FOREIGN TABLE odoo12_account_reconcile_model_template_res_partner_rel("account_reconcile_model_template_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_rel');



drop foreign table if exists odoo12_account_register_payments;


create FOREIGN TABLE odoo12_account_register_payments("id" int4 NOT NULL
  ,"payment_type" varchar NOT NULL
  ,"payment_method_id" int4 NOT NULL
  ,"partner_type" varchar
  ,"partner_id" int4
  ,"amount" numeric NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"payment_date" date NOT NULL
  ,"communication" varchar
  ,"journal_id" int4 NOT NULL
  ,"multi" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"group_invoices" bool
  ,"payment_difference_handling" varchar
  ,"writeoff_account_id" int4
  ,"writeoff_label" varchar
  ,"partner_bank_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_register_payments');



drop foreign table if exists odoo12_account_report_general_ledger;


create FOREIGN TABLE odoo12_account_report_general_ledger("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"display_account" varchar NOT NULL
  ,"initial_balance" bool
  ,"sortby" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_report_general_ledger');



drop foreign table if exists odoo12_account_report_general_ledger_journal_rel;


create FOREIGN TABLE odoo12_account_report_general_ledger_journal_rel("account_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_report_general_ledger_journal_rel');



drop foreign table if exists odoo12_account_report_partner_ledger;


create FOREIGN TABLE odoo12_account_report_partner_ledger("id" int4 NOT NULL
  ,"amount_currency" bool
  ,"reconciled" bool
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"result_selection" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_report_partner_ledger');



drop foreign table if exists odoo12_account_setup_bank_manual_config;


create FOREIGN TABLE odoo12_account_setup_bank_manual_config("id" int4 NOT NULL
  ,"res_partner_bank_id" int4 NOT NULL
  ,"create_or_link_option" varchar
  ,"new_journal_code" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_setup_bank_manual_config');



drop foreign table if exists odoo12_account_tax;


create FOREIGN TABLE odoo12_account_tax("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type_tax_use" varchar NOT NULL
  ,"openupgrade_legacy_12_0_tax_adjustment" bool
  ,"amount_type" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4 NOT NULL
  ,"sequence" int4 NOT NULL
  ,"amount" numeric NOT NULL
  ,"account_id" int4
  ,"refund_account_id" int4
  ,"description" varchar
  ,"price_include" bool
  ,"include_base_amount" bool
  ,"analytic" bool
  ,"tax_group_id" int4 NOT NULL
  ,"tax_exigibility" varchar
  ,"cash_basis_account_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"cash_basis_base_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax');



drop foreign table if exists odoo12_account_tax_account_tag;


create FOREIGN TABLE odoo12_account_tax_account_tag("account_tax_id" int4 NOT NULL
  ,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_account_tag');



drop foreign table if exists odoo12_account_tax_filiation_rel;


create FOREIGN TABLE odoo12_account_tax_filiation_rel("parent_tax" int4 NOT NULL
  ,"child_tax" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_filiation_rel');



drop foreign table if exists odoo12_account_tax_group;


create FOREIGN TABLE odoo12_account_tax_group("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_group');



drop foreign table if exists odoo12_account_tax_purchase_order_line_rel;


create FOREIGN TABLE odoo12_account_tax_purchase_order_line_rel("purchase_order_line_id" int4 NOT NULL
  ,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_purchase_order_line_rel');



drop foreign table if exists odoo12_account_tax_report;


create FOREIGN TABLE odoo12_account_tax_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_report');



drop foreign table if exists odoo12_account_tax_sale_advance_payment_inv_rel;


create FOREIGN TABLE odoo12_account_tax_sale_advance_payment_inv_rel("sale_advance_payment_inv_id" int4 NOT NULL
  ,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_sale_advance_payment_inv_rel');



drop foreign table if exists odoo12_account_tax_sale_order_line_rel;


create FOREIGN TABLE odoo12_account_tax_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
  ,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_sale_order_line_rel');



drop foreign table if exists odoo12_account_tax_template;


create FOREIGN TABLE odoo12_account_tax_template("id" int4 NOT NULL
  ,"chart_template_id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type_tax_use" varchar NOT NULL
  ,"openupgrade_legacy_12_0_tax_adjustment" bool
  ,"amount_type" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"sequence" int4 NOT NULL
  ,"amount" numeric NOT NULL
  ,"account_id" int4
  ,"refund_account_id" int4
  ,"description" varchar
  ,"price_include" bool
  ,"include_base_amount" bool
  ,"analytic" bool
  ,"tax_group_id" int4
  ,"tax_exigibility" varchar
  ,"cash_basis_account_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"aeat_349_map_line" int4
  ,"cash_basis_base_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template');



drop foreign table if exists odoo12_account_tax_template_aeat_vat_book_map_line_rel;


create FOREIGN TABLE odoo12_account_tax_template_aeat_vat_book_map_line_rel("aeat_vat_book_map_line_id" int4 NOT NULL
  ,"account_tax_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template_aeat_vat_book_map_line_rel');



drop foreign table if exists odoo12_account_tax_template_filiation_rel;


create FOREIGN TABLE odoo12_account_tax_template_filiation_rel("parent_tax" int4 NOT NULL
  ,"child_tax" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template_filiation_rel');



drop foreign table if exists odoo12_account_tax_template_l10n_es_aeat_map_tax_line_rel;


create FOREIGN TABLE odoo12_account_tax_template_l10n_es_aeat_map_tax_line_rel("l10n_es_aeat_map_tax_line_id" int4 NOT NULL
  ,"account_tax_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template_l10n_es_aeat_map_tax_line_rel');



drop foreign table if exists odoo12_account_unreconcile;


create FOREIGN TABLE odoo12_account_unreconcile("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_unreconcile');



drop foreign table if exists odoo12_accounting_report;


create FOREIGN TABLE odoo12_accounting_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"target_move" varchar NOT NULL
  ,"enable_filter" bool
  ,"account_report_id" int4 NOT NULL
  ,"label_filter" varchar
  ,"filter_cmp" varchar NOT NULL
  ,"date_from_cmp" date
  ,"date_to_cmp" date
  ,"debit_credit" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'accounting_report');



drop foreign table if exists odoo12_add_mis_report_instance_dashboard_wizard;


create FOREIGN TABLE odoo12_add_mis_report_instance_dashboard_wizard("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"dashboard_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'add_mis_report_instance_dashboard_wizard');



drop foreign table if exists odoo12_address_format_es_simplified;


create FOREIGN TABLE odoo12_address_format_es_simplified("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'address_format_es_simplified');



drop foreign table if exists odoo12_aeat_349_map_line;


create FOREIGN TABLE odoo12_aeat_349_map_line("id" int4 NOT NULL
  ,"physical_product" bool
  ,"operation_key" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_349_map_line');



drop foreign table if exists odoo12_aeat_model_export_config;


create FOREIGN TABLE odoo12_aeat_model_export_config("id" int4 NOT NULL
  ,"name" varchar
  ,"model_number" varchar(3)
  ,"model_id" int4
  ,"date_start" date
  ,"date_end" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"active" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_model_export_config');



drop foreign table if exists odoo12_aeat_model_export_config_line;


create FOREIGN TABLE odoo12_aeat_model_export_config_line("id" int4 NOT NULL
  ,"sequence" int4
  ,"export_config_id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"repeat_expression" varchar
  ,"repeat" bool
  ,"conditional_expression" varchar
  ,"conditional" bool
  ,"subconfig_id" int4
  ,"export_type" varchar NOT NULL
  ,"apply_sign" bool
  ,"positive_sign" varchar(1)
  ,"negative_sign" varchar(1)
  ,"size" int4
  ,"alignment" varchar
  ,"bool_no" varchar(1)
  ,"bool_yes" varchar(1)
  ,"decimal_size" int4
  ,"expression" varchar
  ,"fixed_value" varchar
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_model_export_config_line');



drop foreign table if exists odoo12_aeat_tax_agency;
create FOREIGN TABLE odoo12_aeat_tax_agency("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_tax_agency');



drop foreign table if exists odoo12_aeat_vat_book_map_line;


create FOREIGN TABLE odoo12_aeat_vat_book_map_line("id" int4 NOT NULL
  ,"name" varchar
  ,"book_type" varchar
  ,"special_tax_group" varchar
  ,"fee_type_xlsx_column" varchar
  ,"fee_amount_xlsx_column" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_vat_book_map_line');



drop foreign table if exists odoo12_aged_partner_balance_wizard;


create FOREIGN TABLE odoo12_aged_partner_balance_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_at" date NOT NULL
  ,"target_move" varchar NOT NULL
  ,"receivable_accounts_only" bool
  ,"payable_accounts_only" bool
  ,"show_move_line_details" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard');



drop foreign table if exists odoo12_aged_partner_balance_wizard_res_partner_rel;


create FOREIGN TABLE odoo12_aged_partner_balance_wizard_res_partner_rel("aged_partner_balance_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard_res_partner_rel');



drop foreign table if exists odoo12_asset_depreciation_confirmation_wizard;


create FOREIGN TABLE odoo12_asset_depreciation_confirmation_wizard("id" int4 NOT NULL
  ,"date" date NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'asset_depreciation_confirmation_wizard');



drop foreign table if exists odoo12_asset_modify;


create FOREIGN TABLE odoo12_asset_modify("id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"method_number" int4 NOT NULL
  ,"method_period" int4
  ,"method_end" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'asset_modify');



drop foreign table if exists odoo12_bank_payment_line;


create FOREIGN TABLE odoo12_bank_payment_line("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"order_id" int4
  ,"payment_type" varchar
  ,"state" varchar
  ,"partner_id" int4
  ,"amount_currency" numeric
  ,"amount_company_currency" numeric
  ,"communication" varchar NOT NULL
  ,"company_id" int4
  ,"company_currency_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bank_payment_line');



drop foreign table if exists odoo12_barcode_nomenclature;


create FOREIGN TABLE odoo12_barcode_nomenclature("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"upc_ean_conv" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'barcode_nomenclature');



drop foreign table if exists odoo12_barcode_rule;


create FOREIGN TABLE odoo12_barcode_rule("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"barcode_nomenclature_id" int4
  ,"sequence" int4
  ,"encoding" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"pattern" varchar(32) NOT NULL
  ,"alias" varchar(32) NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'barcode_rule');



drop foreign table if exists odoo12_base_automation;
create FOREIGN TABLE odoo12_base_automation("id" int4 NOT NULL
  ,"action_server_id" int4 NOT NULL
  ,"active" bool
  ,"trigger" varchar NOT NULL
  ,"trg_date_id" int4
  ,"trg_date_range" int4
  ,"trg_date_range_type" varchar
  ,"trg_date_calendar_id" int4
  ,"filter_pre_domain" varchar
  ,"filter_domain" varchar
  ,"last_run" timestamp
  ,"on_change_fields" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation');



drop foreign table if exists odoo12_base_automation_lead_test;


create FOREIGN TABLE odoo12_base_automation_lead_test("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"user_id" int4
  ,"state" varchar
  ,"active" bool
  ,"partner_id" int4
  ,"date_action_last" timestamp
  ,"customer" bool
  ,"priority" bool
  ,"deadline" bool
  ,"is_assigned_to_admin" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation_lead_test');



drop foreign table if exists odoo12_base_automation_line_test;


create FOREIGN TABLE odoo12_base_automation_line_test("id" int4 NOT NULL
  ,"name" varchar
  ,"lead_id" int4
  ,"user_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation_line_test');



drop foreign table if exists odoo12_base_import_import;


create FOREIGN TABLE odoo12_base_import_import("id" int4 NOT NULL
  ,"res_model" varchar
  ,"file" bytea
  ,"file_name" varchar
  ,"file_type" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_import');



drop foreign table if exists odoo12_base_import_mapping;


create FOREIGN TABLE odoo12_base_import_mapping("id" int4 NOT NULL
  ,"res_model" varchar
  ,"column_name" varchar
  ,"field_name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_mapping');



drop foreign table if exists odoo12_base_import_tests_models_char;


create FOREIGN TABLE odoo12_base_import_tests_models_char("id" int4 NOT NULL
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char');



drop foreign table if exists odoo12_base_import_tests_models_char_noreadonly;


create FOREIGN TABLE odoo12_base_import_tests_models_char_noreadonly("id" int4 NOT NULL
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_noreadonly');



drop foreign table if exists odoo12_base_import_tests_models_char_readonly;


create FOREIGN TABLE odoo12_base_import_tests_models_char_readonly("id" int4 NOT NULL
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_readonly');



drop foreign table if exists odoo12_base_import_tests_models_char_required;


create FOREIGN TABLE odoo12_base_import_tests_models_char_required("id" int4 NOT NULL
  ,"value" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_required');



drop foreign table if exists odoo12_base_import_tests_models_char_states;


create FOREIGN TABLE odoo12_base_import_tests_models_char_states("id" int4 NOT NULL
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_states');



drop foreign table if exists odoo12_base_import_tests_models_char_stillreadonly;


create FOREIGN TABLE odoo12_base_import_tests_models_char_stillreadonly("id" int4 NOT NULL
  ,"value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_stillreadonly');



drop foreign table if exists odoo12_base_import_tests_models_complex;


create FOREIGN TABLE odoo12_base_import_tests_models_complex("id" int4 NOT NULL
  ,"f" float8
  ,"m" numeric
  ,"c" varchar
  ,"currency_id" int4
  ,"d" date
  ,"dt" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_complex');



drop foreign table if exists odoo12_base_import_tests_models_float;


create FOREIGN TABLE odoo12_base_import_tests_models_float("id" int4 NOT NULL
  ,"value" float8
  ,"value2" numeric
  ,"currency_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_float');



drop foreign table if exists odoo12_base_import_tests_models_m2o;


create FOREIGN TABLE odoo12_base_import_tests_models_m2o("id" int4 NOT NULL
  ,"value" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o');



drop foreign table if exists odoo12_base_import_tests_models_m2o_related;


create FOREIGN TABLE odoo12_base_import_tests_models_m2o_related("id" int4 NOT NULL
  ,"value" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_related');



drop foreign table if exists odoo12_base_import_tests_models_m2o_required;


create FOREIGN TABLE odoo12_base_import_tests_models_m2o_required("id" int4 NOT NULL
  ,"value" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required');



drop foreign table if exists odoo12_base_import_tests_models_m2o_required_related;


create FOREIGN TABLE odoo12_base_import_tests_models_m2o_required_related("id" int4 NOT NULL
  ,"value" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required_related');



drop foreign table if exists odoo12_base_import_tests_models_o2m;


create FOREIGN TABLE odoo12_base_import_tests_models_o2m("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m');



drop foreign table if exists odoo12_base_import_tests_models_o2m_child;


create FOREIGN TABLE odoo12_base_import_tests_models_o2m_child("id" int4 NOT NULL
  ,"parent_id" int4
  ,"value" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m_child');



drop foreign table if exists odoo12_base_import_tests_models_preview;


create FOREIGN TABLE odoo12_base_import_tests_models_preview("id" int4 NOT NULL
  ,"name" varchar
  ,"somevalue" int4 NOT NULL
  ,"othervalue" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_preview');



drop foreign table if exists odoo12_base_language_export;


create FOREIGN TABLE odoo12_base_language_export("id" int4 NOT NULL
  ,"name" varchar
  ,"lang" varchar NOT NULL
  ,"format" varchar NOT NULL
  ,"data" bytea
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_export');



drop foreign table if exists odoo12_base_language_import;


create FOREIGN TABLE odoo12_base_language_import("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar(6) NOT NULL
  ,"data" bytea NOT NULL
  ,"filename" varchar NOT NULL
  ,"overwrite" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_import');



drop foreign table if exists odoo12_base_language_install;


create FOREIGN TABLE odoo12_base_language_install("id" int4 NOT NULL
  ,"lang" varchar NOT NULL
  ,"overwrite" bool
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_install');



drop foreign table if exists odoo12_base_language_install_website_rel;


create FOREIGN TABLE odoo12_base_language_install_website_rel("base_language_install_id" int4 NOT NULL
  ,"website_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_install_website_rel');



drop foreign table if exists odoo12_base_module_uninstall;


create FOREIGN TABLE odoo12_base_module_uninstall("id" int4 NOT NULL
  ,"show_all" bool
  ,"module_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_uninstall');



drop foreign table if exists odoo12_base_module_update;


create FOREIGN TABLE odoo12_base_module_update("id" int4 NOT NULL
  ,"updated" int4
  ,"added" int4
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_update');



drop foreign table if exists odoo12_base_module_upgrade;


create FOREIGN TABLE odoo12_base_module_upgrade("id" int4 NOT NULL
  ,"module_info" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_upgrade');



drop foreign table if exists odoo12_base_partner_merge_automatic_wizard;


create FOREIGN TABLE odoo12_base_partner_merge_automatic_wizard("id" int4 NOT NULL
  ,"group_by_email" bool
  ,"group_by_name" bool
  ,"group_by_is_company" bool
  ,"group_by_vat" bool
  ,"group_by_parent_id" bool
  ,"state" varchar NOT NULL
  ,"number_group" int4
  ,"current_line_id" int4
  ,"dst_partner_id" int4
  ,"exclude_contact" bool
  ,"exclude_journal_item" bool
  ,"maximum_group" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard');



drop foreign table if exists odoo12_base_partner_merge_automatic_wizard_res_partner_rel;


create FOREIGN TABLE odoo12_base_partner_merge_automatic_wizard_res_partner_rel("base_partner_merge_automatic_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard_res_partner_rel');



drop foreign table if exists odoo12_base_partner_merge_line;


create FOREIGN TABLE odoo12_base_partner_merge_line("id" int4 NOT NULL
  ,"wizard_id" int4
  ,"min_id" int4
  ,"aggr_ids" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_line');



drop foreign table if exists odoo12_base_update_translations;


create FOREIGN TABLE odoo12_base_update_translations("id" int4 NOT NULL
  ,"lang" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_update_translations');



drop foreign table if exists odoo12_better_zip_geonames_import;


create FOREIGN TABLE odoo12_better_zip_geonames_import("id" int4 NOT NULL
  ,"country_id" int4 NOT NULL
  ,"letter_case" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'better_zip_geonames_import');



drop foreign table if exists odoo12_bus_bus;


create FOREIGN TABLE odoo12_bus_bus("id" int4 NOT NULL
  ,"create_date" timestamp
  ,"channel" varchar
  ,"message" varchar
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bus_bus');



drop foreign table if exists odoo12_bus_presence;


create FOREIGN TABLE odoo12_bus_presence("id" int4 NOT NULL
  ,"user_id" int4 NOT NULL
  ,"last_poll" timestamp
  ,"last_presence" timestamp
  ,"status" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bus_presence');



drop foreign table if exists odoo12_calendar_alarm;


create FOREIGN TABLE odoo12_calendar_alarm("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"duration" int4 NOT NULL
  ,"interval" varchar NOT NULL
  ,"duration_minutes" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_alarm');



drop foreign table if exists odoo12_calendar_alarm_calendar_event_rel;


create FOREIGN TABLE odoo12_calendar_alarm_calendar_event_rel("calendar_event_id" int4 NOT NULL
  ,"calendar_alarm_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_alarm_calendar_event_rel');



drop foreign table if exists odoo12_calendar_attendee;


create FOREIGN TABLE odoo12_calendar_attendee("id" int4 NOT NULL
  ,"state" varchar
  ,"common_name" varchar
  ,"partner_id" int4
  ,"email" varchar
  ,"availability" varchar
  ,"access_token" varchar
  ,"event_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"google_internal_event_id" varchar
  ,"oe_synchro_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_attendee');



drop foreign table if exists odoo12_calendar_contacts;


create FOREIGN TABLE odoo12_calendar_contacts("id" int4 NOT NULL
  ,"user_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_contacts');



drop foreign table if exists odoo12_calendar_event;


create FOREIGN TABLE odoo12_calendar_event("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"state" varchar
  ,"display_start" varchar
  ,"start" timestamp NOT NULL
  ,"stop" timestamp NOT NULL
  ,"allday" bool
  ,"start_date" date
  ,"start_datetime" timestamp
  ,"stop_date" date
  ,"stop_datetime" timestamp
  ,"duration" float8
  ,"description" text
  ,"privacy" varchar
  ,"location" varchar
  ,"show_as" varchar
  ,"res_id" int4
  ,"res_model_id" int4
  ,"res_model" varchar
  ,"rrule" varchar
  ,"rrule_type" varchar
  ,"recurrency" bool
  ,"recurrent_id" int4
  ,"recurrent_id_date" timestamp
  ,"end_type" varchar
  ,"interval" int4
  ,"count" int4
  ,"mo" bool
  ,"tu" bool
  ,"we" bool
  ,"th" bool
  ,"fr" bool
  ,"sa" bool
  ,"su" bool
  ,"month_by" varchar
  ,"day" int4
  ,"week_list" varchar
  ,"byday" varchar
  ,"final_date" date
  ,"user_id" int4
  ,"active" bool
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"opportunity_id" int4
  ,"message_main_attachment_id" int4
  ,"oe_update_date" timestamp
  ,"applicant_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event');



drop foreign table if exists odoo12_calendar_event_res_partner_rel;


create FOREIGN TABLE odoo12_calendar_event_res_partner_rel("calendar_event_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event_res_partner_rel');



drop foreign table if exists odoo12_calendar_event_type;


create FOREIGN TABLE odoo12_calendar_event_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event_type');



drop foreign table if exists odoo12_cash_box_in;


create FOREIGN TABLE odoo12_cash_box_in("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"amount" numeric NOT NULL
  ,"ref" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'cash_box_in');



drop foreign table if exists odoo12_cash_box_out;


create FOREIGN TABLE odoo12_cash_box_out("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"amount" numeric NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'cash_box_out');



drop foreign table if exists odoo12_change_password_user;


create FOREIGN TABLE odoo12_change_password_user("id" int4 NOT NULL
  ,"wizard_id" int4 NOT NULL
  ,"user_id" int4 NOT NULL
  ,"user_login" varchar
  ,"new_passwd" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'change_password_user');



drop foreign table if exists odoo12_change_password_wizard;


create FOREIGN TABLE odoo12_change_password_wizard("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'change_password_wizard');



drop foreign table if exists odoo12_city_zip_geonames_import;


create FOREIGN TABLE odoo12_city_zip_geonames_import("id" int4 NOT NULL
  ,"country_id" int4 NOT NULL
  ,"letter_case" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'city_zip_geonames_import');



drop foreign table if exists odoo12_config_es_toponyms;


create FOREIGN TABLE odoo12_config_es_toponyms("id" int4 NOT NULL
  ,"name" varchar(64)
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'config_es_toponyms');



drop foreign table if exists odoo12_contract_contract;


create FOREIGN TABLE odoo12_contract_contract("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"code" varchar
  ,"active" bool
  ,"company_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"pricelist_id" int4
  ,"recurring_rule_type" varchar
  ,"recurring_invoicing_type" varchar
  ,"recurring_interval" int4
  ,"journal_id" int4
  ,"contract_template_id" int4
  ,"date_start" date
  ,"date_end" date
  ,"recurring_invoices" bool
  ,"recurring_next_date" date
  ,"user_id" int4
  ,"skip_zero_qty" bool
  ,"invoicing_sales" bool
  ,"message_main_attachment_id" int4
  ,"group_id" int4
  ,"contract_type" varchar
  ,"payment_term_id" int4
  ,"fiscal_position_id" int4
  ,"invoice_partner_id" int4
  ,"commercial_partner_id" int4
  ,"note" text
  ,"is_terminated" bool
  ,"terminate_reason_id" int4
  ,"terminate_comment" text
  ,"terminate_date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract');



drop foreign table if exists odoo12_contract_contract_contract_tag_rel;


create FOREIGN TABLE odoo12_contract_contract_contract_tag_rel("contract_contract_id" int4 NOT NULL
  ,"contract_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_contract_tag_rel');



drop foreign table if exists odoo12_contract_contract_terminate;


create FOREIGN TABLE odoo12_contract_contract_terminate("id" int4 NOT NULL
  ,"contract_id" int4 NOT NULL
  ,"terminate_reason_id" int4 NOT NULL
  ,"terminate_comment" text
  ,"terminate_date" date NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_terminate');



drop foreign table if exists odoo12_contract_line;


create FOREIGN TABLE odoo12_contract_line("id" int4 NOT NULL
  ,"product_id" int4
  ,"analytic_account_id" int4
  ,"name" text NOT NULL
  ,"quantity" float8 NOT NULL
  ,"uom_id" int4
  ,"automatic_price" bool
  ,"specific_price" float8
  ,"discount" numeric
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"qty_type" varchar NOT NULL
  ,"qty_formula_id" int4
  ,"recurring_rule_type" varchar(255) NOT NULL
  ,"recurring_invoicing_type" varchar(255) NOT NULL
  ,"recurring_interval" int4 NOT NULL
  ,"recurring_next_date" date
  ,"date_start" date NOT NULL
  ,"date_end" date
  ,"contract_id" int4 NOT NULL
  ,"is_canceled" bool
  ,"is_auto_renew" bool
  ,"auto_renew_interval" int4
  ,"auto_renew_rule_type" varchar
  ,"termination_notice_interval" int4
  ,"termination_notice_rule_type" varchar
  ,"display_type" varchar
  ,"note_invoicing_mode" varchar
  ,"last_date_invoiced" date
  ,"termination_notice_date" date
  ,"successor_contract_line_id" int4
  ,"predecessor_contract_line_id" int4
  ,"manual_renew_needed" bool
  ,"active" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_line');



drop foreign table if exists odoo12_contract_line_qty_formula;


create FOREIGN TABLE odoo12_contract_line_qty_formula("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" text NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_line_qty_formula');



drop foreign table if exists odoo12_contract_line_wizard;


create FOREIGN TABLE odoo12_contract_line_wizard("id" int4 NOT NULL
  ,"date_start" date
  ,"date_end" date
  ,"recurring_next_date" date
  ,"is_auto_renew" bool
  ,"manual_renew_needed" bool
  ,"contract_line_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_line_wizard');



drop foreign table if exists odoo12_contract_manually_create_invoice;


create FOREIGN TABLE odoo12_contract_manually_create_invoice("id" int4 NOT NULL
  ,"invoice_date" date NOT NULL
  ,"contract_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_manually_create_invoice');



drop foreign table if exists odoo12_contract_tag;


create FOREIGN TABLE odoo12_contract_tag("id" int4 NOT NULL
  ,"name" varchar
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_tag');



drop foreign table if exists odoo12_contract_template;


create FOREIGN TABLE odoo12_contract_template("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"partner_id" int4
  ,"pricelist_id" int4
  ,"recurring_rule_type" varchar
  ,"recurring_invoicing_type" varchar
  ,"recurring_interval" int4
  ,"journal_id" int4
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"contract_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_template');



drop foreign table if exists odoo12_contract_template_line;


create FOREIGN TABLE odoo12_contract_template_line("id" int4 NOT NULL
  ,"contract_id" int4 NOT NULL
  ,"product_id" int4
  ,"name" text NOT NULL
  ,"quantity" float8 NOT NULL
  ,"uom_id" int4
  ,"automatic_price" bool
  ,"specific_price" float8
  ,"discount" numeric
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"qty_type" varchar NOT NULL
  ,"qty_formula_id" int4
  ,"recurring_rule_type" varchar(255) NOT NULL
  ,"recurring_invoicing_type" varchar(255) NOT NULL
  ,"recurring_interval" int4 NOT NULL
  ,"date_start" date
  ,"recurring_next_date" date
  ,"last_date_invoiced" date
  ,"is_canceled" bool
  ,"is_auto_renew" bool
  ,"auto_renew_interval" int4
  ,"auto_renew_rule_type" varchar
  ,"termination_notice_interval" int4
  ,"termination_notice_rule_type" varchar
  ,"display_type" varchar
  ,"note_invoicing_mode" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_template_line');



drop foreign table if exists odoo12_contract_terminate_reason;


create FOREIGN TABLE odoo12_contract_terminate_reason("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"terminate_comment_required" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_terminate_reason');



drop foreign table if exists odoo12_crm_activity_report;


create FOREIGN TABLE odoo12_crm_activity_report("id" int4
  ,"subtype_id" int4
  ,"mail_activity_type_id" int4
  ,"author_id" int4
  ,"date" timestamp
  ,"subject" varchar
  ,"lead_id" int4
  ,"user_id" int4
  ,"team_id" int4
  ,"country_id" int4
  ,"company_id" int4
  ,"stage_id" int4
  ,"partner_id" int4
  ,"lead_type" varchar
  ,"active" bool
  ,"probability" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_activity_report');



drop foreign table if exists odoo12_crm_lead;


create FOREIGN TABLE odoo12_crm_lead("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"campaign_id" int4
  ,"source_id" int4
  ,"medium_id" int4
  ,"name" varchar NOT NULL
  ,"partner_id" int4
  ,"active" bool
  ,"date_action_last" timestamp
  ,"email_from" varchar
  ,"website" varchar
  ,"team_id" int4
  ,"email_cc" text
  ,"description" text
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"contact_name" varchar
  ,"partner_name" varchar
  ,"openupgrade_legacy_12_0_opt_out" bool
  ,"type" varchar NOT NULL
  ,"priority" varchar
  ,"date_closed" timestamp
  ,"stage_id" int4
  ,"user_id" int4
  ,"referred" varchar
  ,"date_open" timestamp
  ,"day_open" float8
  ,"day_close" float8
  ,"date_last_stage_update" timestamp
  ,"date_conversion" timestamp
  ,"message_bounce" int4
  ,"probability" float8
  ,"date_deadline" date
  ,"color" int4
  ,"street" varchar
  ,"street2" varchar
  ,"zip" varchar
  ,"city" varchar
  ,"state_id" int4
  ,"country_id" int4
  ,"phone" varchar
  ,"mobile" varchar
  ,"function" varchar
  ,"title" int4
  ,"company_id" int4
  ,"lost_reason" int4
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_uid" int4
  ,"write_uid" int4
  ,"message_main_attachment_id" int4
  ,"planned_revenue" numeric
  ,"expected_revenue" numeric
  ,"project_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead');



drop foreign table if exists odoo12_crm_lead2opportunity_partner;


create FOREIGN TABLE odoo12_crm_lead2opportunity_partner("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"user_id" int4
  ,"team_id" int4
  ,"action" varchar NOT NULL
  ,"partner_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner');



drop foreign table if exists odoo12_crm_lead2opportunity_partner_mass;


create FOREIGN TABLE odoo12_crm_lead2opportunity_partner_mass("id" int4 NOT NULL
  ,"team_id" int4
  ,"deduplicate" bool
  ,"action" varchar NOT NULL
  ,"force_assignation" bool
  ,"name" varchar NOT NULL
  ,"user_id" int4
  ,"partner_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass');



drop foreign table if exists odoo12_crm_lead2opportunity_partner_mass_res_users_rel;


create FOREIGN TABLE odoo12_crm_lead2opportunity_partner_mass_res_users_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass_res_users_rel');



drop foreign table if exists odoo12_crm_lead_convert2task;


create FOREIGN TABLE odoo12_crm_lead_convert2task("id" int4 NOT NULL
  ,"action" varchar NOT NULL
  ,"partner_id" int4
  ,"lead_id" int4
  ,"project_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_convert2task');



drop foreign table if exists odoo12_crm_lead_crm_lead2opportunity_partner_mass_rel;


create FOREIGN TABLE odoo12_crm_lead_crm_lead2opportunity_partner_mass_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
  ,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_mass_rel');



drop foreign table if exists odoo12_crm_lead_crm_lead2opportunity_partner_rel;


create FOREIGN TABLE odoo12_crm_lead_crm_lead2opportunity_partner_rel("crm_lead2opportunity_partner_id" int4 NOT NULL
  ,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_rel');



drop foreign table if exists odoo12_crm_lead_lost;


create FOREIGN TABLE odoo12_crm_lead_lost("id" int4 NOT NULL
  ,"lost_reason_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_lost');



drop foreign table if exists odoo12_crm_lead_tag;


create FOREIGN TABLE odoo12_crm_lead_tag("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_tag');



drop foreign table if exists odoo12_crm_lead_tag_rel;


create FOREIGN TABLE odoo12_crm_lead_tag_rel("lead_id" int4 NOT NULL
  ,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_tag_rel');



drop foreign table if exists odoo12_crm_lost_reason;
create FOREIGN TABLE odoo12_crm_lost_reason("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lost_reason');



drop foreign table if exists odoo12_crm_merge_opportunity;


create FOREIGN TABLE odoo12_crm_merge_opportunity("id" int4 NOT NULL
  ,"user_id" int4
  ,"team_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_merge_opportunity');



drop foreign table if exists odoo12_crm_partner_binding;


create FOREIGN TABLE odoo12_crm_partner_binding("id" int4 NOT NULL
  ,"action" varchar NOT NULL
  ,"partner_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_partner_binding');



drop foreign table if exists odoo12_crm_stage;


create FOREIGN TABLE odoo12_crm_stage("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"probability" float8 NOT NULL
  ,"on_change" bool
  ,"requirements" text
  ,"team_id" int4
  ,"legend_priority" text
  ,"fold" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_stage');



drop foreign table if exists odoo12_crm_team;


create FOREIGN TABLE odoo12_crm_team("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"user_id" int4
  ,"reply_to" varchar
  ,"color" int4
  ,"team_type" varchar NOT NULL
  ,"dashboard_graph_model" varchar
  ,"dashboard_graph_group" varchar
  ,"dashboard_graph_period" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"use_quotations" bool
  ,"use_invoices" bool
  ,"invoiced_target" int4
  ,"use_leads" bool
  ,"use_opportunities" bool
  ,"alias_id" int4 NOT NULL
  ,"dashboard_graph_group_pipeline" varchar
  ,"message_main_attachment_id" int4
  ,"openupgrade_legacy_12_0_dashboard_graph_model" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_team');



drop foreign table if exists odoo12_date_range;


create FOREIGN TABLE odoo12_date_range("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"type_id" int4 NOT NULL
  ,"type_name" varchar
  ,"company_id" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range');



drop foreign table if exists odoo12_date_range_generator;


create FOREIGN TABLE odoo12_date_range_generator("id" int4 NOT NULL
  ,"name_prefix" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"type_id" int4 NOT NULL
  ,"company_id" int4
  ,"unit_of_time" int4 NOT NULL
  ,"duration_count" int4 NOT NULL
  ,"count" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range_generator');



drop foreign table if exists odoo12_date_range_type;
create FOREIGN TABLE odoo12_date_range_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"allow_overlap" bool
  ,"active" bool
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range_type');



drop foreign table if exists odoo12_decimal_precision;


create FOREIGN TABLE odoo12_decimal_precision("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"digits" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'decimal_precision');



drop foreign table if exists odoo12_decimal_precision_test;


create FOREIGN TABLE odoo12_decimal_precision_test("id" int4 NOT NULL
  ,"float" float8
  ,"float_2" numeric
  ,"float_4" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'decimal_precision_test');



drop foreign table if exists odoo12_digest_digest;


create FOREIGN TABLE odoo12_digest_digest("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"periodicity" varchar NOT NULL
  ,"next_run_date" date
  ,"template_id" int4 NOT NULL
  ,"company_id" int4
  ,"state" varchar
  ,"kpi_res_users_connected" bool
  ,"kpi_mail_message_total" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"kpi_account_total_revenue" bool
  ,"kpi_crm_lead_created" bool
  ,"kpi_crm_opportunities_won" bool
  ,"kpi_project_task_opened" bool
  ,"kpi_all_sale_total" bool
  ,"kpi_hr_recruitment_new_colleagues" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_digest');



drop foreign table if exists odoo12_digest_digest_res_users_rel;


create FOREIGN TABLE odoo12_digest_digest_res_users_rel("digest_digest_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_digest_res_users_rel');



drop foreign table if exists odoo12_digest_tip;


create FOREIGN TABLE odoo12_digest_tip("id" int4 NOT NULL
  ,"sequence" int4
  ,"tip_description" text
  ,"group_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_tip');



drop foreign table if exists odoo12_digest_tip_res_users_rel;


create FOREIGN TABLE odoo12_digest_tip_res_users_rel("digest_tip_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_tip_res_users_rel');



drop foreign table if exists odoo12_email_template_attachment_rel;


create FOREIGN TABLE odoo12_email_template_attachment_rel("email_template_id" int4 NOT NULL
  ,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_attachment_rel');



drop foreign table if exists odoo12_email_template_preview;


create FOREIGN TABLE odoo12_email_template_preview("id" int4 NOT NULL
  ,"res_id" varchar
  ,"name" varchar
  ,"model_id" int4
  ,"model" varchar
  ,"lang" varchar
  ,"user_signature" bool
  ,"subject" varchar
  ,"email_from" varchar
  ,"use_default_to" bool
  ,"email_to" varchar
  ,"partner_to" varchar
  ,"email_cc" varchar
  ,"reply_to" varchar
  ,"mail_server_id" int4
  ,"body_html" text
  ,"report_name" varchar
  ,"report_template" int4
  ,"ref_ir_act_window" int4
  ,"auto_delete" bool
  ,"model_object_field" int4
  ,"sub_object" int4
  ,"sub_model_object_field" int4
  ,"null_value" varchar
  ,"copyvalue" varchar
  ,"scheduled_date" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"force_email_send" bool
  ,"easy_my_coop" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_preview');



drop foreign table if exists odoo12_email_template_preview_res_partner_rel;


create FOREIGN TABLE odoo12_email_template_preview_res_partner_rel("email_template_preview_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_preview_res_partner_rel');



drop foreign table if exists odoo12_employee_category_rel;


create FOREIGN TABLE odoo12_employee_category_rel("category_id" int4 NOT NULL
  ,"emp_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'employee_category_rel');



drop foreign table if exists odoo12_expense_tax;


create FOREIGN TABLE odoo12_expense_tax("expense_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'expense_tax');



drop foreign table if exists odoo12_fetchmail_server;


create FOREIGN TABLE odoo12_fetchmail_server("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"state" varchar
  ,"server" varchar
  ,"port" int4
  ,"type" varchar NOT NULL
  ,"is_ssl" bool
  ,"attach" bool
  ,"original" bool
  ,"date" timestamp
  ,"user" varchar
  ,"password" varchar
  ,"action_id" int4
  ,"object_id" int4
  ,"priority" int4
  ,"configuration" text
  ,"script" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'fetchmail_server');



drop foreign table if exists odoo12_general_ledger_report_wizard;


create FOREIGN TABLE odoo12_general_ledger_report_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"target_move" varchar NOT NULL
  ,"centralize" bool
  ,"hide_account_balance_at_0" bool
  ,"receivable_accounts_only" bool
  ,"payable_accounts_only" bool
  ,"not_only_one_unaffected_earnings_account" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hide_account_at_0" bool
  ,"show_analytic_tags" bool
  ,"foreign_currency" bool
  ,"partner_ungrouped" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard');



drop foreign table if exists odoo12_general_ledger_report_wizard_res_partner_rel;


create FOREIGN TABLE odoo12_general_ledger_report_wizard_res_partner_rel("general_ledger_report_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard_res_partner_rel');



drop foreign table if exists odoo12_google_service;


create FOREIGN TABLE odoo12_google_service("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'google_service');



drop foreign table if exists odoo12_helpdesk_ticket;
create FOREIGN TABLE odoo12_helpdesk_ticket("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"access_token" varchar
  ,"active" bool
  ,"number" varchar
  ,"name" varchar NOT NULL
  ,"description" text NOT NULL
  ,"user_id" int4
  ,"stage_id" int4
  ,"partner_id" int4
  ,"partner_name" varchar
  ,"partner_email" varchar
  ,"last_stage_update" timestamp
  ,"assigned_date" timestamp
  ,"closed_date" timestamp
  ,"unattended" bool
  ,"company_id" int4
  ,"channel_id" int4
  ,"category_id" int4
  ,"team_id" int4
  ,"priority" varchar
  ,"color" int4
  ,"kanban_state" varchar
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"type_id" int4
  ,"project_id" int4
  ,"task_id" int4
  ,"planned_hours" float8
  ,"progress" float8
  ,"remaining_hours" float8
  ,"total_hours" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket');



drop foreign table if exists odoo12_helpdesk_ticket_category;


create FOREIGN TABLE odoo12_helpdesk_ticket_category("id" int4 NOT NULL
  ,"active" bool
  ,"name" varchar NOT NULL
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_category');



drop foreign table if exists odoo12_helpdesk_ticket_category_helpdesk_ticket_team_rel;


create FOREIGN TABLE odoo12_helpdesk_ticket_category_helpdesk_ticket_team_rel("helpdesk_ticket_team_id" int4 NOT NULL
  ,"helpdesk_ticket_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_category_helpdesk_ticket_team_rel');



drop foreign table if exists odoo12_helpdesk_ticket_channel;


create FOREIGN TABLE odoo12_helpdesk_ticket_channel("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_channel');



drop foreign table if exists odoo12_helpdesk_ticket_helpdesk_ticket_tag_rel;


create FOREIGN TABLE odoo12_helpdesk_ticket_helpdesk_ticket_tag_rel("helpdesk_ticket_id" int4 NOT NULL
  ,"helpdesk_ticket_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_helpdesk_ticket_tag_rel');



drop foreign table if exists odoo12_helpdesk_ticket_stage;


create FOREIGN TABLE odoo12_helpdesk_ticket_stage("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" text
  ,"sequence" int4
  ,"active" bool
  ,"unattended" bool
  ,"closed" bool
  ,"portal_user_can_close" bool
  ,"mail_template_id" int4
  ,"fold" bool
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_stage');



drop foreign table if exists odoo12_helpdesk_ticket_tag;


create FOREIGN TABLE odoo12_helpdesk_ticket_tag("id" int4 NOT NULL
  ,"name" varchar
  ,"color" int4
  ,"active" bool
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_tag');



drop foreign table if exists odoo12_helpdesk_ticket_team;


create FOREIGN TABLE odoo12_helpdesk_ticket_team("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"alias_id" int4 NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"allow_timesheet" bool
  ,"default_project_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team');



drop foreign table if exists odoo12_helpdesk_ticket_team_helpdesk_ticket_type_rel;


create FOREIGN TABLE odoo12_helpdesk_ticket_team_helpdesk_ticket_type_rel("helpdesk_ticket_type_id" int4 NOT NULL
  ,"helpdesk_ticket_team_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team_helpdesk_ticket_type_rel');



drop foreign table if exists odoo12_helpdesk_ticket_team_res_users_rel;


create FOREIGN TABLE odoo12_helpdesk_ticket_team_res_users_rel("helpdesk_ticket_team_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team_res_users_rel');



drop foreign table if exists odoo12_helpdesk_ticket_type;


create FOREIGN TABLE odoo12_helpdesk_ticket_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_type');



drop foreign table if exists odoo12_hr_applicant;


create FOREIGN TABLE odoo12_hr_applicant("id" int4 NOT NULL
  ,"campaign_id" int4
  ,"source_id" int4
  ,"medium_id" int4
  ,"message_main_attachment_id" int4
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"description" text
  ,"email_from" varchar(128)
  ,"email_cc" text
  ,"probability" float8
  ,"partner_id" int4
  ,"create_date" timestamp
  ,"stage_id" int4
  ,"last_stage_id" int4
  ,"company_id" int4
  ,"user_id" int4
  ,"date_closed" timestamp
  ,"date_open" timestamp
  ,"date_last_stage_update" timestamp
  ,"priority" varchar
  ,"job_id" int4
  ,"salary_proposed_extra" varchar
  ,"salary_expected_extra" varchar
  ,"salary_proposed" float8
  ,"salary_expected" float8
  ,"availability" date
  ,"partner_name" varchar
  ,"partner_phone" varchar(32)
  ,"partner_mobile" varchar(32)
  ,"type_id" int4
  ,"department_id" int4
  ,"reference" varchar
  ,"delay_close" float8
  ,"color" int4
  ,"emp_id" int4
  ,"kanban_state" varchar NOT NULL
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"response_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_applicant');



drop foreign table if exists odoo12_hr_applicant_category;


create FOREIGN TABLE odoo12_hr_applicant_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_applicant_category');



drop foreign table if exists odoo12_hr_applicant_hr_applicant_category_rel;


create FOREIGN TABLE odoo12_hr_applicant_hr_applicant_category_rel("hr_applicant_id" int4 NOT NULL
  ,"hr_applicant_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_applicant_hr_applicant_category_rel');



drop foreign table if exists odoo12_hr_contract;


create FOREIGN TABLE odoo12_hr_contract("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"employee_id" int4
  ,"department_id" int4
  ,"type_id" int4 NOT NULL
  ,"job_id" int4
  ,"date_start" date NOT NULL
  ,"date_end" date
  ,"trial_date_end" date
  ,"resource_calendar_id" int4 NOT NULL
  ,"wage" numeric NOT NULL
  ,"advantages" text
  ,"notes" text
  ,"state" varchar
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"struct_id" int4
  ,"schedule_pay" varchar
  ,"analytic_account_id" int4
  ,"journal_id" int4
  ,"message_main_attachment_id" int4
  ,"active" bool
  ,"reported_to_secretariat" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_contract');



drop foreign table if exists odoo12_hr_contract_advantage_template;


create FOREIGN TABLE odoo12_hr_contract_advantage_template("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"lower_bound" float8
  ,"upper_bound" float8
  ,"default_value" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_contract_advantage_template');



drop foreign table if exists odoo12_hr_contract_type;


create FOREIGN TABLE odoo12_hr_contract_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_contract_type');



drop foreign table if exists odoo12_hr_contribution_register;


create FOREIGN TABLE odoo12_hr_contribution_register("id" int4 NOT NULL
  ,"company_id" int4
  ,"partner_id" int4
  ,"name" varchar NOT NULL
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_contribution_register');



drop foreign table if exists odoo12_hr_course;


create FOREIGN TABLE odoo12_hr_course("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"name" varchar NOT NULL
  ,"category_id" int4 NOT NULL
  ,"start_date" date
  ,"end_date" date
  ,"currency_id" int4
  ,"cost" numeric NOT NULL
  ,"authorized_by" int4 NOT NULL
  ,"permanence" bool
  ,"permanence_time" varchar
  ,"state" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_course');



drop foreign table if exists odoo12_hr_course_attendee;


create FOREIGN TABLE odoo12_hr_course_attendee("id" int4 NOT NULL
  ,"course_id" int4 NOT NULL
  ,"employee_id" int4
  ,"result" varchar
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_course_attendee');



drop foreign table if exists odoo12_hr_course_category;


create FOREIGN TABLE odoo12_hr_course_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_course_category');



drop foreign table if exists odoo12_hr_course_hr_employee_rel;


create FOREIGN TABLE odoo12_hr_course_hr_employee_rel("hr_course_id" int4 NOT NULL
  ,"hr_employee_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_course_hr_employee_rel');



drop foreign table if exists odoo12_hr_department;


create FOREIGN TABLE odoo12_hr_department("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"complete_name" varchar
  ,"active" bool
  ,"company_id" int4
  ,"parent_id" int4
  ,"manager_id" int4
  ,"note" text
  ,"color" int4
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_department');



drop foreign table if exists odoo12_hr_department_mail_channel_rel;


create FOREIGN TABLE odoo12_hr_department_mail_channel_rel("mail_channel_id" int4 NOT NULL
  ,"hr_department_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_department_mail_channel_rel');



drop foreign table if exists odoo12_hr_employee;


create FOREIGN TABLE odoo12_hr_employee("id" int4 NOT NULL
  ,"name" varchar
  ,"active" bool
  ,"address_home_id" int4
  ,"country_id" int4
  ,"gender" varchar
  ,"marital" varchar
  ,"birthday" date
  ,"ssnid" varchar
  ,"sinid" varchar
  ,"identification_id" varchar
  ,"passport_id" varchar
  ,"bank_account_id" int4
  ,"permit_no" varchar
  ,"visa_no" varchar
  ,"visa_expire" date
  ,"address_id" int4
  ,"work_phone" varchar
  ,"mobile_phone" varchar
  ,"work_email" varchar
  ,"work_location" varchar
  ,"job_id" int4
  ,"department_id" int4
  ,"parent_id" int4
  ,"coach_id" int4
  ,"notes" text
  ,"color" int4
  ,"message_last_post" timestamp
  ,"resource_id" int4 NOT NULL
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"manager" bool
  ,"medic_exam" date
  ,"place_of_birth" varchar
  ,"children" int4
  ,"vehicle" varchar
  ,"km_home_work" int4
  ,"timesheet_cost" numeric
  ,"message_main_attachment_id" int4
  ,"resource_calendar_id" int4
  ,"user_id" int4
  ,"spouse_complete_name" varchar
  ,"spouse_birthdate" date
  ,"country_of_birth" int4
  ,"additional_note" text
  ,"certificate" varchar
  ,"study_field" varchar
  ,"study_school" varchar
  ,"emergency_contact" varchar
  ,"emergency_phone" varchar
  ,"google_drive_link" varchar
  ,"job_title" varchar
  ,"expense_manager_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_employee');



drop foreign table if exists odoo12_hr_employee_category;


create FOREIGN TABLE odoo12_hr_employee_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_employee_category');



drop foreign table if exists odoo12_hr_employee_group_rel;


create FOREIGN TABLE odoo12_hr_employee_group_rel("payslip_id" int4 NOT NULL
  ,"employee_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_employee_group_rel');



drop foreign table if exists odoo12_hr_expense;


create FOREIGN TABLE odoo12_hr_expense("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"date" date
  ,"employee_id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"product_uom_id" int4 NOT NULL
  ,"unit_amount" numeric NOT NULL
  ,"quantity" numeric NOT NULL
  ,"untaxed_amount" numeric
  ,"total_amount" numeric
  ,"company_id" int4
  ,"currency_id" int4
  ,"analytic_account_id" int4
  ,"account_id" int4
  ,"description" text
  ,"payment_mode" varchar
  ,"state" varchar
  ,"sheet_id" int4
  ,"reference" varchar
  ,"is_refused" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"sale_order_id" int4
  ,"message_main_attachment_id" int4
  ,"company_currency_id" int4
  ,"total_amount_company" numeric
  ,"invoice_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense');



drop foreign table if exists odoo12_hr_expense_create_invoice;


create FOREIGN TABLE odoo12_hr_expense_create_invoice("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_create_invoice');



drop foreign table if exists odoo12_hr_expense_hr_expense_create_invoice_rel;


create FOREIGN TABLE odoo12_hr_expense_hr_expense_create_invoice_rel("hr_expense_create_invoice_id" int4 NOT NULL
  ,"hr_expense_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_hr_expense_create_invoice_rel');



drop foreign table if exists odoo12_hr_expense_hr_expense_refuse_wizard_rel;


create FOREIGN TABLE odoo12_hr_expense_hr_expense_refuse_wizard_rel("hr_expense_refuse_wizard_id" int4 NOT NULL
  ,"hr_expense_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_hr_expense_refuse_wizard_rel');



drop foreign table if exists odoo12_hr_expense_refuse_wizard;


create FOREIGN TABLE odoo12_hr_expense_refuse_wizard("id" int4 NOT NULL
  ,"reason" varchar NOT NULL
  ,"hr_expense_sheet_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_refuse_wizard');



drop foreign table if exists odoo12_hr_expense_sheet;


create FOREIGN TABLE odoo12_hr_expense_sheet("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"state" varchar NOT NULL
  ,"employee_id" int4 NOT NULL
  ,"address_id" int4
  ,"user_id" int4
  ,"total_amount" numeric
  ,"company_id" int4
  ,"currency_id" int4
  ,"journal_id" int4
  ,"bank_journal_id" int4
  ,"accounting_date" date
  ,"account_move_id" int4
  ,"department_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_sheet');



drop foreign table if exists odoo12_hr_expense_sheet_register_payment_wizard;


create FOREIGN TABLE odoo12_hr_expense_sheet_register_payment_wizard("id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"payment_method_id" int4 NOT NULL
  ,"amount" numeric NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"payment_date" date NOT NULL
  ,"communication" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"partner_bank_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_expense_sheet_register_payment_wizard');



drop foreign table if exists odoo12_hr_holiday_public_state_rel;


create FOREIGN TABLE odoo12_hr_holiday_public_state_rel("line_id" int4 NOT NULL
  ,"state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holiday_public_state_rel');



drop foreign table if exists odoo12_hr_holidays;


create FOREIGN TABLE odoo12_hr_holidays("id" int4 NOT NULL
  ,"name" varchar
  ,"state" varchar
  ,"payslip_status" bool
  ,"report_note" text
  ,"user_id" int4
  ,"date_from" timestamp
  ,"date_to" timestamp
  ,"holiday_status_id" int4 NOT NULL
  ,"employee_id" int4
  ,"manager_id" int4
  ,"notes" text
  ,"number_of_days" float8
  ,"openupgrade_legacy_12_0_number_of_days" float8
  ,"meeting_id" int4
  ,"openupgrade_legacy_12_0_type" varchar NOT NULL
  ,"parent_id" int4
  ,"department_id" int4
  ,"category_id" int4
  ,"holiday_type" varchar NOT NULL
  ,"first_approver_id" int4
  ,"second_approver_id" int4
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays');



drop foreign table if exists odoo12_hr_holidays_public;


create FOREIGN TABLE odoo12_hr_holidays_public("id" int4 NOT NULL
  ,"display_name" varchar
  ,"year" int4 NOT NULL
  ,"country_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_public');



drop foreign table if exists odoo12_hr_holidays_public_line;


create FOREIGN TABLE odoo12_hr_holidays_public_line("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date" date NOT NULL
  ,"year_id" int4 NOT NULL
  ,"variable_date" bool
  ,"meeting_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_public_line');



drop foreign table if exists odoo12_hr_holidays_public_line_transient;


create FOREIGN TABLE odoo12_hr_holidays_public_line_transient("id" int4 NOT NULL
  ,"name" varchar
  ,"date" date
  ,"wizard_id" int4
  ,"line_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_public_line_transient');



drop foreign table if exists odoo12_hr_holidays_public_line_transient_res_country_state_rel;


create FOREIGN TABLE odoo12_hr_holidays_public_line_transient_res_country_state_rel("hr_holidays_public_line_transient_id" int4 NOT NULL
  ,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_public_line_transient_res_country_state_rel');



drop foreign table if exists odoo12_hr_holidays_remaining_leaves_user;


create FOREIGN TABLE odoo12_hr_holidays_remaining_leaves_user("id" int4
  ,"name" varchar
  ,"no_of_leaves" float8
  ,"user_id" int4
  ,"leave_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_remaining_leaves_user');



drop foreign table if exists odoo12_hr_holidays_summary_dept;


create FOREIGN TABLE odoo12_hr_holidays_summary_dept("id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"holiday_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_summary_dept');



drop foreign table if exists odoo12_hr_holidays_summary_employee;


create FOREIGN TABLE odoo12_hr_holidays_summary_employee("id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"holiday_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_holidays_summary_employee');



drop foreign table if exists odoo12_hr_job;


create FOREIGN TABLE odoo12_hr_job("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"expected_employees" int4
  ,"no_of_employee" int4
  ,"no_of_recruitment" int4
  ,"no_of_hired_employee" int4
  ,"description" text
  ,"requirements" text
  ,"department_id" int4
  ,"company_id" int4
  ,"state" varchar NOT NULL
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4
  ,"address_id" int4
  ,"manager_id" int4
  ,"user_id" int4
  ,"hr_responsible_id" int4
  ,"alias_id" int4 NOT NULL
  ,"color" int4
  ,"survey_id" int4
  ,"website_meta_title" varchar
  ,"website_meta_description" text
  ,"website_meta_keywords" varchar
  ,"website_meta_og_img" varchar
  ,"website_id" int4
  ,"is_published" bool
  ,"website_description" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_job');



drop foreign table if exists odoo12_hr_leave;


create FOREIGN TABLE odoo12_hr_leave("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"name" varchar
  ,"state" varchar
  ,"payslip_status" bool
  ,"report_note" text
  ,"user_id" int4
  ,"holiday_status_id" int4 NOT NULL
  ,"employee_id" int4
  ,"manager_id" int4
  ,"department_id" int4
  ,"notes" text
  ,"date_from" timestamp NOT NULL
  ,"date_to" timestamp NOT NULL
  ,"number_of_days" float8
  ,"meeting_id" int4
  ,"parent_id" int4
  ,"holiday_type" varchar NOT NULL
  ,"category_id" int4
  ,"mode_company_id" int4
  ,"first_approver_id" int4
  ,"second_approver_id" int4
  ,"request_date_from" date
  ,"request_date_to" date
  ,"request_hour_from" int4
  ,"request_hour_to" int4
  ,"request_date_from_period" varchar
  ,"request_unit_half" bool
  ,"request_unit_hours" bool
  ,"request_unit_custom" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_leave');



drop foreign table if exists odoo12_hr_leave_allocation;


create FOREIGN TABLE odoo12_hr_leave_allocation("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"name" varchar
  ,"state" varchar
  ,"date_from" timestamp
  ,"date_to" timestamp
  ,"holiday_status_id" int4 NOT NULL
  ,"employee_id" int4
  ,"notes" text
  ,"number_of_days" float8
  ,"parent_id" int4
  ,"first_approver_id" int4
  ,"second_approver_id" int4
  ,"holiday_type" varchar NOT NULL
  ,"mode_company_id" int4
  ,"department_id" int4
  ,"category_id" int4
  ,"accrual" bool
  ,"accrual_limit" int4
  ,"number_per_interval" float8
  ,"interval_number" int4
  ,"unit_per_interval" varchar
  ,"interval_unit" varchar
  ,"nextcall" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_leave_allocation');



drop foreign table if exists odoo12_hr_leave_report;
create FOREIGN TABLE odoo12_hr_leave_report("id" int8
  ,"employee_id" int4
  ,"name" varchar
  ,"number_of_days" float8
  ,"type" text
  ,"category_id" int4
  ,"department_id" int4
  ,"holiday_status_id" int4
  ,"state" varchar
  ,"holiday_type" varchar
  ,"date_from" timestamp
  ,"date_to" timestamp
  ,"payslip_status" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_leave_report');



drop foreign table if exists odoo12_hr_leave_type;


create FOREIGN TABLE odoo12_hr_leave_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"categ_id" int4
  ,"color_name" varchar NOT NULL
  ,"openupgrade_legacy_12_0_limit" bool
  ,"active" bool
  ,"openupgrade_legacy_12_0_double_validation" bool
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"timesheet_generate" bool
  ,"timesheet_project_id" int4
  ,"timesheet_task_id" int4
  ,"validity_start" date
  ,"sequence" int4
  ,"validation_type" varchar
  ,"allocation_type" varchar
  ,"validity_stop" date
  ,"time_type" varchar
  ,"request_unit" varchar NOT NULL
  ,"unpaid" bool
  ,"exclude_public_holidays" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_leave_type');



drop foreign table if exists odoo12_hr_payroll_structure;


create FOREIGN TABLE odoo12_hr_payroll_structure("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"note" text
  ,"parent_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payroll_structure');



drop foreign table if exists odoo12_hr_payslip;


create FOREIGN TABLE odoo12_hr_payslip("id" int4 NOT NULL
  ,"struct_id" int4
  ,"name" varchar
  ,"number" varchar
  ,"employee_id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"state" varchar
  ,"company_id" int4
  ,"paid" bool
  ,"note" text
  ,"contract_id" int4
  ,"credit_note" bool
  ,"payslip_run_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"date" date
  ,"journal_id" int4 NOT NULL
  ,"move_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip');



drop foreign table if exists odoo12_hr_payslip_employees;


create FOREIGN TABLE odoo12_hr_payslip_employees("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip_employees');



drop foreign table if exists odoo12_hr_payslip_input;


create FOREIGN TABLE odoo12_hr_payslip_input("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"payslip_id" int4 NOT NULL
  ,"sequence" int4 NOT NULL
  ,"code" varchar NOT NULL
  ,"amount" float8
  ,"contract_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip_input');



drop foreign table if exists odoo12_hr_payslip_line;
create FOREIGN TABLE odoo12_hr_payslip_line("id" int4 NOT NULL
  ,"slip_id" int4 NOT NULL
  ,"salary_rule_id" int4 NOT NULL
  ,"employee_id" int4 NOT NULL
  ,"contract_id" int4 NOT NULL
  ,"rate" numeric
  ,"amount" numeric
  ,"quantity" numeric
  ,"total" numeric
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"sequence" int4 NOT NULL
  ,"category_id" int4 NOT NULL
  ,"active" bool
  ,"appears_on_payslip" bool
  ,"parent_rule_id" int4
  ,"company_id" int4
  ,"condition_select" varchar NOT NULL
  ,"condition_range" varchar
  ,"condition_python" text NOT NULL
  ,"condition_range_min" float8
  ,"condition_range_max" float8
  ,"amount_select" varchar NOT NULL
  ,"amount_fix" numeric
  ,"amount_percentage" numeric
  ,"amount_python_compute" text
  ,"amount_percentage_base" varchar
  ,"register_id" int4
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"analytic_account_id" int4
  ,"account_tax_id" int4
  ,"account_debit" int4
  ,"account_credit" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip_line');



drop foreign table if exists odoo12_hr_payslip_run;


create FOREIGN TABLE odoo12_hr_payslip_run("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"state" varchar
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"credit_note" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip_run');



drop foreign table if exists odoo12_hr_payslip_worked_days;


create FOREIGN TABLE odoo12_hr_payslip_worked_days("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"payslip_id" int4 NOT NULL
  ,"sequence" int4 NOT NULL
  ,"code" varchar NOT NULL
  ,"number_of_days" float8
  ,"number_of_hours" float8
  ,"contract_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_payslip_worked_days');



drop foreign table if exists odoo12_hr_recruitment_degree;


create FOREIGN TABLE odoo12_hr_recruitment_degree("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_recruitment_degree');



drop foreign table if exists odoo12_hr_recruitment_source;


create FOREIGN TABLE odoo12_hr_recruitment_source("id" int4 NOT NULL
  ,"source_id" int4 NOT NULL
  ,"job_id" int4
  ,"alias_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_recruitment_source');



drop foreign table if exists odoo12_hr_recruitment_stage;


create FOREIGN TABLE odoo12_hr_recruitment_stage("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"job_id" int4
  ,"requirements" text
  ,"template_id" int4
  ,"fold" bool
  ,"legend_blocked" varchar NOT NULL
  ,"legend_done" varchar NOT NULL
  ,"legend_normal" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_recruitment_stage');



drop foreign table if exists odoo12_hr_rule_input;


create FOREIGN TABLE odoo12_hr_rule_input("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"input_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_rule_input');



drop foreign table if exists odoo12_hr_salary_rule;


create FOREIGN TABLE odoo12_hr_salary_rule("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"sequence" int4 NOT NULL
  ,"quantity" varchar
  ,"category_id" int4 NOT NULL
  ,"active" bool
  ,"appears_on_payslip" bool
  ,"parent_rule_id" int4
  ,"company_id" int4
  ,"condition_select" varchar NOT NULL
  ,"condition_range" varchar
  ,"condition_python" text NOT NULL
  ,"condition_range_min" float8
  ,"condition_range_max" float8
  ,"amount_select" varchar NOT NULL
  ,"amount_fix" numeric
  ,"amount_percentage" numeric
  ,"amount_python_compute" text
  ,"amount_percentage_base" varchar
  ,"register_id" int4
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"analytic_account_id" int4
  ,"account_tax_id" int4
  ,"account_debit" int4
  ,"account_credit" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_salary_rule');



drop foreign table if exists odoo12_hr_salary_rule_category;


create FOREIGN TABLE odoo12_hr_salary_rule_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"parent_id" int4
  ,"note" text
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_salary_rule_category');



drop foreign table if exists odoo12_hr_structure_salary_rule_rel;


create FOREIGN TABLE odoo12_hr_structure_salary_rule_rel("struct_id" int4 NOT NULL
  ,"rule_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_structure_salary_rule_rel');



drop foreign table if exists odoo12_hr_timesheet_switch;


create FOREIGN TABLE odoo12_hr_timesheet_switch("id" int4 NOT NULL
  ,"running_timer_id" int4
  ,"name" varchar NOT NULL
  ,"date" date NOT NULL
  ,"amount" numeric NOT NULL
  ,"unit_amount" float8
  ,"product_uom_id" int4
  ,"account_id" int4 NOT NULL
  ,"partner_id" int4
  ,"user_id" int4
  ,"company_id" int4 NOT NULL
  ,"currency_id" int4
  ,"group_id" int4
  ,"product_id" int4
  ,"general_account_id" int4
  ,"move_id" int4
  ,"code" varchar(8)
  ,"ref" varchar
  ,"task_id" int4
  ,"project_id" int4
  ,"employee_id" int4
  ,"department_id" int4
  ,"holiday_id" int4
  ,"so_line" int4
  ,"date_time" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"lead_id" int4
  ,"timesheet_invoice_type" varchar
  ,"timesheet_invoice_id" int4
  ,"ticket_id" int4
  ,"ticket_partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'hr_timesheet_switch');



drop foreign table if exists odoo12_iap_account;


create FOREIGN TABLE odoo12_iap_account("id" int4 NOT NULL
  ,"service_name" varchar
  ,"account_token" varchar
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'iap_account');



drop foreign table if exists odoo12_ir_act_client;


create FOREIGN TABLE odoo12_ir_act_client("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"tag" varchar NOT NULL
  ,"target" varchar
  ,"res_model" varchar
  ,"context" varchar NOT NULL
  ,"params_store" bytea)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_client');



drop foreign table if exists odoo12_ir_act_report_xml;


create FOREIGN TABLE odoo12_ir_act_report_xml("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"model" varchar NOT NULL
  ,"report_type" varchar NOT NULL
  ,"report_name" varchar NOT NULL
  ,"report_file" varchar
  ,"multi" bool
  ,"paperformat_id" int4
  ,"print_report_name" varchar
  ,"attachment_use" bool
  ,"attachment" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_report_xml');



drop foreign table if exists odoo12_ir_act_server;


create FOREIGN TABLE odoo12_ir_act_server("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"usage" varchar NOT NULL
  ,"state" varchar NOT NULL
  ,"sequence" int4
  ,"model_id" int4 NOT NULL
  ,"model_name" varchar
  ,"code" text
  ,"crud_model_id" int4
  ,"link_field_id" int4
  ,"template_id" int4
  ,"activity_type_id" int4
  ,"activity_summary" varchar
  ,"activity_note" text
  ,"activity_date_deadline_range" int4
  ,"activity_date_deadline_range_type" varchar
  ,"activity_user_type" varchar NOT NULL
  ,"activity_user_id" int4
  ,"activity_user_field_name" varchar
  ,"website_path" varchar
  ,"website_published" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server');



drop foreign table if exists odoo12_ir_act_server_mail_channel_rel;


create FOREIGN TABLE odoo12_ir_act_server_mail_channel_rel("ir_act_server_id" int4 NOT NULL
  ,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server_mail_channel_rel');



drop foreign table if exists odoo12_ir_act_server_res_partner_rel;


create FOREIGN TABLE odoo12_ir_act_server_res_partner_rel("ir_act_server_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server_res_partner_rel');



drop foreign table if exists odoo12_ir_act_url;


create FOREIGN TABLE odoo12_ir_act_url("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"url" text NOT NULL
  ,"target" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_url');



drop foreign table if exists odoo12_ir_act_window;


create FOREIGN TABLE odoo12_ir_act_window("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"view_id" int4
  ,"domain" varchar
  ,"context" varchar NOT NULL
  ,"res_id" int4
  ,"res_model" varchar NOT NULL
  ,"src_model" varchar
  ,"target" varchar
  ,"view_mode" varchar NOT NULL
  ,"view_type" varchar NOT NULL
  ,"usage" varchar
  ,"limit" int4
  ,"search_view_id" int4
  ,"filter" bool
  ,"auto_search" bool
  ,"multi" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window');



drop foreign table if exists odoo12_ir_act_window_group_rel;


create FOREIGN TABLE odoo12_ir_act_window_group_rel("act_id" int4 NOT NULL
  ,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window_group_rel');



drop foreign table if exists odoo12_ir_act_window_view;


create FOREIGN TABLE odoo12_ir_act_window_view("id" int4 NOT NULL
  ,"sequence" int4
  ,"view_id" int4
  ,"view_mode" varchar NOT NULL
  ,"act_window_id" int4
  ,"multi" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window_view');



drop foreign table if exists odoo12_ir_actions;


create FOREIGN TABLE odoo12_ir_actions("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"help" text
  ,"binding_model_id" int4
  ,"binding_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_actions');



drop foreign table if exists odoo12_ir_actions_todo;
create FOREIGN TABLE odoo12_ir_actions_todo("id" int4 NOT NULL
  ,"action_id" int4 NOT NULL
  ,"sequence" int4
  ,"state" varchar NOT NULL
  ,"name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_actions_todo');



drop foreign table if exists odoo12_ir_attachment;


create FOREIGN TABLE odoo12_ir_attachment("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"datas_fname" varchar
  ,"description" text
  ,"res_name" varchar
  ,"res_model" varchar
  ,"res_field" varchar
  ,"res_id" int4
  ,"create_date" timestamp
  ,"create_uid" int4
  ,"company_id" int4
  ,"type" varchar NOT NULL
  ,"url" varchar(1024)
  ,"public" bool
  ,"access_token" varchar
  ,"db_datas" bytea
  ,"store_fname" varchar
  ,"file_size" int4
  ,"checksum" varchar(40)
  ,"mimetype" varchar
  ,"index_content" text
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"res_model_name" varchar
  ,"active" bool
  ,"key" varchar
  ,"website_id" int4
  ,"theme_template_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_attachment');



drop foreign table if exists odoo12_ir_config_parameter;


create FOREIGN TABLE odoo12_ir_config_parameter("id" int4 NOT NULL
  ,"key" varchar NOT NULL
  ,"value" text NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_config_parameter');



drop foreign table if exists odoo12_ir_cron;


create FOREIGN TABLE odoo12_ir_cron("id" int4 NOT NULL
  ,"ir_actions_server_id" int4 NOT NULL
  ,"cron_name" varchar
  ,"user_id" int4 NOT NULL
  ,"active" bool
  ,"interval_number" int4
  ,"interval_type" varchar
  ,"numbercall" int4
  ,"doall" bool
  ,"nextcall" timestamp NOT NULL
  ,"priority" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_cron');



drop foreign table if exists odoo12_ir_default;


create FOREIGN TABLE odoo12_ir_default("id" int4 NOT NULL
  ,"field_id" int4 NOT NULL
  ,"user_id" int4
  ,"company_id" int4
  ,"condition" varchar
  ,"json_value" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_default');



drop foreign table if exists odoo12_ir_demo;


create FOREIGN TABLE odoo12_ir_demo("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo');



drop foreign table if exists odoo12_ir_demo_failure;
create FOREIGN TABLE odoo12_ir_demo_failure("id" int4 NOT NULL
  ,"module_id" int4 NOT NULL
  ,"error" varchar
  ,"wizard_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo_failure');



drop foreign table if exists odoo12_ir_demo_failure_wizard;


create FOREIGN TABLE odoo12_ir_demo_failure_wizard("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo_failure_wizard');



drop foreign table if exists odoo12_ir_exports;


create FOREIGN TABLE odoo12_ir_exports("id" int4 NOT NULL
  ,"name" varchar
  ,"resource" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_exports');



drop foreign table if exists odoo12_ir_exports_line;
create FOREIGN TABLE odoo12_ir_exports_line("id" int4 NOT NULL
  ,"name" varchar
  ,"export_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_exports_line');



drop foreign table if exists odoo12_ir_filters;


create FOREIGN TABLE odoo12_ir_filters("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"user_id" int4
  ,"domain" text NOT NULL
  ,"context" text NOT NULL
  ,"sort" text NOT NULL
  ,"model_id" varchar NOT NULL
  ,"is_default" bool
  ,"action_id" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_filters');



drop foreign table if exists odoo12_ir_logging;


create FOREIGN TABLE odoo12_ir_logging("id" int4 NOT NULL
  ,"create_date" timestamp
  ,"create_uid" int4
  ,"name" varchar NOT NULL
  ,"type" varchar NOT NULL
  ,"dbname" varchar
  ,"level" varchar
  ,"message" text NOT NULL
  ,"path" varchar NOT NULL
  ,"func" varchar NOT NULL
  ,"line" varchar NOT NULL
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_logging');



drop foreign table if exists odoo12_ir_mail_server;


create FOREIGN TABLE odoo12_ir_mail_server("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"smtp_host" varchar NOT NULL
  ,"smtp_port" int4 NOT NULL
  ,"smtp_user" varchar
  ,"smtp_pass" varchar
  ,"smtp_encryption" varchar NOT NULL
  ,"smtp_debug" bool
  ,"sequence" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_mail_server');



drop foreign table if exists odoo12_ir_model;


create FOREIGN TABLE odoo12_ir_model("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"model" varchar NOT NULL
  ,"info" text
  ,"state" varchar
  ,"transient" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"is_mail_thread" bool
  ,"website_form_access" bool
  ,"website_form_default_field_id" int4
  ,"website_form_label" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model');



drop foreign table if exists odoo12_ir_model_access;
create FOREIGN TABLE odoo12_ir_model_access("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"model_id" int4 NOT NULL
  ,"group_id" int4
  ,"perm_read" bool
  ,"perm_write" bool
  ,"perm_create" bool
  ,"perm_unlink" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_access');



drop foreign table if exists odoo12_ir_model_constraint;


create FOREIGN TABLE odoo12_ir_model_constraint("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"definition" varchar
  ,"model" int4 NOT NULL
  ,"module" int4 NOT NULL
  ,"type" varchar(1) NOT NULL
  ,"date_update" timestamp
  ,"date_init" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_constraint');



drop foreign table if exists odoo12_ir_model_data;


create FOREIGN TABLE odoo12_ir_model_data("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"write_uid" int4
  ,"noupdate" bool
  ,"name" varchar NOT NULL
  ,"date_init" timestamp
  ,"date_update" timestamp
  ,"module" varchar NOT NULL
  ,"model" varchar NOT NULL
  ,"res_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_data');



drop foreign table if exists odoo12_ir_model_fields;
create FOREIGN TABLE odoo12_ir_model_fields("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"complete_name" varchar
  ,"model" varchar NOT NULL
  ,"relation" varchar
  ,"relation_field" varchar
  ,"model_id" int4 NOT NULL
  ,"field_description" varchar NOT NULL
  ,"help" text
  ,"ttype" varchar NOT NULL
  ,"selection" varchar
  ,"copied" bool
  ,"related" varchar
  ,"required" bool
  ,"readonly" bool
  ,"index" bool
  ,"translate" bool
  ,"size" int4
  ,"state" varchar NOT NULL
  ,"on_delete" varchar
  ,"domain" varchar
  ,"selectable" bool
  ,"relation_table" varchar
  ,"column1" varchar
  ,"column2" varchar
  ,"compute" text
  ,"depends" varchar
  ,"store" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"track_visibility" varchar
  ,"relation_field_id" int4
  ,"related_field_id" int4
  ,"website_form_blacklisted" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields');



drop foreign table if exists odoo12_ir_model_fields_group_rel;


create FOREIGN TABLE odoo12_ir_model_fields_group_rel("field_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields_group_rel');



drop foreign table if exists odoo12_ir_model_fields_mis_report_query_rel;


create FOREIGN TABLE odoo12_ir_model_fields_mis_report_query_rel("mis_report_query_id" int4 NOT NULL
  ,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields_mis_report_query_rel');



drop foreign table if exists odoo12_ir_model_relation;


create FOREIGN TABLE odoo12_ir_model_relation("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"model" int4 NOT NULL
  ,"module" int4 NOT NULL
  ,"date_update" timestamp
  ,"date_init" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_relation');



drop foreign table if exists odoo12_ir_module_category;


create FOREIGN TABLE odoo12_ir_module_category("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"write_uid" int4
  ,"parent_id" int4
  ,"name" varchar NOT NULL
  ,"description" text
  ,"sequence" int4
  ,"visible" bool
  ,"exclusive" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_category');



drop foreign table if exists odoo12_ir_module_module;


create FOREIGN TABLE odoo12_ir_module_module("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"write_uid" int4
  ,"website" varchar
  ,"summary" varchar
  ,"name" varchar NOT NULL
  ,"author" varchar
  ,"icon" varchar
  ,"state" varchar(16)
  ,"latest_version" varchar
  ,"shortdesc" varchar
  ,"category_id" int4
  ,"description" text
  ,"application" bool
  ,"demo" bool
  ,"web" bool
  ,"license" varchar(32)
  ,"sequence" int4
  ,"auto_install" bool
  ,"maintainer" varchar
  ,"contributors" text
  ,"published_version" varchar
  ,"url" varchar
  ,"menus_by_module" text
  ,"reports_by_module" text
  ,"views_by_module" text
  ,"to_buy" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module');



drop foreign table if exists odoo12_ir_module_module_dependency;


create FOREIGN TABLE odoo12_ir_module_module_dependency("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"write_uid" int4
  ,"name" varchar
  ,"module_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module_dependency');



drop foreign table if exists odoo12_ir_module_module_exclusion;


create FOREIGN TABLE odoo12_ir_module_module_exclusion("id" int4 NOT NULL
  ,"name" varchar
  ,"module_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module_exclusion');



drop foreign table if exists odoo12_ir_property;


create FOREIGN TABLE odoo12_ir_property("id" int4 NOT NULL
  ,"name" varchar
  ,"res_id" varchar
  ,"company_id" int4
  ,"fields_id" int4 NOT NULL
  ,"value_float" float8
  ,"value_integer" int4
  ,"value_text" text
  ,"value_binary" bytea
  ,"value_reference" varchar
  ,"value_datetime" timestamp
  ,"type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_property');



drop foreign table if exists odoo12_ir_rule;


create FOREIGN TABLE odoo12_ir_rule("id" int4 NOT NULL
  ,"name" varchar
  ,"active" bool
  ,"model_id" int4 NOT NULL
  ,"domain_force" text
  ,"perm_read" bool
  ,"perm_write" bool
  ,"perm_create" bool
  ,"perm_unlink" bool
  ,"global" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_rule');



drop foreign table if exists odoo12_ir_sequence;


create FOREIGN TABLE odoo12_ir_sequence("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar
  ,"implementation" varchar NOT NULL
  ,"active" bool
  ,"prefix" varchar
  ,"suffix" varchar
  ,"number_next" int4 NOT NULL
  ,"number_increment" int4 NOT NULL
  ,"padding" int4 NOT NULL
  ,"company_id" int4
  ,"use_date_range" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_sequence');



drop foreign table if exists odoo12_ir_sequence_date_range;


create FOREIGN TABLE odoo12_ir_sequence_date_range("id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"sequence_id" int4 NOT NULL
  ,"number_next" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_sequence_date_range');



drop foreign table if exists odoo12_ir_server_object_lines;


create FOREIGN TABLE odoo12_ir_server_object_lines("id" int4 NOT NULL
  ,"server_id" int4
  ,"col1" int4 NOT NULL
  ,"value" text NOT NULL
  ,"type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_server_object_lines');



drop foreign table if exists odoo12_ir_translation;


create FOREIGN TABLE odoo12_ir_translation("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"res_id" int4
  ,"lang" varchar
  ,"type" varchar
  ,"src" text
  ,"value" text
  ,"module" varchar
  ,"state" varchar
  ,"comments" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_translation');



drop foreign table if exists odoo12_ir_ui_menu;


create FOREIGN TABLE odoo12_ir_ui_menu("id" int4 NOT NULL
  ,"parent_left" int4
  ,"parent_right" int4
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"sequence" int4
  ,"parent_id" int4
  ,"web_icon" varchar
  ,"action" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"parent_path" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_menu');



drop foreign table if exists odoo12_ir_ui_menu_group_rel;


create FOREIGN TABLE odoo12_ir_ui_menu_group_rel("menu_id" int4 NOT NULL
  ,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_menu_group_rel');



drop foreign table if exists odoo12_ir_ui_view;


create FOREIGN TABLE odoo12_ir_ui_view("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"model" varchar
  ,"key" varchar
  ,"priority" int4 NOT NULL
  ,"type" varchar
  ,"arch_db" text
  ,"arch_fs" varchar
  ,"inherit_id" int4
  ,"field_parent" varchar
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"mode" varchar NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"write_uid" int4
  ,"website_meta_title" varchar
  ,"website_meta_description" text
  ,"website_meta_keywords" varchar
  ,"website_meta_og_img" varchar
  ,"customize_show" bool
  ,"website_id" int4
  ,"theme_template_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view');



drop foreign table if exists odoo12_ir_ui_view_custom;


create FOREIGN TABLE odoo12_ir_ui_view_custom("id" int4 NOT NULL
  ,"ref_id" int4 NOT NULL
  ,"user_id" int4 NOT NULL
  ,"arch" text NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view_custom');



drop foreign table if exists odoo12_ir_ui_view_group_rel;


create FOREIGN TABLE odoo12_ir_ui_view_group_rel("view_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view_group_rel');



drop foreign table if exists odoo12_journal_ledger_report_wizard;


create FOREIGN TABLE odoo12_journal_ledger_report_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"move_target" varchar NOT NULL
  ,"foreign_currency" bool
  ,"sort_option" varchar NOT NULL
  ,"group_option" varchar NOT NULL
  ,"with_account_name" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'journal_ledger_report_wizard');



drop foreign table if exists odoo12_l10n_es_aeat_certificate;


create FOREIGN TABLE odoo12_l10n_es_aeat_certificate("id" int4 NOT NULL
  ,"name" varchar
  ,"state" varchar
  ,"file" bytea NOT NULL
  ,"folder" varchar NOT NULL
  ,"date_start" date
  ,"date_end" date
  ,"public_key" varchar
  ,"private_key" varchar
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate');



drop foreign table if exists odoo12_l10n_es_aeat_certificate_password;


create FOREIGN TABLE odoo12_l10n_es_aeat_certificate_password("id" int4 NOT NULL
  ,"password" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate_password');



drop foreign table if exists odoo12_l10n_es_aeat_map_tax;


create FOREIGN TABLE odoo12_l10n_es_aeat_map_tax("id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"model" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax');



drop foreign table if exists odoo12_l10n_es_aeat_map_tax_line;


create FOREIGN TABLE odoo12_l10n_es_aeat_map_tax_line("id" int4 NOT NULL
  ,"field_number" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"map_parent_id" int4 NOT NULL
  ,"move_type" varchar NOT NULL
  ,"field_type" varchar NOT NULL
  ,"sum_type" varchar NOT NULL
  ,"inverse" bool
  ,"to_regularize" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"exigible_type" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax_line');



drop foreign table if exists odoo12_l10n_es_aeat_mod111_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod111_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"number" varchar(3) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"casilla_01" int4
  ,"casilla_04" int4
  ,"casilla_07" int4
  ,"casilla_10" int4
  ,"casilla_11" float8
  ,"casilla_12" float8
  ,"casilla_13" int4
  ,"casilla_14" float8
  ,"casilla_15" float8
  ,"casilla_16" int4
  ,"casilla_17" float8
  ,"casilla_18" float8
  ,"casilla_19" int4
  ,"casilla_20" float8
  ,"casilla_21" float8
  ,"casilla_22" int4
  ,"casilla_23" float8
  ,"casilla_24" float8
  ,"casilla_25" int4
  ,"casilla_26" float8
  ,"casilla_27" float8
  ,"casilla_29" float8
  ,"tipo_declaracion" varchar NOT NULL
  ,"colegio_concertado" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod111_report');



drop foreign table if exists odoo12_l10n_es_aeat_mod115_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod115_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"number" varchar(3) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"casilla_04" float8
  ,"tipo_declaracion" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod115_report');



drop foreign table if exists odoo12_l10n_es_aeat_mod303_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod303_report("id" int4 NOT NULL
  ,"devolucion_mensual" bool
  ,"total_devengado" float8
  ,"total_deducir" float8
  ,"casilla_46" float8
  ,"porcentaje_atribuible_estado" float8
  ,"atribuible_estado" float8
  ,"cuota_compensar" float8
  ,"regularizacion_anual" float8
  ,"casilla_69" float8
  ,"casilla_77" float8
  ,"previous_result" float8
  ,"resultado_liquidacion" float8
  ,"counterpart_account_id" int4
  ,"exonerated_390" varchar NOT NULL
  ,"has_operation_volume" bool
  ,"has_347" bool
  ,"is_voluntary_sii" bool
  ,"main_activity_code" int4
  ,"main_activity_iae" varchar(4)
  ,"other_first_activity_code" int4
  ,"other_first_activity_iae" varchar(4)
  ,"other_second_activity_code" int4
  ,"other_second_activity_iae" varchar(4)
  ,"other_third_activity_code" int4
  ,"other_third_activity_iae" varchar(4)
  ,"other_fourth_activity_code" int4
  ,"other_fourth_activity_iae" varchar(4)
  ,"other_fifth_activity_code" int4
  ,"other_fifth_activity_iae" varchar(4)
  ,"casilla_88" float8
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"number" varchar(3) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"contact_email" varchar(50)
  ,"message_main_attachment_id" int4
  ,"potential_cuota_compensar" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report');



drop foreign table if exists odoo12_l10n_es_aeat_mod303_report_activity_code;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod303_report_activity_code("id" int4 NOT NULL
  ,"period_type" varchar NOT NULL
  ,"code" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report_activity_code');



drop foreign table if exists odoo12_l10n_es_aeat_mod347_move_record;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod347_move_record("id" int4 NOT NULL
  ,"partner_record_id" int4 NOT NULL
  ,"move_id" int4
  ,"move_type" varchar
  ,"invoice_id" int4
  ,"date" date
  ,"amount" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_move_record');



drop foreign table if exists odoo12_l10n_es_aeat_mod347_partner_record;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod347_partner_record("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"access_token" varchar
  ,"report_id" int4
  ,"user_id" int4
  ,"state" varchar
  ,"operation_key" varchar
  ,"partner_id" int4 NOT NULL
  ,"partner_vat" varchar(9)
  ,"representative_vat" varchar(9)
  ,"community_vat" varchar(17)
  ,"partner_country_code" varchar(2)
  ,"partner_state_code" varchar(2)
  ,"first_quarter" numeric
  ,"first_quarter_real_estate_transmission" numeric
  ,"second_quarter" numeric
  ,"second_quarter_real_estate_transmission" numeric
  ,"third_quarter" numeric
  ,"third_quarter_real_estate_transmission" numeric
  ,"fourth_quarter" numeric
  ,"fourth_quarter_real_estate_transmission" numeric
  ,"amount" numeric
  ,"cash_amount" numeric
  ,"real_estate_transmissions_amount" numeric
  ,"insurance_operation" bool
  ,"cash_basis_operation" bool
  ,"tax_person_operation" bool
  ,"related_goods_operation" bool
  ,"bussiness_real_estate_rent" bool
  ,"origin_year" int4
  ,"check_ok" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_partner_record');



drop foreign table if exists odoo12_l10n_es_aeat_mod347_real_estate_record;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod347_real_estate_record("id" int4 NOT NULL
  ,"report_id" int4
  ,"partner_id" int4 NOT NULL
  ,"partner_vat" varchar(32)
  ,"representative_vat" varchar(32)
  ,"amount" numeric
  ,"situation" varchar NOT NULL
  ,"reference" varchar(25)
  ,"address_type" varchar(5)
  ,"address" varchar(50)
  ,"number_type" varchar
  ,"number" int4
  ,"number_calification" varchar
  ,"block" varchar(3)
  ,"portal" varchar(3)
  ,"stairway" varchar(3)
  ,"floor" varchar(3)
  ,"door" varchar(3)
  ,"complement" varchar(40)
  ,"city" varchar(30)
  ,"township" varchar(30)
  ,"township_code" varchar(5)
  ,"state_code" varchar(2)
  ,"postal_code" varchar(5)
  ,"check_ok" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_real_estate_record');



drop foreign table if exists odoo12_l10n_es_aeat_mod347_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod347_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"message_main_attachment_id" int4
  ,"number" varchar(3) NOT NULL
  ,"operations_limit" numeric
  ,"received_cash_limit" numeric
  ,"total_partner_records" int4
  ,"total_amount" float8
  ,"total_cash_amount" float8
  ,"total_real_estate_transmissions_amount" float8
  ,"total_real_estate_records" int4
  ,"total_real_estate_amount" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_report');



drop foreign table if exists odoo12_l10n_es_aeat_mod349_partner_record;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod349_partner_record("id" int4 NOT NULL
  ,"report_id" int4
  ,"partner_id" int4 NOT NULL
  ,"partner_vat" varchar(15)
  ,"country_id" int4
  ,"operation_key" varchar
  ,"total_operation_amount" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record');



drop foreign table if exists odoo12_l10n_es_aeat_mod349_partner_record_detail;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod349_partner_record_detail("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"report_type" varchar
  ,"partner_record_id" int4
  ,"move_line_id" int4 NOT NULL
  ,"amount_untaxed" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record_detail');



drop foreign table if exists odoo12_l10n_es_aeat_mod349_partner_refund;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod349_partner_refund("id" int4 NOT NULL
  ,"report_id" int4
  ,"partner_id" int4 NOT NULL
  ,"partner_vat" varchar(15)
  ,"operation_key" varchar
  ,"country_id" int4
  ,"total_operation_amount" float8
  ,"total_origin_amount" float8
  ,"period_type" varchar
  ,"year" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund');



drop foreign table if exists odoo12_l10n_es_aeat_mod349_partner_refund_detail;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod349_partner_refund_detail("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"report_type" varchar
  ,"refund_id" int4
  ,"refund_line_id" int4 NOT NULL
  ,"amount_untaxed" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund_detail');



drop foreign table if exists odoo12_l10n_es_aeat_mod349_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod349_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"frequency_change" bool
  ,"total_partner_records" int4
  ,"total_partner_records_amount" float8
  ,"total_partner_refunds" int4
  ,"total_partner_refunds_amount" float8
  ,"number" varchar(3) NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_report');



drop foreign table if exists odoo12_l10n_es_aeat_mod390_report;


create FOREIGN TABLE odoo12_l10n_es_aeat_mod390_report("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"number" varchar(3) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"calculation_date" timestamp
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"has_347" bool
  ,"main_activity" varchar(40)
  ,"main_activity_code" varchar
  ,"main_activity_iae" varchar(4)
  ,"other_first_activity" varchar(40)
  ,"other_first_activity_code" varchar
  ,"other_first_activity_iae" varchar(4)
  ,"other_second_activity" varchar(40)
  ,"other_second_activity_code" varchar
  ,"other_second_activity_iae" varchar(4)
  ,"other_third_activity" varchar(40)
  ,"other_third_activity_code" varchar
  ,"other_third_activity_iae" varchar(4)
  ,"other_fourth_activity" varchar(40)
  ,"other_fourth_activity_code" varchar
  ,"other_fourth_activity_iae" varchar(4)
  ,"other_fifth_activity" varchar(40)
  ,"other_fifth_activity_code" varchar
  ,"other_fifth_activity_iae" varchar(4)
  ,"first_representative_name" varchar(80)
  ,"first_representative_vat" varchar(9)
  ,"first_representative_date" date
  ,"first_representative_notary" varchar(12)
  ,"second_representative_name" varchar(80)
  ,"second_representative_vat" varchar(9)
  ,"second_representative_date" date
  ,"second_representative_notary" varchar(12)
  ,"third_representative_name" varchar(80)
  ,"third_representative_vat" varchar(9)
  ,"third_representative_date" date
  ,"third_representative_notary" varchar(12)
  ,"casilla_33" float8
  ,"casilla_34" float8
  ,"casilla_47" float8
  ,"casilla_48" float8
  ,"casilla_49" float8
  ,"casilla_50" float8
  ,"casilla_51" float8
  ,"casilla_52" float8
  ,"casilla_53" float8
  ,"casilla_54" float8
  ,"casilla_55" float8
  ,"casilla_56" float8
  ,"casilla_57" float8
  ,"casilla_58" float8
  ,"casilla_59" float8
  ,"casilla_597" float8
  ,"casilla_598" float8
  ,"casilla_64" float8
  ,"casilla_65" float8
  ,"casilla_85" float8
  ,"casilla_86" float8
  ,"casilla_95" float8
  ,"casilla_97" float8
  ,"casilla_98" float8
  ,"casilla_108" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod390_report');



drop foreign table if exists odoo12_l10n_es_aeat_report_compare_boe_file;


create FOREIGN TABLE odoo12_l10n_es_aeat_report_compare_boe_file("id" int4 NOT NULL
  ,"data" bytea NOT NULL
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file');



drop foreign table if exists odoo12_l10n_es_aeat_report_compare_boe_file_line;


create FOREIGN TABLE odoo12_l10n_es_aeat_report_compare_boe_file_line("id" int4 NOT NULL
  ,"wizard_id" int4 NOT NULL
  ,"export_line_id" int4 NOT NULL
  ,"content" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file_line');



drop foreign table if exists odoo12_l10n_es_aeat_report_export_to_boe;


create FOREIGN TABLE odoo12_l10n_es_aeat_report_export_to_boe("id" int4 NOT NULL
  ,"name" varchar
  ,"data" bytea
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_export_to_boe');



drop foreign table if exists odoo12_l10n_es_aeat_soap;


create FOREIGN TABLE odoo12_l10n_es_aeat_soap("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_soap');



drop foreign table if exists odoo12_l10n_es_aeat_tax_line;


create FOREIGN TABLE odoo12_l10n_es_aeat_tax_line("id" int4 NOT NULL
  ,"res_id" int4 NOT NULL
  ,"field_number" int4
  ,"name" varchar
  ,"amount" numeric
  ,"map_line_id" int4 NOT NULL
  ,"model" varchar NOT NULL
  ,"model_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_tax_line');



drop foreign table if exists odoo12_l10n_es_partner_import_wizard;


create FOREIGN TABLE odoo12_l10n_es_partner_import_wizard("id" int4 NOT NULL
  ,"import_fail" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_partner_import_wizard');



drop foreign table if exists odoo12_l10n_es_vat_book;


create FOREIGN TABLE odoo12_l10n_es_vat_book("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"company_vat" varchar(9) NOT NULL
  ,"previous_number" varchar(13)
  ,"contact_name" varchar(40) NOT NULL
  ,"contact_phone" varchar(9) NOT NULL
  ,"contact_email" varchar(50)
  ,"representative_vat" varchar(9)
  ,"year" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"support_type" varchar NOT NULL
  ,"state" varchar
  ,"name" varchar(13)
  ,"export_config_id" int4
  ,"period_type" varchar NOT NULL
  ,"date_start" date NOT NULL
  ,"date_end" date NOT NULL
  ,"counterpart_account_id" int4
  ,"journal_id" int4
  ,"move_id" int4
  ,"partner_bank_id" int4
  ,"number" varchar(3) NOT NULL
  ,"calculation_date" date
  ,"auto_renumber" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book');



drop foreign table if exists odoo12_l10n_es_vat_book_line;


create FOREIGN TABLE odoo12_l10n_es_vat_book_line("id" int4 NOT NULL
  ,"ref" varchar
  ,"entry_number" int4
  ,"external_ref" varchar
  ,"line_type" varchar
  ,"invoice_date" date
  ,"partner_id" int4
  ,"vat_number" varchar
  ,"vat_book_id" int4
  ,"invoice_id" int4
  ,"move_id" int4
  ,"exception_text" varchar
  ,"base_amount" float8
  ,"total_amount" float8
  ,"special_tax_group" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_line');



drop foreign table if exists odoo12_l10n_es_vat_book_line_tax;


create FOREIGN TABLE odoo12_l10n_es_vat_book_line_tax("id" int4 NOT NULL
  ,"vat_book_line_id" int4 NOT NULL
  ,"base_amount" float8
  ,"tax_id" int4
  ,"tax_amount" float8
  ,"total_amount" float8
  ,"special_tax_group" varchar
  ,"special_tax_id" int4
  ,"special_tax_amount" float8
  ,"total_amount_special_include" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_line_tax');



drop foreign table if exists odoo12_l10n_es_vat_book_summary;


create FOREIGN TABLE odoo12_l10n_es_vat_book_summary("id" int4 NOT NULL
  ,"vat_book_id" int4
  ,"book_type" varchar
  ,"base_amount" float8
  ,"tax_amount" float8
  ,"total_amount" float8
  ,"special_tax_group" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_summary');



drop foreign table if exists odoo12_l10n_es_vat_book_tax_summary;


create FOREIGN TABLE odoo12_l10n_es_vat_book_tax_summary("id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL
  ,"vat_book_id" int4
  ,"book_type" varchar
  ,"base_amount" float8
  ,"tax_amount" float8
  ,"total_amount" float8
  ,"special_tax_group" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_tax_summary');



drop foreign table if exists odoo12_link_tracker;


create FOREIGN TABLE odoo12_link_tracker("id" int4 NOT NULL
  ,"campaign_id" int4
  ,"source_id" int4
  ,"medium_id" int4
  ,"url" varchar NOT NULL
  ,"count" int4
  ,"title" varchar
  ,"favicon" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"mass_mailing_id" int4
  ,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker');



drop foreign table if exists odoo12_link_tracker_click;


create FOREIGN TABLE odoo12_link_tracker_click("id" int4 NOT NULL
  ,"click_date" date
  ,"link_id" int4 NOT NULL
  ,"ip" varchar
  ,"country_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"mail_stat_id" int4
  ,"mass_mailing_id" int4
  ,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker_click');



drop foreign table if exists odoo12_link_tracker_code;


create FOREIGN TABLE odoo12_link_tracker_code("id" int4 NOT NULL
  ,"code" varchar
  ,"link_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker_code');



drop foreign table if exists odoo12_mail_activity;


create FOREIGN TABLE odoo12_mail_activity("id" int4 NOT NULL
  ,"res_id" int4 NOT NULL
  ,"res_model_id" int4 NOT NULL
  ,"res_model" varchar
  ,"res_name" varchar
  ,"activity_type_id" int4
  ,"summary" varchar
  ,"note" text
  ,"feedback" text
  ,"date_deadline" date NOT NULL
  ,"user_id" int4 NOT NULL
  ,"recommended_activity_type_id" int4
  ,"previous_activity_type_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"calendar_event_id" int4
  ,"automated" bool
  ,"create_user_id" int4
  ,"done" bool
  ,"active" bool
  ,"date_done" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity');



drop foreign table if exists odoo12_mail_activity_rel;


create FOREIGN TABLE odoo12_mail_activity_rel("activity_id" int4 NOT NULL
  ,"recommended_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_rel');



drop foreign table if exists odoo12_mail_activity_type;


create FOREIGN TABLE odoo12_mail_activity_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"summary" varchar
  ,"sequence" int4
  ,"delay_count" int4
  ,"icon" varchar
  ,"res_model_id" int4
  ,"category" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"active" bool
  ,"delay_unit" varchar NOT NULL
  ,"delay_from" varchar NOT NULL
  ,"decoration_type" varchar
  ,"default_next_type_id" int4
  ,"force_next" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_type');



drop foreign table if exists odoo12_mail_activity_type_mail_template_rel;


create FOREIGN TABLE odoo12_mail_activity_type_mail_template_rel("mail_activity_type_id" int4 NOT NULL
  ,"mail_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_type_mail_template_rel');



drop foreign table if exists odoo12_mail_alias;


create FOREIGN TABLE odoo12_mail_alias("id" int4 NOT NULL
  ,"alias_name" varchar
  ,"alias_model_id" int4 NOT NULL
  ,"alias_user_id" int4
  ,"alias_defaults" text NOT NULL
  ,"alias_force_thread_id" int4
  ,"alias_parent_model_id" int4
  ,"alias_parent_thread_id" int4
  ,"alias_contact" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_alias');



drop foreign table if exists odoo12_mail_blacklist;


create FOREIGN TABLE odoo12_mail_blacklist("id" int4 NOT NULL
  ,"email" varchar NOT NULL
  ,"active" bool
  ,"message_main_attachment_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_blacklist');



drop foreign table if exists odoo12_mail_channel;


create FOREIGN TABLE odoo12_mail_channel("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"channel_type" varchar
  ,"description" text
  ,"uuid" varchar(50)
  ,"email_send" bool
  ,"public" varchar NOT NULL
  ,"group_public_id" int4
  ,"message_last_post" timestamp
  ,"alias_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4
  ,"moderation" bool
  ,"moderation_notify" bool
  ,"moderation_notify_msg" text
  ,"moderation_guidelines" bool
  ,"moderation_guidelines_msg" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel');



drop foreign table if exists odoo12_mail_channel_mail_wizard_invite_rel;


create FOREIGN TABLE odoo12_mail_channel_mail_wizard_invite_rel("mail_wizard_invite_id" int4 NOT NULL
  ,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_mail_wizard_invite_rel');



drop foreign table if exists odoo12_mail_channel_moderator_rel;


create FOREIGN TABLE odoo12_mail_channel_moderator_rel("mail_channel_id" int4 NOT NULL
  ,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_moderator_rel');



drop foreign table if exists odoo12_mail_channel_partner;


create FOREIGN TABLE odoo12_mail_channel_partner("id" int4 NOT NULL
  ,"partner_id" int4
  ,"channel_id" int4
  ,"seen_message_id" int4
  ,"fold_state" varchar
  ,"is_minimized" bool
  ,"is_pinned" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_partner');



drop foreign table if exists odoo12_mail_channel_res_groups_rel;


create FOREIGN TABLE odoo12_mail_channel_res_groups_rel("mail_channel_id" int4 NOT NULL
  ,"res_groups_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_res_groups_rel');



drop foreign table if exists odoo12_mail_compose_message;


create FOREIGN TABLE odoo12_mail_compose_message("id" int4 NOT NULL
  ,"date" timestamp
  ,"body" text
  ,"parent_id" int4
  ,"model" varchar
  ,"res_id" int4
  ,"record_name" varchar
  ,"mail_activity_type_id" int4
  ,"email_from" varchar
  ,"author_id" int4
  ,"no_auto_thread" bool
  ,"message_id" varchar
  ,"reply_to" varchar
  ,"mail_server_id" int4
  ,"composition_mode" varchar
  ,"use_active_domain" bool
  ,"active_domain" text
  ,"is_log" bool
  ,"subject" varchar
  ,"notify" bool
  ,"auto_delete" bool
  ,"auto_delete_message" bool
  ,"template_id" int4
  ,"message_type" varchar NOT NULL
  ,"subtype_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"moderation_status" varchar
  ,"moderator_id" int4
  ,"layout" varchar
  ,"add_sign" bool
  ,"email_cc" varchar
  ,"email_to" varchar
  ,"mail_tracking_needs_action" bool
  ,"mass_mailing_campaign_id" int4
  ,"mass_mailing_id" int4
  ,"mass_mailing_name" varchar
  ,"website_published" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message');



drop foreign table if exists odoo12_mail_compose_message_ir_attachments_rel;


create FOREIGN TABLE odoo12_mail_compose_message_ir_attachments_rel("wizard_id" int4 NOT NULL
  ,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_ir_attachments_rel');



drop foreign table if exists odoo12_mail_compose_message_mail_mass_mailing_list_rel;


create FOREIGN TABLE odoo12_mail_compose_message_mail_mass_mailing_list_rel("mail_compose_message_id" int4 NOT NULL
  ,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_mail_mass_mailing_list_rel');



drop foreign table if exists odoo12_mail_compose_message_res_partner_rel;


create FOREIGN TABLE odoo12_mail_compose_message_res_partner_rel("wizard_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_res_partner_rel');



drop foreign table if exists odoo12_mail_followers;


create FOREIGN TABLE odoo12_mail_followers("id" int4 NOT NULL
  ,"res_model" varchar NOT NULL
  ,"res_id" int4
  ,"partner_id" int4
  ,"channel_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_followers');



drop foreign table if exists odoo12_mail_followers_mail_message_subtype_rel;


create FOREIGN TABLE odoo12_mail_followers_mail_message_subtype_rel("mail_followers_id" int4 NOT NULL
  ,"mail_message_subtype_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_followers_mail_message_subtype_rel');



drop foreign table if exists odoo12_mail_list_wizard_partner;


create FOREIGN TABLE odoo12_mail_list_wizard_partner("partner_mail_list_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_list_wizard_partner');



drop foreign table if exists odoo12_mail_mail;


create FOREIGN TABLE odoo12_mail_mail("id" int4 NOT NULL
  ,"mail_message_id" int4 NOT NULL
  ,"body_html" text
  ,"references" text
  ,"headers" text
  ,"notification" bool
  ,"email_to" text
  ,"email_cc" varchar
  ,"state" varchar
  ,"auto_delete" bool
  ,"failure_reason" text
  ,"scheduled_date" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"fetchmail_server_id" int4
  ,"mailing_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail');



drop foreign table if exists odoo12_mail_mail_res_partner_rel;


create FOREIGN TABLE odoo12_mail_mail_res_partner_rel("mail_mail_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail_res_partner_rel');



drop foreign table if exists odoo12_mail_mail_statistics;


create FOREIGN TABLE odoo12_mail_mail_statistics("id" int4 NOT NULL
  ,"mail_mail_id" int4
  ,"mail_mail_id_int" int4
  ,"message_id" varchar
  ,"model" varchar
  ,"res_id" int4
  ,"mass_mailing_id" int4
  ,"mass_mailing_campaign_id" int4
  ,"ignored" timestamp
  ,"scheduled" timestamp
  ,"sent" timestamp
  ,"exception" timestamp
  ,"opened" timestamp
  ,"replied" timestamp
  ,"bounced" timestamp
  ,"clicked" timestamp
  ,"state" varchar
  ,"state_update" timestamp
  ,"email" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail_statistics');



drop foreign table if exists odoo12_mail_mass_mailing;


create FOREIGN TABLE odoo12_mail_mass_mailing("id" int4 NOT NULL
  ,"active" bool
  ,"email_from" varchar NOT NULL
  ,"sent_date" timestamp
  ,"schedule_date" timestamp
  ,"body_html" text
  ,"keep_archives" bool
  ,"mass_mailing_campaign_id" int4
  ,"campaign_id" int4
  ,"source_id" int4 NOT NULL
  ,"medium_id" int4
  ,"state" varchar NOT NULL
  ,"color" int4
  ,"user_id" int4
  ,"reply_to_mode" varchar NOT NULL
  ,"reply_to" varchar
  ,"mailing_model_id" int4
  ,"mailing_domain" varchar
  ,"mail_server_id" int4
  ,"contact_ab_pc" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing');



drop foreign table if exists odoo12_mail_mass_mailing_campaign;


create FOREIGN TABLE odoo12_mail_mass_mailing_campaign("id" int4 NOT NULL
  ,"stage_id" int4 NOT NULL
  ,"user_id" int4 NOT NULL
  ,"campaign_id" int4 NOT NULL
  ,"source_id" int4
  ,"medium_id" int4
  ,"unique_ab_testing" bool
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_campaign');



drop foreign table if exists odoo12_mail_mass_mailing_contact;


create FOREIGN TABLE odoo12_mail_mass_mailing_contact("id" int4 NOT NULL
  ,"name" varchar
  ,"company_name" varchar
  ,"title_id" int4
  ,"email" varchar NOT NULL
  ,"is_email_valid" bool
  ,"message_bounce" int4
  ,"country_id" int4
  ,"message_main_attachment_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact');



drop foreign table if exists odoo12_mail_mass_mailing_contact_list_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_contact_list_rel("id" int4 NOT NULL
  ,"contact_id" int4 NOT NULL
  ,"list_id" int4 NOT NULL
  ,"opt_out" bool
  ,"unsubscription_date" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_list_rel');



drop foreign table if exists odoo12_mail_mass_mailing_contact_res_partner_category_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_contact_res_partner_category_rel("mail_mass_mailing_contact_id" int4 NOT NULL
  ,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_res_partner_category_rel');



drop foreign table if exists odoo12_mail_mass_mailing_list;


create FOREIGN TABLE odoo12_mail_mass_mailing_list("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"is_public" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"partner_mandatory" bool
  ,"partner_category" int4
  ,"dynamic" bool
  ,"sync_method" varchar NOT NULL
  ,"sync_domain" varchar NOT NULL
  ,"is_synced" bool
  ,"popup_content" text
  ,"popup_redirect_url" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list');



drop foreign table if exists odoo12_mail_mass_mailing_list_mass_mailing_list_merge_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_list_mass_mailing_list_merge_rel("mass_mailing_list_merge_id" int4 NOT NULL
  ,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_mass_mailing_list_merge_rel');



drop foreign table if exists odoo12_mail_mass_mailing_list_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_list_rel("mail_mass_mailing_id" int4 NOT NULL
  ,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_rel');



drop foreign table if exists odoo12_mail_mass_mailing_list_survey_mail_compose_message_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_list_survey_mail_compose_message_rel("survey_mail_compose_message_id" int4 NOT NULL
  ,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_survey_mail_compose_message_rel');



drop foreign table if exists odoo12_mail_mass_mailing_load_filter;


create FOREIGN TABLE odoo12_mail_mass_mailing_load_filter("id" int4 NOT NULL
  ,"filter_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_load_filter');



drop foreign table if exists odoo12_mail_mass_mailing_stage;


create FOREIGN TABLE odoo12_mail_mass_mailing_stage("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_stage');



drop foreign table if exists odoo12_mail_mass_mailing_tag;


create FOREIGN TABLE odoo12_mail_mass_mailing_tag("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag');



drop foreign table if exists odoo12_mail_mass_mailing_tag_rel;


create FOREIGN TABLE odoo12_mail_mass_mailing_tag_rel("tag_id" int4 NOT NULL
  ,"campaign_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag_rel');



drop foreign table if exists odoo12_mail_mass_mailing_test;


create FOREIGN TABLE odoo12_mail_mass_mailing_test("id" int4 NOT NULL
  ,"email_to" varchar NOT NULL
  ,"mass_mailing_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_test');



drop foreign table if exists odoo12_mail_message;


create FOREIGN TABLE odoo12_mail_message("id" int4 NOT NULL
  ,"subject" varchar
  ,"date" timestamp
  ,"body" text
  ,"parent_id" int4
  ,"model" varchar
  ,"res_id" int4
  ,"record_name" varchar
  ,"message_type" varchar NOT NULL
  ,"subtype_id" int4
  ,"mail_activity_type_id" int4
  ,"email_from" varchar
  ,"author_id" int4
  ,"no_auto_thread" bool
  ,"message_id" varchar
  ,"reply_to" varchar
  ,"mail_server_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"add_sign" bool
  ,"moderation_status" varchar
  ,"moderator_id" int4
  ,"layout" varchar
  ,"email_cc" varchar
  ,"email_to" varchar
  ,"mail_tracking_needs_action" bool
  ,"website_published" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message');



drop foreign table if exists odoo12_mail_message_mail_channel_rel;


create FOREIGN TABLE odoo12_mail_message_mail_channel_rel("mail_message_id" int4 NOT NULL
  ,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_mail_channel_rel');



drop foreign table if exists odoo12_mail_message_res_partner_needaction_rel;


create FOREIGN TABLE odoo12_mail_message_res_partner_needaction_rel("id" int4 NOT NULL
  ,"mail_message_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL
  ,"is_read" bool
  ,"is_email" bool
  ,"email_status" varchar
  ,"mail_id" int4
  ,"failure_type" varchar
  ,"failure_reason" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel');



drop foreign table if exists odoo12_mail_message_res_partner_needaction_rel_mail_resend_message_rel;


create FOREIGN TABLE odoo12_mail_message_res_partner_needaction_rel_mail_resend_message_rel("mail_resend_message_id" int4 NOT NULL
  ,"mail_message_res_partner_needaction_rel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel_mail_resend_message_rel');



drop foreign table if exists odoo12_mail_message_res_partner_rel;


create FOREIGN TABLE odoo12_mail_message_res_partner_rel("mail_message_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_rel');



drop foreign table if exists odoo12_mail_message_res_partner_starred_rel;


create FOREIGN TABLE odoo12_mail_message_res_partner_starred_rel("mail_message_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_starred_rel');



drop foreign table if exists odoo12_mail_message_subtype;


create FOREIGN TABLE odoo12_mail_message_subtype("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" text
  ,"internal" bool
  ,"parent_id" int4
  ,"relation_field" varchar
  ,"res_model" varchar
  ,"default" bool
  ,"sequence" int4
  ,"hidden" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_subtype');



drop foreign table if exists odoo12_mail_moderation;
create FOREIGN TABLE odoo12_mail_moderation("id" int4 NOT NULL
  ,"email" varchar NOT NULL
  ,"status" varchar NOT NULL
  ,"channel_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_moderation');



drop foreign table if exists odoo12_mail_resend_cancel;


create FOREIGN TABLE odoo12_mail_resend_cancel("id" int4 NOT NULL
  ,"model" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_cancel');



drop foreign table if exists odoo12_mail_resend_message;


create FOREIGN TABLE odoo12_mail_resend_message("id" int4 NOT NULL
  ,"mail_message_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_message');



drop foreign table if exists odoo12_mail_resend_partner;


create FOREIGN TABLE odoo12_mail_resend_partner("id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"resend" bool
  ,"resend_wizard_id" int4
  ,"message" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_partner');



drop foreign table if exists odoo12_mail_shortcode;


create FOREIGN TABLE odoo12_mail_shortcode("id" int4 NOT NULL
  ,"source" varchar NOT NULL
  ,"unicode_source" varchar
  ,"substitution" text NOT NULL
  ,"description" varchar
  ,"shortcode_type" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_shortcode');



drop foreign table if exists odoo12_mail_statistics_report;


create FOREIGN TABLE odoo12_mail_statistics_report("id" int4
  ,"scheduled_date" timestamp
  ,"name" varchar
  ,"campaign" varchar
  ,"bounced" int8
  ,"sent" int8
  ,"delivered" int8
  ,"opened" int8
  ,"replied" int8
  ,"clicked" int8
  ,"state" varchar
  ,"email_from" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_statistics_report');



drop foreign table if exists odoo12_mail_template;


create FOREIGN TABLE odoo12_mail_template("id" int4 NOT NULL
  ,"name" varchar
  ,"model_id" int4
  ,"model" varchar
  ,"lang" varchar
  ,"user_signature" bool
  ,"subject" varchar
  ,"email_from" varchar
  ,"use_default_to" bool
  ,"email_to" varchar
  ,"partner_to" varchar
  ,"email_cc" varchar
  ,"reply_to" varchar
  ,"mail_server_id" int4
  ,"body_html" text
  ,"report_name" varchar
  ,"report_template" int4
  ,"ref_ir_act_window" int4
  ,"auto_delete" bool
  ,"model_object_field" int4
  ,"sub_object" int4
  ,"sub_model_object_field" int4
  ,"null_value" varchar
  ,"copyvalue" varchar
  ,"scheduled_date" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"force_email_send" bool
  ,"easy_my_coop" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_template');



drop foreign table if exists odoo12_mail_test;


create FOREIGN TABLE odoo12_mail_test("id" int4 NOT NULL
  ,"name" varchar
  ,"description" text
  ,"alias_id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_test');



drop foreign table if exists odoo12_mail_test_simple;


create FOREIGN TABLE odoo12_mail_test_simple("id" int4 NOT NULL
  ,"name" varchar
  ,"email_from" varchar
  ,"description" text
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_test_simple');



drop foreign table if exists odoo12_mail_tracking_email;


create FOREIGN TABLE odoo12_mail_tracking_email("id" int4 NOT NULL
  ,"name" varchar
  ,"display_name" varchar
  ,"timestamp" numeric
  ,"time" timestamp
  ,"date" date
  ,"mail_message_id" int4
  ,"mail_id" int4
  ,"partner_id" int4
  ,"recipient" varchar
  ,"recipient_address" varchar
  ,"sender" varchar
  ,"state" varchar
  ,"error_smtp_server" varchar
  ,"error_type" varchar
  ,"error_description" varchar
  ,"bounce_type" varchar
  ,"bounce_description" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"token" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_tracking_email');



drop foreign table if exists odoo12_mail_tracking_event;


create FOREIGN TABLE odoo12_mail_tracking_event("id" int4 NOT NULL
  ,"recipient" varchar
  ,"recipient_address" varchar
  ,"timestamp" numeric
  ,"time" timestamp
  ,"date" date
  ,"tracking_email_id" int4 NOT NULL
  ,"event_type" varchar
  ,"smtp_server" varchar
  ,"url" varchar
  ,"ip" varchar
  ,"user_agent" varchar
  ,"mobile" bool
  ,"os_family" varchar
  ,"ua_family" varchar
  ,"ua_type" varchar
  ,"user_country_id" int4
  ,"error_type" varchar
  ,"error_description" varchar
  ,"error_details" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"mailgun_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_tracking_event');



drop foreign table if exists odoo12_mail_tracking_value;


create FOREIGN TABLE odoo12_mail_tracking_value("id" int4 NOT NULL
  ,"field" varchar NOT NULL
  ,"field_desc" varchar NOT NULL
  ,"field_type" varchar
  ,"old_value_integer" int4
  ,"old_value_float" float8
  ,"old_value_monetary" float8
  ,"old_value_char" varchar
  ,"old_value_text" text
  ,"old_value_datetime" timestamp
  ,"new_value_integer" int4
  ,"new_value_float" float8
  ,"new_value_monetary" float8
  ,"new_value_char" varchar
  ,"new_value_text" text
  ,"new_value_datetime" timestamp
  ,"mail_message_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"track_sequence" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_tracking_value');



drop foreign table if exists odoo12_mail_wizard_invite;


create FOREIGN TABLE odoo12_mail_wizard_invite("id" int4 NOT NULL
  ,"res_model" varchar NOT NULL
  ,"res_id" int4
  ,"message" text
  ,"send_mail" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_wizard_invite');



drop foreign table if exists odoo12_mail_wizard_invite_res_partner_rel;


create FOREIGN TABLE odoo12_mail_wizard_invite_res_partner_rel("mail_wizard_invite_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_wizard_invite_res_partner_rel');



drop foreign table if exists odoo12_mass_editing_wizard;


create FOREIGN TABLE odoo12_mass_editing_wizard("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_editing_wizard');



drop foreign table if exists odoo12_mass_field_rel;


create FOREIGN TABLE odoo12_mass_field_rel("mass_id" int4 NOT NULL
  ,"field_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_field_rel');



drop foreign table if exists odoo12_mass_group_rel;


create FOREIGN TABLE odoo12_mass_group_rel("mass_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_group_rel');



drop foreign table if exists odoo12_mass_mailing_ir_attachments_rel;


create FOREIGN TABLE odoo12_mass_mailing_ir_attachments_rel("mass_mailing_id" int4 NOT NULL
  ,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_ir_attachments_rel');



drop foreign table if exists odoo12_mass_mailing_list_merge;


create FOREIGN TABLE odoo12_mass_mailing_list_merge("id" int4 NOT NULL
  ,"dest_list_id" int4
  ,"merge_options" varchar NOT NULL
  ,"new_list_name" varchar
  ,"archive_src_lists" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_list_merge');



drop foreign table if exists odoo12_mass_mailing_schedule_date;


create FOREIGN TABLE odoo12_mass_mailing_schedule_date("id" int4 NOT NULL
  ,"schedule_date" timestamp
  ,"mass_mailing_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_schedule_date');



drop foreign table if exists odoo12_mass_object;


create FOREIGN TABLE odoo12_mass_object("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"model_id" int4 NOT NULL
  ,"ref_ir_act_window_id" int4
  ,"model_list" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_object');



drop foreign table if exists odoo12_meeting_category_rel;


create FOREIGN TABLE odoo12_meeting_category_rel("event_id" int4 NOT NULL
  ,"type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'meeting_category_rel');



drop foreign table if exists odoo12_merge_opportunity_rel;


create FOREIGN TABLE odoo12_merge_opportunity_rel("merge_id" int4 NOT NULL
  ,"opportunity_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'merge_opportunity_rel');



drop foreign table if exists odoo12_message_attachment_rel;


create FOREIGN TABLE odoo12_message_attachment_rel("message_id" int4 NOT NULL
  ,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'message_attachment_rel');



drop foreign table if exists odoo12_mis_budget;


create FOREIGN TABLE odoo12_mis_budget("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" varchar
  ,"report_id" int4 NOT NULL
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"state" varchar NOT NULL
  ,"company_id" int4
  ,"message_last_post" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget');



drop foreign table if exists odoo12_mis_budget_by_account;


create FOREIGN TABLE odoo12_mis_budget_by_account("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"name" varchar NOT NULL
  ,"description" varchar
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"state" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_by_account');



drop foreign table if exists odoo12_mis_budget_by_account_item;


create FOREIGN TABLE odoo12_mis_budget_by_account_item("id" int4 NOT NULL
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"analytic_account_id" int4
  ,"budget_id" int4 NOT NULL
  ,"debit" numeric
  ,"credit" numeric
  ,"balance" numeric
  ,"company_id" int4
  ,"company_currency_id" int4
  ,"account_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_by_account_item');



drop foreign table if exists odoo12_mis_budget_item;
create FOREIGN TABLE odoo12_mis_budget_item("id" int4 NOT NULL
  ,"budget_id" int4 NOT NULL
  ,"kpi_expression_id" int4 NOT NULL
  ,"date_range_id" int4
  ,"analytic_account_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"amount" float8
  ,"seq1" int4
  ,"seq2" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_item');



drop foreign table if exists odoo12_mis_cash_flow;


create FOREIGN TABLE odoo12_mis_cash_flow("id" int4
  ,"line_type" varchar
  ,"move_line_id" int4
  ,"account_id" int4
  ,"debit" float8
  ,"credit" float8
  ,"reconciled" bool
  ,"full_reconcile_id" int4
  ,"partner_id" int4
  ,"company_id" int4
  ,"user_type_id" int4
  ,"name" varchar
  ,"date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_cash_flow');



drop foreign table if exists odoo12_mis_cash_flow_forecast_line;


create FOREIGN TABLE odoo12_mis_cash_flow_forecast_line("id" int4 NOT NULL
  ,"date" date NOT NULL
  ,"account_id" int4 NOT NULL
  ,"partner_id" int4
  ,"name" varchar NOT NULL
  ,"balance" float8 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_cash_flow_forecast_line');



drop foreign table if exists odoo12_mis_report;


create FOREIGN TABLE odoo12_mis_report("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" varchar
  ,"style_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"move_lines_source" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report');



drop foreign table if exists odoo12_mis_report_instance;


create FOREIGN TABLE odoo12_mis_report_instance("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date" date
  ,"report_id" int4 NOT NULL
  ,"target_move" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"multi_company" bool
  ,"currency_id" int4
  ,"landscape_pdf" bool
  ,"no_auto_expand_accounts" bool
  ,"date_range_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"temporary" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"display_columns_description" bool
  ,"analytic_account_id" int4
  ,"hide_analytic_filters" bool
  ,"analytic_group_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance');



drop foreign table if exists odoo12_mis_report_instance_period;


create FOREIGN TABLE odoo12_mis_report_instance_period("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"mode" varchar NOT NULL
  ,"type" varchar
  ,"date_range_type_id" int4
  ,"offset" int4
  ,"duration" int4
  ,"manual_date_from" date
  ,"manual_date_to" date
  ,"date_range_id" int4
  ,"sequence" int4
  ,"report_instance_id" int4 NOT NULL
  ,"normalize_factor" int4
  ,"source" varchar NOT NULL
  ,"source_aml_model_id" int4
  ,"source_sumcol_accdet" bool
  ,"source_cmpcol_from_id" int4
  ,"source_cmpcol_to_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"source_mis_budget_id" int4
  ,"is_ytd" bool
  ,"analytic_account_id" int4
  ,"source_mis_budget_by_account_id" int4
  ,"analytic_group_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period');



drop foreign table if exists odoo12_mis_report_instance_period_mis_report_subkpi_rel;


create FOREIGN TABLE odoo12_mis_report_instance_period_mis_report_subkpi_rel("mis_report_instance_period_id" int4 NOT NULL
  ,"mis_report_subkpi_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_mis_report_subkpi_rel');



drop foreign table if exists odoo12_mis_report_instance_period_sum;


create FOREIGN TABLE odoo12_mis_report_instance_period_sum("id" int4 NOT NULL
  ,"period_id" int4 NOT NULL
  ,"period_to_sum_id" int4 NOT NULL
  ,"sign" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_sum');



drop foreign table if exists odoo12_mis_report_instance_res_company_rel;


create FOREIGN TABLE odoo12_mis_report_instance_res_company_rel("mis_report_instance_id" int4 NOT NULL
  ,"res_company_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_res_company_rel');



drop foreign table if exists odoo12_mis_report_kpi;


create FOREIGN TABLE odoo12_mis_report_kpi("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"description" varchar NOT NULL
  ,"multi" bool
  ,"auto_expand_accounts" bool
  ,"auto_expand_accounts_style_id" int4
  ,"style_id" int4
  ,"style_expression" varchar
  ,"type" varchar NOT NULL
  ,"compare_method" varchar NOT NULL
  ,"accumulation_method" varchar NOT NULL
  ,"sequence" int4
  ,"report_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"budgetable" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_kpi');



drop foreign table if exists odoo12_mis_report_kpi_expression;


create FOREIGN TABLE odoo12_mis_report_kpi_expression("id" int4 NOT NULL
  ,"sequence" int4
  ,"name" varchar
  ,"kpi_id" int4 NOT NULL
  ,"subkpi_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_kpi_expression');



drop foreign table if exists odoo12_mis_report_query;


create FOREIGN TABLE odoo12_mis_report_query("id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"model_id" int4 NOT NULL
  ,"aggregate" varchar
  ,"date_field" int4 NOT NULL
  ,"domain" varchar
  ,"report_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_query');



drop foreign table if exists odoo12_mis_report_style;


create FOREIGN TABLE odoo12_mis_report_style("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color_inherit" bool
  ,"color" varchar
  ,"background_color_inherit" bool
  ,"background_color" varchar
  ,"font_style_inherit" bool
  ,"font_style" varchar
  ,"font_weight_inherit" bool
  ,"font_weight" varchar
  ,"font_size_inherit" bool
  ,"font_size" varchar
  ,"indent_level_inherit" bool
  ,"indent_level" int4
  ,"prefix_inherit" bool
  ,"prefix" varchar(16)
  ,"suffix_inherit" bool
  ,"suffix" varchar(16)
  ,"dp_inherit" bool
  ,"dp" int4
  ,"divider_inherit" bool
  ,"divider" varchar
  ,"hide_empty_inherit" bool
  ,"hide_empty" bool
  ,"hide_always_inherit" bool
  ,"hide_always" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_style');



drop foreign table if exists odoo12_mis_report_subkpi;


create FOREIGN TABLE odoo12_mis_report_subkpi("id" int4 NOT NULL
  ,"sequence" int4
  ,"report_id" int4 NOT NULL
  ,"name" varchar(32) NOT NULL
  ,"description" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_subkpi');



drop foreign table if exists odoo12_mis_report_subreport;


create FOREIGN TABLE odoo12_mis_report_subreport("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"report_id" int4 NOT NULL
  ,"subreport_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_subreport');



drop foreign table if exists odoo12_open_items_report_wizard;


create FOREIGN TABLE odoo12_open_items_report_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_at" date NOT NULL
  ,"target_move" varchar NOT NULL
  ,"hide_account_balance_at_0" bool
  ,"receivable_accounts_only" bool
  ,"payable_accounts_only" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hide_account_at_0" bool
  ,"foreign_currency" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'open_items_report_wizard');



drop foreign table if exists odoo12_open_items_report_wizard_res_partner_rel;


create FOREIGN TABLE odoo12_open_items_report_wizard_res_partner_rel("open_items_report_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'open_items_report_wizard_res_partner_rel');



drop foreign table if exists odoo12_operation_request;


create FOREIGN TABLE odoo12_operation_request("id" int4 NOT NULL
  ,"request_date" date
  ,"effective_date" date
  ,"partner_id" int4 NOT NULL
  ,"partner_id_to" int4
  ,"operation_type" varchar NOT NULL
  ,"share_product_id" int4 NOT NULL
  ,"share_to_product_id" int4
  ,"quantity" int4 NOT NULL
  ,"state" varchar NOT NULL
  ,"user_id" int4
  ,"receiver_not_member" bool
  ,"company_id" int4 NOT NULL
  ,"invoice" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'operation_request');



drop foreign table if exists odoo12_partner_create_subscription;


create FOREIGN TABLE odoo12_partner_create_subscription("id" int4 NOT NULL
  ,"is_company" bool
  ,"cooperator" int4
  ,"register_number" varchar
  ,"email" varchar NOT NULL
  ,"bank_account" varchar NOT NULL
  ,"share_product" int4 NOT NULL
  ,"share_qty" int4 NOT NULL
  ,"representative_name" varchar
  ,"representative_email" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_create_subscription');



drop foreign table if exists odoo12_partner_mail_list_wizard;


create FOREIGN TABLE odoo12_partner_mail_list_wizard("id" int4 NOT NULL
  ,"mail_list_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_mail_list_wizard');



drop foreign table if exists odoo12_partner_update_info;


create FOREIGN TABLE odoo12_partner_update_info("id" int4 NOT NULL
  ,"is_company" bool
  ,"register_number" varchar
  ,"cooperator" int4
  ,"from_sub_req" bool
  ,"all" bool
  ,"birthdate" bool
  ,"legal_form" bool
  ,"representative_function" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_update_info');



drop foreign table if exists odoo12_payment_acquirer;


create FOREIGN TABLE odoo12_payment_acquirer("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" text
  ,"sequence" int4
  ,"provider" varchar NOT NULL
  ,"company_id" int4 NOT NULL
  ,"view_template_id" int4 NOT NULL
  ,"registration_view_template_id" int4
  ,"environment" varchar NOT NULL
  ,"website_published" bool
  ,"capture_manually" bool
  ,"journal_id" int4
  ,"specific_countries" bool
  ,"pre_msg" text
  ,"post_msg" text
  ,"pending_msg" text
  ,"done_msg" text
  ,"cancel_msg" text
  ,"error_msg" text
  ,"save_token" varchar
  ,"fees_active" bool
  ,"fees_dom_fixed" float8
  ,"fees_dom_var" float8
  ,"fees_int_fixed" float8
  ,"fees_int_var" float8
  ,"module_id" int4
  ,"payment_flow" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"qr_code" bool
  ,"so_reference_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer');



drop foreign table if exists odoo12_payment_acquirer_onboarding_wizard;


create FOREIGN TABLE odoo12_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
  ,"payment_method" varchar
  ,"paypal_email_account" varchar
  ,"paypal_seller_account" varchar
  ,"paypal_pdt_token" varchar
  ,"stripe_secret_key" varchar
  ,"stripe_publishable_key" varchar
  ,"manual_name" varchar
  ,"journal_name" varchar
  ,"acc_number" varchar
  ,"manual_post_msg" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer_onboarding_wizard');



drop foreign table if exists odoo12_payment_acquirer_payment_icon_rel;


create FOREIGN TABLE odoo12_payment_acquirer_payment_icon_rel("payment_acquirer_id" int4 NOT NULL
  ,"payment_icon_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer_payment_icon_rel');



drop foreign table if exists odoo12_payment_country_rel;


create FOREIGN TABLE odoo12_payment_country_rel("payment_id" int4 NOT NULL
  ,"country_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_country_rel');



drop foreign table if exists odoo12_payment_icon;


create FOREIGN TABLE odoo12_payment_icon("id" int4 NOT NULL
  ,"name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_icon');



drop foreign table if exists odoo12_payment_return;


create FOREIGN TABLE odoo12_payment_return("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"company_id" int4 NOT NULL
  ,"date" date
  ,"name" varchar NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"move_id" int4
  ,"state" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"imported_bank_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return');



drop foreign table if exists odoo12_payment_return_import;


create FOREIGN TABLE odoo12_payment_return_import("id" int4 NOT NULL
  ,"journal_id" int4
  ,"data_file" bytea NOT NULL
  ,"match_after_import" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_import');



drop foreign table if exists odoo12_payment_return_line;


create FOREIGN TABLE odoo12_payment_return_line("id" int4 NOT NULL
  ,"return_id" int4 NOT NULL
  ,"concept" varchar
  ,"reason_id" int4
  ,"reason_additional_information" varchar
  ,"reference" varchar
  ,"date" date
  ,"partner_name" varchar
  ,"partner_id" int4
  ,"amount" numeric
  ,"expense_account" int4
  ,"expense_amount" float8
  ,"expense_partner_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"unique_import_id" varchar
  ,"raw_import_data" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_line');



drop foreign table if exists odoo12_payment_return_reason;


create FOREIGN TABLE odoo12_payment_return_reason("id" int4 NOT NULL
  ,"code" varchar
  ,"name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_reason');



drop foreign table if exists odoo12_payment_token;


create FOREIGN TABLE odoo12_payment_token("id" int4 NOT NULL
  ,"name" varchar
  ,"partner_id" int4 NOT NULL
  ,"acquirer_id" int4 NOT NULL
  ,"acquirer_ref" varchar NOT NULL
  ,"active" bool
  ,"verified" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_token');



drop foreign table if exists odoo12_payment_transaction;


create FOREIGN TABLE odoo12_payment_transaction("id" int4 NOT NULL
  ,"create_date" timestamp
  ,"date" timestamp
  ,"acquirer_id" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"state" varchar NOT NULL
  ,"state_message" text
  ,"amount" numeric NOT NULL
  ,"fees" numeric
  ,"currency_id" int4 NOT NULL
  ,"reference" varchar NOT NULL
  ,"acquirer_reference" varchar
  ,"partner_id" int4
  ,"partner_name" varchar
  ,"partner_lang" varchar
  ,"partner_email" varchar
  ,"partner_zip" varchar
  ,"partner_address" varchar
  ,"partner_city" varchar
  ,"partner_country_id" int4 NOT NULL
  ,"partner_phone" varchar
  ,"html_3ds" varchar
  ,"callback_model_id" int4
  ,"callback_res_id" int4
  ,"callback_method" varchar
  ,"callback_hash" varchar
  ,"payment_token_id" int4
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"openupgrade_legacy_12_0_state" varchar
  ,"return_url" varchar
  ,"is_processed" bool
  ,"payment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_transaction');



drop foreign table if exists odoo12_payslip_lines_contribution_register;


create FOREIGN TABLE odoo12_payslip_lines_contribution_register("id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payslip_lines_contribution_register');



drop foreign table if exists odoo12_portal_share;


create FOREIGN TABLE odoo12_portal_share("id" int4 NOT NULL
  ,"res_model" varchar NOT NULL
  ,"res_id" int4 NOT NULL
  ,"note" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_share');



drop foreign table if exists odoo12_portal_share_res_partner_rel;


create FOREIGN TABLE odoo12_portal_share_res_partner_rel("portal_share_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_share_res_partner_rel');



drop foreign table if exists odoo12_portal_wizard;


create FOREIGN TABLE odoo12_portal_wizard("id" int4 NOT NULL
  ,"portal_id" int4
  ,"welcome_message" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_wizard');



drop foreign table if exists odoo12_portal_wizard_user;


create FOREIGN TABLE odoo12_portal_wizard_user("id" int4 NOT NULL
  ,"wizard_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"email" varchar
  ,"in_portal" bool
  ,"user_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_wizard_user');



drop foreign table if exists odoo12_procurement_group;


create FOREIGN TABLE odoo12_procurement_group("id" int4 NOT NULL
  ,"partner_id" int4
  ,"name" varchar NOT NULL
  ,"move_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"sale_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'procurement_group');



drop foreign table if exists odoo12_product_attr_exclusion_value_ids_rel;


create FOREIGN TABLE odoo12_product_attr_exclusion_value_ids_rel("product_template_attribute_exclusion_id" int4 NOT NULL
  ,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attr_exclusion_value_ids_rel');



drop foreign table if exists odoo12_product_attribute;


create FOREIGN TABLE odoo12_product_attribute("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"openupgrade_legacy_12_0_create_variant" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"create_variant" varchar NOT NULL
  ,"type" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute');



drop foreign table if exists odoo12_product_attribute_custom_value;


create FOREIGN TABLE odoo12_product_attribute_custom_value("id" int4 NOT NULL
  ,"attribute_value_id" int4
  ,"sale_order_line_id" int4
  ,"custom_value" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_custom_value');



drop foreign table if exists odoo12_product_attribute_value;


create FOREIGN TABLE odoo12_product_attribute_value("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"attribute_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"is_custom" bool
  ,"html_color" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value');



drop foreign table if exists odoo12_product_attribute_value_product_product_rel;


create FOREIGN TABLE odoo12_product_attribute_value_product_product_rel("product_product_id" int4 NOT NULL
  ,"product_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_product_rel');



drop foreign table if exists odoo12_product_attribute_value_product_template_attribute_line_rel;


create FOREIGN TABLE odoo12_product_attribute_value_product_template_attribute_line_rel("product_template_attribute_line_id" int4 NOT NULL
  ,"product_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_template_attribute_line_rel');



drop foreign table if exists odoo12_product_category;


create FOREIGN TABLE odoo12_product_category("id" int4 NOT NULL
  ,"parent_left" int4
  ,"parent_right" int4
  ,"name" varchar NOT NULL
  ,"complete_name" varchar
  ,"parent_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"removal_strategy_id" int4
  ,"parent_path" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_category');



drop foreign table if exists odoo12_product_margin;


create FOREIGN TABLE odoo12_product_margin("id" int4 NOT NULL
  ,"from_date" date
  ,"to_date" date
  ,"invoice_state" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_margin');



drop foreign table if exists odoo12_product_optional_rel;


create FOREIGN TABLE odoo12_product_optional_rel("src_id" int4 NOT NULL
  ,"dest_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_optional_rel');



drop foreign table if exists odoo12_product_packaging;


create FOREIGN TABLE odoo12_product_packaging("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"product_id" int4
  ,"qty" float8
  ,"barcode" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_packaging');



drop foreign table if exists odoo12_product_price_history;


create FOREIGN TABLE odoo12_product_price_history("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"datetime" timestamp
  ,"cost" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_price_history');



drop foreign table if exists odoo12_product_price_list;


create FOREIGN TABLE odoo12_product_price_list("id" int4 NOT NULL
  ,"price_list" int4 NOT NULL
  ,"qty1" int4
  ,"qty2" int4
  ,"qty3" int4
  ,"qty4" int4
  ,"qty5" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_price_list');



drop foreign table if exists odoo12_product_pricelist;


create FOREIGN TABLE odoo12_product_pricelist("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"currency_id" int4 NOT NULL
  ,"company_id" int4
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"discount_policy" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_pricelist');



drop foreign table if exists odoo12_product_pricelist_item;


create FOREIGN TABLE odoo12_product_pricelist_item("id" int4 NOT NULL
  ,"product_tmpl_id" int4
  ,"product_id" int4
  ,"categ_id" int4
  ,"min_quantity" int4
  ,"applied_on" varchar NOT NULL
  ,"base" varchar NOT NULL
  ,"base_pricelist_id" int4
  ,"pricelist_id" int4
  ,"price_surcharge" numeric
  ,"price_discount" numeric
  ,"price_round" numeric
  ,"price_min_margin" numeric
  ,"price_max_margin" numeric
  ,"company_id" int4
  ,"currency_id" int4
  ,"date_start" date
  ,"date_end" date
  ,"compute_price" varchar
  ,"fixed_price" numeric
  ,"percent_price" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_pricelist_item');



drop foreign table if exists odoo12_product_product;
create FOREIGN TABLE odoo12_product_product("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"default_code" varchar
  ,"active" bool
  ,"product_tmpl_id" int4 NOT NULL
  ,"barcode" varchar
  ,"volume" float8
  ,"weight" numeric
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_product');



drop foreign table if exists odoo12_product_putaway;
create FOREIGN TABLE odoo12_product_putaway("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_putaway');



drop foreign table if exists odoo12_product_removal;
create FOREIGN TABLE odoo12_product_removal("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"method" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_removal');



drop foreign table if exists odoo12_product_replenish;


create FOREIGN TABLE odoo12_product_replenish("id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"product_tmpl_id" int4 NOT NULL
  ,"product_has_variants" bool NOT NULL
  ,"product_uom_id" int4 NOT NULL
  ,"quantity" float8 NOT NULL
  ,"date_planned" timestamp
  ,"warehouse_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_replenish');



drop foreign table if exists odoo12_product_replenish_stock_location_route_rel;


create FOREIGN TABLE odoo12_product_replenish_stock_location_route_rel("product_replenish_id" int4 NOT NULL
  ,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_replenish_stock_location_route_rel');



drop foreign table if exists odoo12_product_supplier_taxes_rel;


create FOREIGN TABLE odoo12_product_supplier_taxes_rel("prod_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_supplier_taxes_rel');



drop foreign table if exists odoo12_product_supplierinfo;


create FOREIGN TABLE odoo12_product_supplierinfo("id" int4 NOT NULL
  ,"name" int4 NOT NULL
  ,"product_name" varchar
  ,"product_code" varchar
  ,"sequence" int4
  ,"min_qty" float8 NOT NULL
  ,"price" numeric NOT NULL
  ,"company_id" int4
  ,"currency_id" int4 NOT NULL
  ,"date_start" date
  ,"date_end" date
  ,"product_id" int4
  ,"product_tmpl_id" int4
  ,"delay" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_supplierinfo');



drop foreign table if exists odoo12_product_taxes_rel;


create FOREIGN TABLE odoo12_product_taxes_rel("prod_id" int4 NOT NULL
  ,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_taxes_rel');



drop foreign table if exists odoo12_product_template;


create FOREIGN TABLE odoo12_product_template("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"description" text
  ,"description_purchase" text
  ,"description_sale" text
  ,"type" varchar NOT NULL
  ,"rental" bool
  ,"categ_id" int4 NOT NULL
  ,"list_price" numeric
  ,"volume" float8
  ,"weight" numeric
  ,"sale_ok" bool
  ,"purchase_ok" bool
  ,"uom_id" int4 NOT NULL
  ,"uom_po_id" int4 NOT NULL
  ,"company_id" int4
  ,"active" bool
  ,"color" int4
  ,"default_code" varchar
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"service_type" varchar
  ,"sale_line_warn" varchar NOT NULL
  ,"sale_line_warn_msg" text
  ,"expense_policy" varchar
  ,"invoice_policy" varchar
  ,"responsible_id" int4 NOT NULL
  ,"sale_delay" float8
  ,"tracking" varchar NOT NULL
  ,"description_picking" text
  ,"description_pickingout" text
  ,"description_pickingin" text
  ,"can_be_expensed" bool
  ,"purchase_method" varchar
  ,"purchase_line_warn" varchar NOT NULL
  ,"purchase_line_warn_msg" text
  ,"service_tracking" varchar
  ,"invoicing_finished_task" bool
  ,"message_main_attachment_id" int4
  ,"service_to_purchase" bool
  ,"is_share" bool
  ,"short_name" varchar
  ,"display_on_website" bool
  ,"default_share_product" bool
  ,"minimum_quantity" int4
  ,"force_min_qty" bool
  ,"by_company" bool
  ,"by_individual" bool
  ,"customer" bool
  ,"mail_template" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template');



drop foreign table if exists odoo12_product_template_attribute_exclusion;


create FOREIGN TABLE odoo12_product_template_attribute_exclusion("id" int4 NOT NULL
  ,"product_template_attribute_value_id" int4
  ,"product_tmpl_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_exclusion');



drop foreign table if exists odoo12_product_template_attribute_line;


create FOREIGN TABLE odoo12_product_template_attribute_line("id" int4 NOT NULL
  ,"product_tmpl_id" int4 NOT NULL
  ,"attribute_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_line');



drop foreign table if exists odoo12_product_template_attribute_value;


create FOREIGN TABLE odoo12_product_template_attribute_value("id" int4 NOT NULL
  ,"product_tmpl_id" int4 NOT NULL
  ,"product_attribute_value_id" int4 NOT NULL
  ,"price_extra" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_value');



drop foreign table if exists odoo12_product_template_attribute_value_sale_order_line_rel;


create FOREIGN TABLE odoo12_product_template_attribute_value_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
  ,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_value_sale_order_line_rel');



drop foreign table if exists odoo12_project_create_invoice;


create FOREIGN TABLE odoo12_project_create_invoice("id" int4 NOT NULL
  ,"project_id" int4 NOT NULL
  ,"sale_order_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_create_invoice');



drop foreign table if exists odoo12_project_create_sale_order;


create FOREIGN TABLE odoo12_project_create_sale_order("id" int4 NOT NULL
  ,"project_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"product_id" int4
  ,"price_unit" float8
  ,"billable_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_create_sale_order');



drop foreign table if exists odoo12_project_create_sale_order_line;


create FOREIGN TABLE odoo12_project_create_sale_order_line("id" int4 NOT NULL
  ,"wizard_id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"price_unit" float8
  ,"employee_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_create_sale_order_line');



drop foreign table if exists odoo12_project_favorite_user_rel;


create FOREIGN TABLE odoo12_project_favorite_user_rel("project_id" int4 NOT NULL
  ,"user_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_favorite_user_rel');



drop foreign table if exists odoo12_project_profitability_report;


create FOREIGN TABLE odoo12_project_profitability_report("id" int8
  ,"project_id" int4
  ,"user_id" int4
  ,"sale_line_id" int4
  ,"analytic_account_id" int4
  ,"partner_id" int4
  ,"company_id" int4
  ,"currency_id" int4
  ,"sale_order_id" int4
  ,"order_confirmation_date" timestamp
  ,"product_id" int4
  ,"sale_qty_delivered_method" varchar
  ,"expense_amount_untaxed_to_invoice" numeric
  ,"expense_amount_untaxed_invoiced" numeric
  ,"amount_untaxed_to_invoice" numeric
  ,"amount_untaxed_invoiced" numeric
  ,"timesheet_unit_amount" float8
  ,"timesheet_cost" numeric
  ,"expense_cost" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_profitability_report');



drop foreign table if exists odoo12_project_project;
create FOREIGN TABLE odoo12_project_project("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"active" bool
  ,"sequence" int4
  ,"analytic_account_id" int4
  ,"label_tasks" varchar
  ,"resource_calendar_id" int4
  ,"color" int4
  ,"user_id" int4
  ,"alias_id" int4 NOT NULL
  ,"privacy_visibility" varchar NOT NULL
  ,"date_start" date
  ,"date" date
  ,"subtask_project_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"allow_timesheets" bool
  ,"sale_line_id" int4
  ,"name" varchar NOT NULL
  ,"partner_id" int4
  ,"company_id" int4 NOT NULL
  ,"openupgrade_legacy_12_0_analytic_account_id" int4
  ,"access_token" varchar
  ,"message_main_attachment_id" int4
  ,"percentage_satisfaction_task" int4
  ,"percentage_satisfaction_project" int4
  ,"rating_request_deadline" timestamp
  ,"rating_status" varchar NOT NULL
  ,"rating_status_period" varchar
  ,"portal_show_rating" bool
  ,"sale_order_id" int4
  ,"billable_type" varchar
  ,"type_id" int4
  ,"ticket_count" int4
  ,"label_tickets" varchar
  ,"todo_ticket_count" int4
  ,"project_status" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_project');



drop foreign table if exists odoo12_project_sale_line_employee_map;


create FOREIGN TABLE odoo12_project_sale_line_employee_map("id" int4 NOT NULL
  ,"project_id" int4 NOT NULL
  ,"employee_id" int4 NOT NULL
  ,"sale_line_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_sale_line_employee_map');



drop foreign table if exists odoo12_project_status;


create FOREIGN TABLE odoo12_project_status("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" varchar
  ,"status_sequence" int4
  ,"is_closed" bool
  ,"fold" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_status');



drop foreign table if exists odoo12_project_tags;


create FOREIGN TABLE odoo12_project_tags("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_tags');



drop foreign table if exists odoo12_project_tags_project_task_rel;


create FOREIGN TABLE odoo12_project_tags_project_task_rel("project_task_id" int4 NOT NULL
  ,"project_tags_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_tags_project_task_rel');



drop foreign table if exists odoo12_project_task;


create FOREIGN TABLE odoo12_project_task("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"active" bool
  ,"name" varchar NOT NULL
  ,"description" text
  ,"priority" varchar
  ,"sequence" int4
  ,"stage_id" int4
  ,"kanban_state" varchar NOT NULL
  ,"create_date" timestamp
  ,"write_date" timestamp
  ,"date_start" timestamp
  ,"date_end" timestamp
  ,"date_assign" timestamp
  ,"date_deadline" date
  ,"date_last_stage_update" timestamp
  ,"project_id" int4
  ,"notes" text
  ,"planned_hours" float8
  ,"remaining_hours" float8
  ,"user_id" int4
  ,"partner_id" int4
  ,"company_id" int4 NOT NULL
  ,"color" int4
  ,"displayed_image_id" int4
  ,"parent_id" int4
  ,"email_from" varchar
  ,"email_cc" varchar
  ,"working_hours_open" float8
  ,"working_hours_close" float8
  ,"working_days_open" float8
  ,"working_days_close" float8
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_uid" int4
  ,"write_uid" int4
  ,"effective_hours" float8
  ,"total_hours" float8
  ,"total_hours_spent" float8
  ,"progress" float8
  ,"delay_hours" float8
  ,"subtask_effective_hours" float8
  ,"sale_line_id" int4
  ,"invoiceable" bool
  ,"rating_last_value" float8
  ,"access_token" varchar
  ,"message_main_attachment_id" int4
  ,"sale_order_id" int4
  ,"billable_type" varchar
  ,"type_id" int4
  ,"ticket_count" int4
  ,"label_tickets" varchar
  ,"todo_ticket_count" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task');



drop foreign table if exists odoo12_project_task_assign_sale;


create FOREIGN TABLE odoo12_project_task_assign_sale("id" int4 NOT NULL
  ,"sale_line_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_assign_sale');



drop foreign table if exists odoo12_project_task_assign_so_line_rel;


create FOREIGN TABLE odoo12_project_task_assign_so_line_rel("task_id" int4 NOT NULL
  ,"wizard_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_assign_so_line_rel');



drop foreign table if exists odoo12_project_task_merge_wizard;


create FOREIGN TABLE odoo12_project_task_merge_wizard("id" int4 NOT NULL
  ,"user_id" int4
  ,"create_new_task" bool
  ,"target_task_name" varchar
  ,"target_project_id" int4
  ,"target_task_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_merge_wizard');



drop foreign table if exists odoo12_project_task_project_task_merge_wizard_rel;


create FOREIGN TABLE odoo12_project_task_project_task_merge_wizard_rel("project_task_merge_wizard_id" int4 NOT NULL
  ,"project_task_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_project_task_merge_wizard_rel');



drop foreign table if exists odoo12_project_task_type;


create FOREIGN TABLE odoo12_project_task_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"description" text
  ,"sequence" int4
  ,"legend_priority" varchar
  ,"legend_blocked" varchar NOT NULL
  ,"legend_done" varchar NOT NULL
  ,"legend_normal" varchar NOT NULL
  ,"mail_template_id" int4
  ,"fold" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"case_default" bool
  ,"invoiceable" bool
  ,"rating_template_id" int4
  ,"auto_validation_kanban_state" bool
  ,"closed" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_type');



drop foreign table if exists odoo12_project_task_type_rel;


create FOREIGN TABLE odoo12_project_task_type_rel("type_id" int4 NOT NULL
  ,"project_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_task_type_rel');



drop foreign table if exists odoo12_project_type;


create FOREIGN TABLE odoo12_project_type("id" int4 NOT NULL
  ,"parent_id" int4
  ,"name" varchar NOT NULL
  ,"complete_name" varchar
  ,"description" text
  ,"project_ok" bool
  ,"task_ok" bool
  ,"code" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'project_type');



drop foreign table if exists odoo12_public_holidays_next_year_wizard;


create FOREIGN TABLE odoo12_public_holidays_next_year_wizard("id" int4 NOT NULL
  ,"template_id" int4
  ,"year" int4
  ,"warning_existing" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'public_holidays_next_year_wizard');



drop foreign table if exists odoo12_purchase_bill_union;


create FOREIGN TABLE odoo12_purchase_bill_union("id" int4
  ,"name" varchar
  ,"reference" varchar
  ,"partner_id" int4
  ,"date" date
  ,"amount" numeric
  ,"currency_id" int4
  ,"company_id" int4
  ,"vendor_bill_id" int4
  ,"purchase_order_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'purchase_bill_union');



drop foreign table if exists odoo12_purchase_order;


create FOREIGN TABLE odoo12_purchase_order("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"origin" varchar
  ,"partner_ref" varchar
  ,"date_order" timestamp NOT NULL
  ,"date_approve" date
  ,"partner_id" int4 NOT NULL
  ,"dest_address_id" int4
  ,"currency_id" int4 NOT NULL
  ,"state" varchar
  ,"notes" text
  ,"invoice_count" int4
  ,"invoice_status" varchar
  ,"picking_count" int4
  ,"date_planned" timestamp
  ,"amount_untaxed" numeric
  ,"amount_tax" numeric
  ,"amount_total" numeric
  ,"fiscal_position_id" int4
  ,"payment_term_id" int4
  ,"incoterm_id" int4
  ,"create_uid" int4
  ,"company_id" int4 NOT NULL
  ,"picking_type_id" int4 NOT NULL
  ,"group_id" int4
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"user_id" int4
  ,"access_token" varchar
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'purchase_order');



drop foreign table if exists odoo12_purchase_order_line;


create FOREIGN TABLE odoo12_purchase_order_line("id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"sequence" int4
  ,"product_qty" numeric NOT NULL
  ,"date_planned" timestamp NOT NULL
  ,"product_uom" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"price_unit" numeric NOT NULL
  ,"price_subtotal" numeric
  ,"price_total" numeric
  ,"price_tax" float8
  ,"order_id" int4 NOT NULL
  ,"account_analytic_id" int4
  ,"company_id" int4
  ,"state" varchar
  ,"qty_invoiced" numeric
  ,"qty_received" numeric
  ,"partner_id" int4
  ,"currency_id" int4
  ,"orderpoint_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"product_uom_qty" float8
  ,"sale_order_id" int4
  ,"sale_line_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'purchase_order_line');



drop foreign table if exists odoo12_purchase_order_stock_picking_rel;


create FOREIGN TABLE odoo12_purchase_order_stock_picking_rel("purchase_order_id" int4 NOT NULL
  ,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'purchase_order_stock_picking_rel');



drop foreign table if exists odoo12_purchase_report;
create FOREIGN TABLE odoo12_purchase_report("id" int4
  ,"date_order" timestamp
  ,"state" varchar
  ,"date_approve" date
  ,"dest_address_id" int4
  ,"partner_id" int4
  ,"user_id" int4
  ,"company_id" int4
  ,"fiscal_position_id" int4
  ,"product_id" int4
  ,"product_tmpl_id" int4
  ,"category_id" int4
  ,"currency_id" int4
  ,"product_uom" int4
  ,"unit_quantity" numeric
  ,"delay" float8
  ,"delay_pass" float8
  ,"nbr_lines" int8
  ,"price_total" numeric(16,2)
  ,"negociation" numeric(16,2)
  ,"price_standard" numeric(16,2)
  ,"price_average" numeric(16,2)
  ,"country_id" int4
  ,"commercial_partner_id" int4
  ,"account_analytic_id" int4
  ,"weight" numeric
  ,"volume" float8
  ,"picking_type_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'purchase_report');



drop foreign table if exists odoo12_rating_rating;


create FOREIGN TABLE odoo12_rating_rating("id" int4 NOT NULL
  ,"res_name" varchar
  ,"res_model_id" int4
  ,"res_model" varchar
  ,"res_id" int4 NOT NULL
  ,"parent_res_name" varchar
  ,"parent_res_model_id" int4
  ,"parent_res_model" varchar
  ,"parent_res_id" int4
  ,"rated_partner_id" int4
  ,"partner_id" int4
  ,"rating" float8
  ,"rating_text" varchar
  ,"feedback" text
  ,"message_id" int4
  ,"access_token" varchar
  ,"consumed" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"website_published" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rating_rating');



drop foreign table if exists odoo12_rel_modules_langexport;


create FOREIGN TABLE odoo12_rel_modules_langexport("wiz_id" int4 NOT NULL
  ,"module_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rel_modules_langexport');



drop foreign table if exists odoo12_rel_server_actions;


create FOREIGN TABLE odoo12_rel_server_actions("server_id" int4 NOT NULL
  ,"action_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rel_server_actions');



drop foreign table if exists odoo12_report_aged_partner_balance;


create FOREIGN TABLE odoo12_report_aged_partner_balance("id" int4 NOT NULL
  ,"date_at" date
  ,"only_posted_moves" bool
  ,"company_id" int4
  ,"show_move_line_details" bool
  ,"open_items_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance');



drop foreign table if exists odoo12_report_aged_partner_balance_account;


create FOREIGN TABLE odoo12_report_aged_partner_balance_account("id" int4 NOT NULL
  ,"report_id" int4
  ,"account_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"cumul_amount_residual" numeric
  ,"cumul_current" numeric
  ,"cumul_age_30_days" numeric
  ,"cumul_age_60_days" numeric
  ,"cumul_age_90_days" numeric
  ,"cumul_age_120_days" numeric
  ,"cumul_older" numeric
  ,"percent_current" numeric
  ,"percent_age_30_days" numeric
  ,"percent_age_60_days" numeric
  ,"percent_age_90_days" numeric
  ,"percent_age_120_days" numeric
  ,"percent_older" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_account');



drop foreign table if exists odoo12_report_aged_partner_balance_line;


create FOREIGN TABLE odoo12_report_aged_partner_balance_line("id" int4 NOT NULL
  ,"report_partner_id" int4
  ,"partner" varchar
  ,"amount_residual" numeric
  ,"current" numeric
  ,"age_30_days" numeric
  ,"age_60_days" numeric
  ,"age_90_days" numeric
  ,"age_120_days" numeric
  ,"older" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_line');



drop foreign table if exists odoo12_report_aged_partner_balance_move_line;


create FOREIGN TABLE odoo12_report_aged_partner_balance_move_line("id" int4 NOT NULL
  ,"report_partner_id" int4
  ,"move_line_id" int4
  ,"date" date
  ,"date_due" date
  ,"entry" varchar
  ,"journal" varchar
  ,"account" varchar
  ,"partner" varchar
  ,"label" varchar
  ,"amount_residual" numeric
  ,"current" numeric
  ,"age_30_days" numeric
  ,"age_60_days" numeric
  ,"age_90_days" numeric
  ,"age_120_days" numeric
  ,"older" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_move_line');



drop foreign table if exists odoo12_report_aged_partner_balance_partner;


create FOREIGN TABLE odoo12_report_aged_partner_balance_partner("id" int4 NOT NULL
  ,"report_account_id" int4
  ,"partner_id" int4
  ,"name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_partner');



drop foreign table if exists odoo12_report_aged_partner_balance_res_partner_rel;


create FOREIGN TABLE odoo12_report_aged_partner_balance_res_partner_rel("report_aged_partner_balance_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_res_partner_rel');



drop foreign table if exists odoo12_report_all_channels_sales;


create FOREIGN TABLE odoo12_report_all_channels_sales("id" int4
  ,"name" varchar
  ,"partner_id" int4
  ,"product_id" int4
  ,"product_tmpl_id" int4
  ,"date_order" timestamp
  ,"user_id" int4
  ,"categ_id" int4
  ,"company_id" int4
  ,"price_total" numeric
  ,"pricelist_id" int4
  ,"analytic_account_id" int4
  ,"country_id" int4
  ,"team_id" int4
  ,"price_subtotal" numeric
  ,"product_qty" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_all_channels_sales');



drop foreign table if exists odoo12_report_general_ledger;


create FOREIGN TABLE odoo12_report_general_ledger("id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"fy_start_date" date
  ,"only_posted_moves" bool
  ,"hide_account_balance_at_0" bool
  ,"company_id" int4
  ,"centralize" bool
  ,"has_second_currency" bool
  ,"show_cost_center" bool
  ,"unaffected_earnings_account" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hide_account_at_0" bool
  ,"foreign_currency" bool
  ,"show_analytic_tags" bool
  ,"partner_ungrouped" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger');



drop foreign table if exists odoo12_report_general_ledger_account;


create FOREIGN TABLE odoo12_report_general_ledger_account("id" int4 NOT NULL
  ,"report_id" int4
  ,"account_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"initial_debit" numeric
  ,"initial_credit" numeric
  ,"initial_balance" numeric
  ,"final_debit" numeric
  ,"final_credit" numeric
  ,"final_balance" numeric
  ,"is_partner_account" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4
  ,"initial_balance_foreign_currency" numeric
  ,"final_balance_foreign_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_account');



drop foreign table if exists odoo12_report_general_ledger_move_line;


create FOREIGN TABLE odoo12_report_general_ledger_move_line("id" int4 NOT NULL
  ,"report_account_id" int4
  ,"report_partner_id" int4
  ,"move_line_id" int4
  ,"date" date
  ,"entry" varchar
  ,"journal" varchar
  ,"account" varchar
  ,"partner" varchar
  ,"label" varchar
  ,"cost_center" varchar
  ,"matching_number" varchar
  ,"debit" numeric
  ,"credit" numeric
  ,"cumul_balance" numeric
  ,"currency_name" varchar
  ,"amount_currency" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"taxes_description" varchar
  ,"tags" varchar
  ,"currency_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_move_line');



drop foreign table if exists odoo12_report_general_ledger_partner;


create FOREIGN TABLE odoo12_report_general_ledger_partner("id" int4 NOT NULL
  ,"report_account_id" int4
  ,"partner_id" int4
  ,"name" varchar
  ,"initial_debit" numeric
  ,"initial_credit" numeric
  ,"initial_balance" numeric
  ,"final_debit" numeric
  ,"final_credit" numeric
  ,"final_balance" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4
  ,"initial_balance_foreign_currency" numeric
  ,"final_balance_foreign_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_partner');



drop foreign table if exists odoo12_report_general_ledger_res_partner_rel;


create FOREIGN TABLE odoo12_report_general_ledger_res_partner_rel("report_general_ledger_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_res_partner_rel');



drop foreign table if exists odoo12_report_journal_ledger;


create FOREIGN TABLE odoo12_report_journal_ledger("id" int4 NOT NULL
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"company_id" int4 NOT NULL
  ,"move_target" varchar NOT NULL
  ,"sort_option" varchar NOT NULL
  ,"group_option" varchar NOT NULL
  ,"foreign_currency" bool
  ,"with_account_name" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger');



drop foreign table if exists odoo12_report_journal_ledger_journal;


create FOREIGN TABLE odoo12_report_journal_ledger_journal("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar
  ,"report_id" int4 NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"debit" numeric
  ,"credit" numeric
  ,"company_id" int4 NOT NULL
  ,"currency_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal');



drop foreign table if exists odoo12_report_journal_ledger_journal_tax_line;


create FOREIGN TABLE odoo12_report_journal_ledger_journal_tax_line("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"tax_id" int4
  ,"tax_name" varchar
  ,"tax_code" varchar
  ,"base_debit" numeric
  ,"base_credit" numeric
  ,"tax_debit" numeric
  ,"tax_credit" numeric
  ,"report_journal_ledger_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal_tax_line');



drop foreign table if exists odoo12_report_journal_ledger_move;


create FOREIGN TABLE odoo12_report_journal_ledger_move("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"report_journal_ledger_id" int4 NOT NULL
  ,"move_id" int4 NOT NULL
  ,"name" varchar
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move');



drop foreign table if exists odoo12_report_journal_ledger_move_line;


create FOREIGN TABLE odoo12_report_journal_ledger_move_line("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"report_journal_ledger_id" int4 NOT NULL
  ,"report_move_id" int4 NOT NULL
  ,"move_line_id" int4 NOT NULL
  ,"account_id" int4
  ,"account" varchar
  ,"account_code" varchar
  ,"account_type" varchar
  ,"partner" varchar
  ,"partner_id" int4
  ,"date" date
  ,"entry" varchar
  ,"label" varchar
  ,"debit" numeric
  ,"credit" numeric
  ,"company_currency_id" int4
  ,"amount_currency" numeric
  ,"currency_id" int4
  ,"currency_name" varchar
  ,"taxes_description" varchar
  ,"tax_id" int4
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move_line');



drop foreign table if exists odoo12_report_journal_ledger_report_tax_line;


create FOREIGN TABLE odoo12_report_journal_ledger_report_tax_line("id" int4 NOT NULL
  ,"report_id" int4 NOT NULL
  ,"tax_id" int4
  ,"tax_name" varchar
  ,"tax_code" varchar
  ,"base_debit" numeric
  ,"base_credit" numeric
  ,"tax_debit" numeric
  ,"tax_credit" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_report_tax_line');



drop foreign table if exists odoo12_report_layout;


create FOREIGN TABLE odoo12_report_layout("id" int4 NOT NULL
  ,"view_id" int4 NOT NULL
  ,"image" varchar
  ,"pdf" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_layout');



drop foreign table if exists odoo12_report_open_items;


create FOREIGN TABLE odoo12_report_open_items("id" int4 NOT NULL
  ,"date_at" date
  ,"only_posted_moves" bool
  ,"hide_account_balance_at_0" bool
  ,"company_id" int4
  ,"has_second_currency" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hide_account_at_0" bool
  ,"foreign_currency" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items');



drop foreign table if exists odoo12_report_open_items_account;


create FOREIGN TABLE odoo12_report_open_items_account("id" int4 NOT NULL
  ,"report_id" int4
  ,"account_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"final_amount_residual" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4
  ,"final_amount_total_due" numeric
  ,"final_amount_residual_currency" numeric
  ,"final_amount_total_due_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_account');



drop foreign table if exists odoo12_report_open_items_move_line;


create FOREIGN TABLE odoo12_report_open_items_move_line("id" int4 NOT NULL
  ,"report_partner_id" int4
  ,"move_line_id" int4
  ,"date" date
  ,"date_due" date
  ,"entry" varchar
  ,"journal" varchar
  ,"account" varchar
  ,"partner" varchar
  ,"label" varchar
  ,"amount_total_due" numeric
  ,"amount_residual" numeric
  ,"currency_name" varchar
  ,"amount_total_due_currency" numeric
  ,"amount_residual_currency" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_move_line');



drop foreign table if exists odoo12_report_open_items_partner;


create FOREIGN TABLE odoo12_report_open_items_partner("id" int4 NOT NULL
  ,"report_account_id" int4
  ,"partner_id" int4
  ,"name" varchar
  ,"final_amount_residual" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4
  ,"final_amount_total_due" numeric
  ,"final_amount_residual_currency" numeric
  ,"final_amount_total_due_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_partner');



drop foreign table if exists odoo12_report_open_items_res_partner_rel;


create FOREIGN TABLE odoo12_report_open_items_res_partner_rel("report_open_items_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_res_partner_rel');



drop foreign table if exists odoo12_report_paperformat;


create FOREIGN TABLE odoo12_report_paperformat("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"default" bool
  ,"format" varchar
  ,"margin_top" float8
  ,"margin_bottom" float8
  ,"margin_left" float8
  ,"margin_right" float8
  ,"page_height" int4
  ,"page_width" int4
  ,"orientation" varchar
  ,"header_line" bool
  ,"header_spacing" int4
  ,"dpi" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_paperformat');



drop foreign table if exists odoo12_report_project_task_user;


create FOREIGN TABLE odoo12_report_project_task_user("nbr" int4
  ,"id" int4
  ,"date_start" timestamp
  ,"date_end" timestamp
  ,"date_last_stage_update" timestamp
  ,"date_deadline" date
  ,"user_id" int4
  ,"project_id" int4
  ,"priority" varchar
  ,"name" varchar
  ,"company_id" int4
  ,"partner_id" int4
  ,"stage_id" int4
  ,"state" varchar
  ,"working_days_close" float8
  ,"working_days_open" float8
  ,"delay_endings_days" float8
  ,"progress" float8
  ,"hours_effective" float8
  ,"remaining_hours" float8
  ,"hours_planned" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_project_task_user');



drop foreign table if exists odoo12_report_stock_forecast;


create FOREIGN TABLE odoo12_report_stock_forecast("id" int4
  ,"product_id" int4
  ,"date" text
  ,"quantity" float8
  ,"cumulative_quantity" float8
  ,"company_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_stock_forecast');



drop foreign table if exists odoo12_report_trial_balance;


create FOREIGN TABLE odoo12_report_trial_balance("id" int4 NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"fy_start_date" date
  ,"only_posted_moves" bool
  ,"hide_account_balance_at_0" bool
  ,"company_id" int4
  ,"show_partner_details" bool
  ,"general_ledger_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hide_account_at_0" bool
  ,"foreign_currency" bool
  ,"hierarchy_on" varchar NOT NULL
  ,"limit_hierarchy_level" bool
  ,"show_hierarchy_level" int4
  ,"hide_parent_hierarchy_level" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance');



drop foreign table if exists odoo12_report_trial_balance_account;


create FOREIGN TABLE odoo12_report_trial_balance_account("id" int4 NOT NULL
  ,"report_id" int4
  ,"account_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"initial_balance" numeric
  ,"debit" numeric
  ,"credit" numeric
  ,"final_balance" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"sequence" varchar
  ,"level" int4
  ,"account_group_id" int4
  ,"parent_id" int4
  ,"child_account_ids" varchar
  ,"currency_id" int4
  ,"initial_balance_foreign_currency" numeric
  ,"period_balance" numeric
  ,"final_balance_foreign_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_account');



drop foreign table if exists odoo12_report_trial_balance_partner;


create FOREIGN TABLE odoo12_report_trial_balance_partner("id" int4 NOT NULL
  ,"report_account_id" int4
  ,"partner_id" int4
  ,"name" varchar
  ,"initial_balance" numeric
  ,"debit" numeric
  ,"credit" numeric
  ,"final_balance" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"currency_id" int4
  ,"initial_balance_foreign_currency" numeric
  ,"period_balance" numeric
  ,"final_balance_foreign_currency" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_partner');



drop foreign table if exists odoo12_report_trial_balance_res_partner_rel;


create FOREIGN TABLE odoo12_report_trial_balance_res_partner_rel("report_trial_balance_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_res_partner_rel');



drop foreign table if exists odoo12_report_vat_report;


create FOREIGN TABLE odoo12_report_vat_report("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_from" date
  ,"date_to" date
  ,"based_on" varchar NOT NULL
  ,"tax_detail" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report');



drop foreign table if exists odoo12_report_vat_report_tax;


create FOREIGN TABLE odoo12_report_vat_report_tax("id" int4 NOT NULL
  ,"report_tax_id" int4
  ,"tax_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"net" numeric
  ,"tax" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report_tax');



drop foreign table if exists odoo12_report_vat_report_taxtag;


create FOREIGN TABLE odoo12_report_vat_report_taxtag("id" int4 NOT NULL
  ,"report_id" int4
  ,"taxtag_id" int4
  ,"taxgroup_id" int4
  ,"code" varchar
  ,"name" varchar
  ,"net" numeric
  ,"tax" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report_taxtag');



drop foreign table if exists odoo12_res_authentication_attempt;


create FOREIGN TABLE odoo12_res_authentication_attempt("id" int4 NOT NULL
  ,"login" varchar
  ,"remote" varchar
  ,"result" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_authentication_attempt');



drop foreign table if exists odoo12_res_bank;


create FOREIGN TABLE odoo12_res_bank("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"street" varchar
  ,"street2" varchar
  ,"zip" varchar
  ,"city" varchar
  ,"state" int4
  ,"country" int4
  ,"email" varchar
  ,"phone" varchar
  ,"active" bool
  ,"bic" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"code" varchar
  ,"lname" varchar(128)
  ,"vat" varchar(32)
  ,"website" varchar(64))SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_bank');



drop foreign table if exists odoo12_res_better_zip;


create FOREIGN TABLE odoo12_res_better_zip("id" int4 NOT NULL
  ,"name" varchar
  ,"code" varchar(64)
  ,"city" varchar NOT NULL
  ,"city_id" int4
  ,"state_id" int4
  ,"country_id" int4
  ,"latitude" float8
  ,"longitude" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_better_zip');



drop foreign table if exists odoo12_res_city;


create FOREIGN TABLE odoo12_res_city("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"zipcode" varchar
  ,"country_id" int4 NOT NULL
  ,"state_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_city');



drop foreign table if exists odoo12_res_city_zip;


create FOREIGN TABLE odoo12_res_city_zip("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"city_id" int4 NOT NULL
  ,"display_name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"openupgrade_legacy_12_0_better_zip_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_city_zip');



drop foreign table if exists odoo12_res_company;


create FOREIGN TABLE odoo12_res_company("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"sequence" int4
  ,"parent_id" int4
  ,"report_header" text
  ,"report_footer" text
  ,"logo_web" bytea
  ,"account_no" varchar
  ,"email" varchar
  ,"phone" varchar
  ,"company_registry" varchar
  ,"paperformat_id" int4
  ,"openupgrade_legacy_12_0_external_report_layout" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"resource_calendar_id" int4
  ,"fiscalyear_last_day" int4 NOT NULL
  ,"fiscalyear_last_month" int4 NOT NULL
  ,"period_lock_date" date
  ,"fiscalyear_lock_date" date
  ,"transfer_account_id" int4
  ,"expects_chart_of_accounts" bool
  ,"chart_template_id" int4
  ,"bank_account_code_prefix" varchar
  ,"cash_account_code_prefix" varchar
  ,"accounts_code_digits" int4
  ,"tax_cash_basis_journal_id" int4
  ,"tax_calculation_rounding_method" varchar
  ,"currency_exchange_journal_id" int4
  ,"anglo_saxon_accounting" bool
  ,"property_stock_account_input_categ_id" int4
  ,"property_stock_account_output_categ_id" int4
  ,"property_stock_valuation_account_id" int4
  ,"overdue_msg" text
  ,"tax_exigibility" bool
  ,"account_opening_move_id" int4
  ,"account_setup_company_data_done" bool
  ,"account_setup_bank_data_done" bool
  ,"account_setup_fy_data_done" bool
  ,"account_setup_coa_done" bool
  ,"account_setup_bar_closed" bool
  ,"project_time_mode_id" int4
  ,"vat_check_vies" bool
  ,"sale_note" text
  ,"phone_international_format" varchar
  ,"favicon_backend" bytea
  ,"favicon_backend_mimetype" varchar
  ,"password_expiration" int4
  ,"password_length" int4
  ,"password_lower" int4
  ,"password_upper" int4
  ,"password_numeric" int4
  ,"password_special" int4
  ,"password_history" int4
  ,"password_minimum" int4
  ,"propagation_minimum_delta" int4
  ,"internal_transit_location_id" int4
  ,"leave_timesheet_project_id" int4
  ,"leave_timesheet_task_id" int4
  ,"po_lead" float8 NOT NULL
  ,"po_lock" varchar
  ,"po_double_validation" varchar
  ,"po_double_validation_amount" numeric
  ,"security_lead" float8 NOT NULL
  ,"initiating_party_issuer" varchar(35)
  ,"initiating_party_identifier" varchar(35)
  ,"sepa_creditor_identifier" varchar(35)
  ,"external_report_layout_id" int4
  ,"base_onboarding_company_state" varchar
  ,"nomenclature_id" int4
  ,"partner_gid" int4
  ,"snailmail_color" bool
  ,"snailmail_duplex" bool
  ,"incoterm_id" int4
  ,"transfer_account_code_prefix" varchar
  ,"account_sale_tax_id" int4
  ,"account_purchase_tax_id" int4
  ,"account_bank_reconciliation_start" date
  ,"invoice_reference_type" varchar
  ,"qr_code" bool
  ,"invoice_is_email" bool
  ,"invoice_is_print" bool
  ,"account_setup_bank_data_state" varchar
  ,"account_setup_fy_data_state" varchar
  ,"account_setup_coa_state" varchar
  ,"account_onboarding_invoice_layout_state" varchar
  ,"account_onboarding_sample_invoice_state" varchar
  ,"account_onboarding_sale_tax_state" varchar
  ,"account_invoice_onboarding_state" varchar
  ,"account_dashboard_onboarding_state" varchar
  ,"password_estimate" int4
  ,"create_new_line_at_contract_line_renew" bool
  ,"timesheet_encode_uom_id" int4 NOT NULL
  ,"payment_acquirer_onboarding_state" varchar
  ,"payment_onboarding_payment_method" varchar
  ,"portal_confirmation_sign" bool
  ,"portal_confirmation_pay" bool
  ,"quotation_validity_days" int4
  ,"sale_quotation_onboarding_state" varchar
  ,"sale_onboarding_order_confirmation_state" varchar
  ,"sale_onboarding_sample_quotation_state" varchar
  ,"sale_onboarding_payment_method" varchar
  ,"initiating_party_scheme" varchar(35)
  ,"tax_agency_id" int4
  ,"social_twitter" varchar
  ,"social_facebook" varchar
  ,"social_github" varchar
  ,"social_linkedin" varchar
  ,"social_youtube" varchar
  ,"social_googleplus" varchar
  ,"social_instagram" varchar
  ,"coop_email_contact" varchar
  ,"subscription_maximum_amount" float8
  ,"default_country_id" int4
  ,"default_lang_id" int4
  ,"allow_id_card_upload" bool
  ,"create_user" bool
  ,"board_representative" varchar
  ,"signature_scan" bytea
  ,"unmix_share_type" bool
  ,"display_logo1" bool
  ,"display_logo2" bool
  ,"bottom_logo1" bytea
  ,"bottom_logo2" bytea
  ,"display_data_policy_approval" bool
  ,"data_policy_approval_required" bool
  ,"data_policy_approval_text" text
  ,"display_internal_rules_approval" bool
  ,"internal_rules_approval_required" bool
  ,"internal_rules_approval_text" text
  ,"display_financial_risk_approval" bool
  ,"financial_risk_approval_required" bool
  ,"financial_risk_approval_text" text
  ,"display_generic_rules_approval" bool
  ,"generic_rules_approval_required" bool
  ,"generic_rules_approval_text" text
  ,"send_certificate_email" bool
  ,"send_confirmation_email" bool
  ,"send_capital_release_email" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_company');



drop foreign table if exists odoo12_res_company_users_rel;


create FOREIGN TABLE odoo12_res_company_users_rel("cid" int4 NOT NULL
  ,"user_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_company_users_rel');



drop foreign table if exists odoo12_res_config;


create FOREIGN TABLE odoo12_res_config("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config');



drop foreign table if exists odoo12_res_config_installer;


create FOREIGN TABLE odoo12_res_config_installer("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config_installer');



drop foreign table if exists odoo12_res_config_settings;


create FOREIGN TABLE odoo12_res_config_settings("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"group_multi_company" bool
  ,"company_id" int4 NOT NULL
  ,"user_default_rights" bool
  ,"external_email_server_default" bool
  ,"module_base_import" bool
  ,"module_google_calendar" bool
  ,"module_google_drive" bool
  ,"module_google_spreadsheet" bool
  ,"module_auth_oauth" bool
  ,"module_auth_ldap" bool
  ,"module_base_gengo" bool
  ,"module_inter_company_rules" bool
  ,"module_pad" bool
  ,"module_voip" bool
  ,"company_share_partner" bool
  ,"default_custom_report_footer" bool
  ,"group_multi_currency" bool
  ,"fail_counter" int4
  ,"alias_domain" varchar
  ,"auth_signup_reset_password" bool
  ,"auth_signup_uninvited" varchar
  ,"auth_signup_template_user_id" int4
  ,"company_share_product" bool
  ,"group_uom" bool
  ,"group_product_variant" bool
  ,"group_stock_packaging" bool
  ,"group_sale_pricelist" bool
  ,"group_product_pricelist" bool
  ,"group_pricelist_item" bool
  ,"chart_template_id" int4
  ,"module_account_accountant" bool
  ,"group_analytic_accounting" bool
  ,"group_warning_account" bool
  ,"group_cash_rounding" bool
  ,"module_account_asset" bool
  ,"module_account_deferred_revenue" bool
  ,"module_account_budget" bool
  ,"module_account_payment" bool
  ,"module_account_reports" bool
  ,"module_account_reports_followup" bool
  ,"module_l10n_us_check_printing" bool
  ,"module_account_batch_deposit" bool
  ,"module_account_sepa" bool
  ,"module_account_sepa_direct_debit" bool
  ,"module_account_plaid" bool
  ,"module_account_yodlee" bool
  ,"module_account_bank_statement_import_qif" bool
  ,"module_account_bank_statement_import_ofx" bool
  ,"module_account_bank_statement_import_csv" bool
  ,"module_account_bank_statement_import_camt" bool
  ,"module_currency_rate_live" bool
  ,"module_print_docsaway" bool
  ,"module_product_margin" bool
  ,"module_l10n_eu_service" bool
  ,"module_account_taxcloud" bool
  ,"module_hr_timesheet" bool
  ,"module_rating_project" bool
  ,"module_project_forecast" bool
  ,"group_subtask_project" bool
  ,"use_sale_note" bool
  ,"group_discount_per_so_line" bool
  ,"module_sale_margin" bool
  ,"group_sale_layout" bool
  ,"group_warning_sale" bool
  ,"portal_confirmation" bool
  ,"portal_confirmation_options" varchar
  ,"module_sale_payment" bool
  ,"module_website_quote" bool
  ,"group_sale_delivery_address" bool
  ,"multi_sales_price" bool
  ,"multi_sales_price_method" varchar
  ,"sale_pricelist_setting" varchar
  ,"group_show_line_subtotals_tax_excluded" bool
  ,"group_show_line_subtotals_tax_included" bool
  ,"group_proforma_sales" bool
  ,"show_line_subtotals_tax_selection" varchar NOT NULL
  ,"default_invoice_policy" varchar
  ,"deposit_default_product_id" int4
  ,"auto_done_setting" bool
  ,"module_website_sale_digital" bool
  ,"module_delivery" bool
  ,"module_delivery_dhl" bool
  ,"module_delivery_fedex" bool
  ,"module_delivery_ups" bool
  ,"module_delivery_usps" bool
  ,"module_delivery_bpost" bool
  ,"module_product_email_template" bool
  ,"module_sale_coupon" bool
  ,"module_hr_org_chart" bool
  ,"module_l10n_fr_hr_payroll" bool
  ,"module_l10n_be_hr_payroll" bool
  ,"module_l10n_in_hr_payroll" bool
  ,"crm_alias_prefix" varchar
  ,"generate_lead_from_alias" bool
  ,"group_use_lead" bool
  ,"module_crm_phone_validation" bool
  ,"module_web_clearbit" bool
  ,"module_procurement_jit" int4
  ,"module_product_expiry" bool
  ,"group_stock_production_lot" bool
  ,"group_stock_tracking_lot" bool
  ,"group_stock_tracking_owner" bool
  ,"group_stock_adv_location" bool
  ,"group_warning_stock" bool
  ,"use_propagation_minimum_delta" bool
  ,"module_stock_picking_batch" bool
  ,"module_stock_barcode" bool
  ,"group_stock_multi_locations" bool
  ,"group_stock_multi_warehouses" bool
  ,"expense_alias_prefix" varchar
  ,"use_mailgateway" bool
  ,"module_sale_management" bool
  ,"module_project_timesheet_synchro" bool
  ,"module_sale_timesheet" bool
  ,"module_project_timesheet_holidays" bool
  ,"module_stock_landed_costs" bool
  ,"lock_confirmed_po" bool
  ,"po_order_approval" bool
  ,"default_purchase_method" varchar
  ,"module_purchase_requisition" bool
  ,"group_warning_purchase" bool
  ,"module_stock_dropshipping" bool
  ,"group_manage_vendor_price" bool
  ,"module_account_3way_match" bool
  ,"is_installed_sale" bool
  ,"group_analytic_account_for_purchases" bool
  ,"use_po_lead" bool
  ,"group_route_so_lines" bool
  ,"module_sale_order_dates" bool
  ,"group_display_incoterm" bool
  ,"use_security_lead" bool
  ,"default_picking_policy" varchar NOT NULL
  ,"group_pain_multiple_identifier" bool
  ,"module_web_unsplash" bool
  ,"module_partner_autocomplete" bool
  ,"show_effect" bool
  ,"minlength" int4
  ,"unsplash_access_key" varchar
  ,"product_weight_in_lbs" varchar
  ,"digest_emails" bool
  ,"digest_id" int4
  ,"group_lot_on_delivery_slip" bool
  ,"module_delivery_easypost" bool
  ,"group_analytic_tags" bool
  ,"group_fiscal_year" bool
  ,"group_products_in_bills" bool
  ,"module_account_check_printing" bool
  ,"module_account_batch_payment" bool
  ,"module_account_intrastat" bool
  ,"module_account_invoice_extract" bool
  ,"module_crm_reveal" bool
  ,"group_project_rating" bool
  ,"module_account_asset_management" bool
  ,"use_quotation_validity_days" bool
  ,"group_sale_order_dates" bool
  ,"automatic_invoice" bool
  ,"template_id" int4
  ,"group_sale_order_template" bool
  ,"default_sale_order_template_id" int4
  ,"module_sale_quotation_builder" bool
  ,"cal_client_id" varchar
  ,"cal_client_secret" varchar
  ,"server_uri" varchar
  ,"group_mass_mailing_campaign" bool
  ,"mass_mailing_outgoing_mail_server" bool
  ,"mass_mailing_mail_server_id" int4
  ,"show_blacklist_buttons" bool
  ,"partner_names_order" varchar NOT NULL
  ,"module_website_hr_recruitment" bool
  ,"module_hr_recruitment_survey" bool
  ,"website_id" int4
  ,"module_website_version" bool
  ,"module_website_links" bool
  ,"group_multi_website" bool
  ,"group_website_popup_on_exit" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config_settings');



drop foreign table if exists odoo12_res_country;


create FOREIGN TABLE odoo12_res_country("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar(2)
  ,"address_format" text
  ,"address_view_id" int4
  ,"currency_id" int4
  ,"phone_code" int4
  ,"name_position" varchar
  ,"vat_label" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"enforce_cities" bool
  ,"geonames_state_name_column" int4
  ,"geonames_state_code_column" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country');



drop foreign table if exists odoo12_res_country_group;


create FOREIGN TABLE odoo12_res_country_group("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_group');



drop foreign table if exists odoo12_res_country_group_pricelist_rel;


create FOREIGN TABLE odoo12_res_country_group_pricelist_rel("pricelist_id" int4 NOT NULL
  ,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_group_pricelist_rel');



drop foreign table if exists odoo12_res_country_res_country_group_rel;


create FOREIGN TABLE odoo12_res_country_res_country_group_rel("res_country_id" int4 NOT NULL
  ,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_res_country_group_rel');



drop foreign table if exists odoo12_res_country_state;


create FOREIGN TABLE odoo12_res_country_state("id" int4 NOT NULL
  ,"country_id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_state');



drop foreign table if exists odoo12_res_currency;


create FOREIGN TABLE odoo12_res_currency("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"symbol" varchar NOT NULL
  ,"rounding" numeric
  ,"active" bool
  ,"position" varchar
  ,"currency_unit_label" varchar
  ,"currency_subunit_label" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"decimal_places" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_currency');



drop foreign table if exists odoo12_res_currency_rate;


create FOREIGN TABLE odoo12_res_currency_rate("id" int4 NOT NULL
  ,"name" date NOT NULL
  ,"rate" numeric
  ,"currency_id" int4
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_currency_rate');



drop foreign table if exists odoo12_res_groups;


create FOREIGN TABLE odoo12_res_groups("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"comment" text
  ,"category_id" int4
  ,"color" int4
  ,"share" bool
  ,"is_portal" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups');



drop foreign table if exists odoo12_res_groups_implied_rel;


create FOREIGN TABLE odoo12_res_groups_implied_rel("gid" int4 NOT NULL
  ,"hid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_implied_rel');



drop foreign table if exists odoo12_res_groups_report_rel;


create FOREIGN TABLE odoo12_res_groups_report_rel("uid" int4 NOT NULL
  ,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_report_rel');



drop foreign table if exists odoo12_res_groups_users_rel;


create FOREIGN TABLE odoo12_res_groups_users_rel("gid" int4 NOT NULL
  ,"uid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_users_rel');



drop foreign table if exists odoo12_res_lang;


create FOREIGN TABLE odoo12_res_lang("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"code" varchar NOT NULL
  ,"iso_code" varchar
  ,"translatable" bool
  ,"active" bool
  ,"direction" varchar NOT NULL
  ,"date_format" varchar NOT NULL
  ,"time_format" varchar NOT NULL
  ,"grouping" varchar NOT NULL
  ,"decimal_point" varchar NOT NULL
  ,"thousands_sep" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"week_start" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_lang');



drop foreign table if exists odoo12_res_partner;


create FOREIGN TABLE odoo12_res_partner("id" int4 NOT NULL
  ,"name" varchar
  ,"company_id" int4
  ,"display_name" varchar
  ,"date" date
  ,"title" int4
  ,"parent_id" int4
  ,"ref" varchar
  ,"lang" varchar
  ,"tz" varchar
  ,"user_id" int4
  ,"vat" varchar
  ,"website" varchar
  ,"comment" text
  ,"credit_limit" float8
  ,"barcode" varchar
  ,"active" bool
  ,"customer" bool
  ,"supplier" bool
  ,"employee" bool
  ,"function" varchar
  ,"type" varchar
  ,"street" varchar
  ,"street2" varchar
  ,"zip" varchar
  ,"city" varchar
  ,"state_id" int4
  ,"country_id" int4
  ,"email" varchar
  ,"phone" varchar
  ,"mobile" varchar
  ,"is_company" bool
  ,"industry_id" int4
  ,"color" int4
  ,"partner_share" bool
  ,"commercial_partner_id" int4
  ,"commercial_partner_country_id" int4
  ,"commercial_company_name" varchar
  ,"company_name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_bounce" int4
  ,"openupgrade_legacy_12_0_opt_out" bool
  ,"message_last_post" timestamp
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"signup_token" varchar
  ,"signup_type" varchar
  ,"signup_expiration" timestamp
  ,"team_id" int4
  ,"debit_limit" numeric
  ,"last_time_entries_checked" timestamp
  ,"invoice_warn" varchar
  ,"invoice_warn_msg" text
  ,"sale_warn" varchar
  ,"sale_warn_msg" text
  ,"calendar_last_notif_ack" timestamp
  ,"city_id" int4
  ,"openupgrade_legacy_12_0_zip_id" int4
  ,"tracking_emails_count" int4
  ,"email_score" float8
  ,"email_bounced" bool
  ,"picking_warn" varchar
  ,"picking_warn_msg" text
  ,"purchase_warn" varchar
  ,"purchase_warn_msg" text
  ,"comercial" varchar(128)
  ,"aeat_anonymous_cash_customer" bool
  ,"message_main_attachment_id" int4
  ,"partner_gid" int4
  ,"additional_info" varchar
  ,"zip_id" int4
  ,"not_in_mod347" bool
  ,"mass_mailing_contacts_count" int4
  ,"mass_mailing_stats_count" int4
  ,"birthdate_date" date
  ,"firstname" varchar
  ,"lastname" varchar
  ,"cooperator" bool
  ,"member" bool
  ,"coop_candidate" bool
  ,"old_member" bool
  ,"gender" varchar
  ,"cooperator_register_number" int4
  ,"company_register_number" varchar
  ,"cooperator_type" varchar
  ,"effective_date" date
  ,"representative" bool
  ,"representative_of_member_company" bool
  ,"legal_form" varchar
  ,"data_policy_approved" bool
  ,"internal_rules_approved" bool
  ,"financial_risk_approved" bool
  ,"generic_rules_approved" bool
  ,"website_id" int4
  ,"website_meta_title" varchar
  ,"website_meta_description" text
  ,"website_meta_keywords" varchar
  ,"website_meta_og_img" varchar
  ,"is_published" bool
  ,"website_description" text
  ,"website_short_description" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner');



drop foreign table if exists odoo12_res_partner_autocomplete_sync;


create FOREIGN TABLE odoo12_res_partner_autocomplete_sync("id" int4 NOT NULL
  ,"partner_id" int4
  ,"synched" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_autocomplete_sync');



drop foreign table if exists odoo12_res_partner_bank;


create FOREIGN TABLE odoo12_res_partner_bank("id" int4 NOT NULL
  ,"acc_number" varchar NOT NULL
  ,"sanitized_acc_number" varchar
  ,"partner_id" int4 NOT NULL
  ,"bank_id" int4
  ,"sequence" int4
  ,"currency_id" int4
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"acc_type" varchar
  ,"acc_holder_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_bank');



drop foreign table if exists odoo12_res_partner_category;


create FOREIGN TABLE odoo12_res_partner_category("id" int4 NOT NULL
  ,"parent_left" int4
  ,"parent_right" int4
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"parent_id" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"parent_path" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_category');



drop foreign table if exists odoo12_res_partner_industry;


create FOREIGN TABLE odoo12_res_partner_industry("id" int4 NOT NULL
  ,"name" varchar
  ,"full_name" varchar
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_industry');



drop foreign table if exists odoo12_res_partner_res_partner_category_rel;


create FOREIGN TABLE odoo12_res_partner_res_partner_category_rel("category_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_res_partner_category_rel');



drop foreign table if exists odoo12_res_partner_title;


create FOREIGN TABLE odoo12_res_partner_title("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"shortcut" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_title');



drop foreign table if exists odoo12_res_partner_trial_balance_report_wizard_rel;


create FOREIGN TABLE odoo12_res_partner_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
  ,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_trial_balance_report_wizard_rel');



drop foreign table if exists odoo12_res_request_link;


create FOREIGN TABLE odoo12_res_request_link("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"object" varchar NOT NULL
  ,"priority" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_request_link');



drop foreign table if exists odoo12_res_users;


create FOREIGN TABLE odoo12_res_users("id" int4 NOT NULL
  ,"active" bool
  ,"login" varchar NOT NULL
  ,"password" varchar
  ,"company_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"signature" text
  ,"action_id" int4
  ,"share" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"password_crypt" varchar
  ,"alias_id" int4
  ,"notification_type" varchar NOT NULL
  ,"sale_team_id" int4
  ,"target_sales_won" int4
  ,"target_sales_done" int4
  ,"password_write_date" timestamp
  ,"target_sales_invoiced" int4
  ,"chatter_position" varchar
  ,"google_calendar_rtoken" varchar
  ,"google_calendar_token" varchar
  ,"google_calendar_token_validity" timestamp
  ,"google_calendar_last_sync_date" timestamp
  ,"google_calendar_cal_id" varchar
  ,"website_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_users');



drop foreign table if exists odoo12_res_users_log;


create FOREIGN TABLE odoo12_res_users_log("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_users_log');



drop foreign table if exists odoo12_res_users_pass_history;


create FOREIGN TABLE odoo12_res_users_pass_history("id" int4 NOT NULL
  ,"user_id" int4
  ,"password_crypt" varchar
  ,"date" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_users_pass_history');



drop foreign table if exists odoo12_resource_calendar;


create FOREIGN TABLE odoo12_resource_calendar("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hours_per_day" float8
  ,"tz" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar');



drop foreign table if exists odoo12_resource_calendar_attendance;


create FOREIGN TABLE odoo12_resource_calendar_attendance("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"dayofweek" varchar NOT NULL
  ,"date_from" date
  ,"date_to" date
  ,"hour_from" float8 NOT NULL
  ,"hour_to" float8 NOT NULL
  ,"calendar_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"day_period" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar_attendance');



drop foreign table if exists odoo12_resource_calendar_leaves;


create FOREIGN TABLE odoo12_resource_calendar_leaves("id" int4 NOT NULL
  ,"name" varchar
  ,"company_id" int4
  ,"calendar_id" int4
  ,"date_from" timestamp NOT NULL
  ,"date_to" timestamp NOT NULL
  ,"openupgrade_legacy_12_0_tz" varchar
  ,"resource_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"openupgrade_legacy_12_0_holiday_id" int4
  ,"time_type" varchar
  ,"holiday_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar_leaves');



drop foreign table if exists odoo12_resource_resource;


create FOREIGN TABLE odoo12_resource_resource("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4
  ,"resource_type" varchar NOT NULL
  ,"user_id" int4
  ,"time_efficiency" float8 NOT NULL
  ,"calendar_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"tz" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_resource');



drop foreign table if exists odoo12_resource_test;


create FOREIGN TABLE odoo12_resource_test("id" int4 NOT NULL
  ,"name" varchar
  ,"resource_id" int4 NOT NULL
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"resource_calendar_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_test');



drop foreign table if exists odoo12_rule_group_rel;


create FOREIGN TABLE odoo12_rule_group_rel("rule_group_id" int4 NOT NULL
  ,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rule_group_rel');



drop foreign table if exists odoo12_sale_advance_payment_inv;


create FOREIGN TABLE odoo12_sale_advance_payment_inv("id" int4 NOT NULL
  ,"advance_payment_method" varchar NOT NULL
  ,"product_id" int4
  ,"count" int4
  ,"amount" numeric
  ,"deposit_account_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_advance_payment_inv');



drop foreign table if exists odoo12_sale_layout_category;


create FOREIGN TABLE odoo12_sale_layout_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4 NOT NULL
  ,"subtotal" bool
  ,"pagebreak" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_layout_category');



drop foreign table if exists odoo12_sale_order;


create FOREIGN TABLE odoo12_sale_order("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"name" varchar NOT NULL
  ,"origin" varchar
  ,"client_order_ref" varchar
  ,"access_token" varchar
  ,"state" varchar
  ,"date_order" timestamp NOT NULL
  ,"validity_date" date
  ,"create_date" timestamp
  ,"confirmation_date" timestamp
  ,"user_id" int4
  ,"partner_id" int4 NOT NULL
  ,"partner_invoice_id" int4 NOT NULL
  ,"partner_shipping_id" int4 NOT NULL
  ,"pricelist_id" int4 NOT NULL
  ,"analytic_account_id" int4
  ,"invoice_status" varchar
  ,"note" text
  ,"amount_untaxed" numeric
  ,"amount_tax" numeric
  ,"amount_total" numeric
  ,"payment_term_id" int4
  ,"fiscal_position_id" int4
  ,"company_id" int4
  ,"team_id" int4
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"campaign_id" int4
  ,"source_id" int4
  ,"medium_id" int4
  ,"opportunity_id" int4
  ,"margin" numeric
  ,"incoterm" int4
  ,"picking_policy" varchar NOT NULL
  ,"warehouse_id" int4 NOT NULL
  ,"procurement_group_id" int4
  ,"message_main_attachment_id" int4
  ,"reference" varchar
  ,"require_signature" bool
  ,"require_payment" bool
  ,"currency_rate" numeric
  ,"signed_by" varchar
  ,"commitment_date" timestamp
  ,"sale_order_template_id" int4
  ,"effective_date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order');



drop foreign table if exists odoo12_sale_order_line;
create FOREIGN TABLE odoo12_sale_order_line("id" int4 NOT NULL
  ,"order_id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"sequence" int4
  ,"invoice_status" varchar
  ,"price_unit" numeric NOT NULL
  ,"price_subtotal" numeric
  ,"price_tax" float8
  ,"price_total" numeric
  ,"price_reduce" numeric
  ,"price_reduce_taxinc" numeric
  ,"price_reduce_taxexcl" numeric
  ,"discount" numeric
  ,"product_id" int4
  ,"product_uom_qty" numeric NOT NULL
  ,"product_uom" int4
  ,"qty_delivered" numeric
  ,"qty_to_invoice" numeric
  ,"qty_invoiced" numeric
  ,"salesman_id" int4
  ,"currency_id" int4
  ,"company_id" int4
  ,"order_partner_id" int4
  ,"is_downpayment" bool
  ,"state" varchar
  ,"customer_lead" float8 NOT NULL
  ,"amt_to_invoice" numeric
  ,"amt_invoiced" numeric
  ,"layout_category_id" int4
  ,"layout_category_sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"margin" numeric
  ,"purchase_price" numeric
  ,"product_packaging" int4
  ,"route_id" int4
  ,"task_id" int4
  ,"is_service" bool
  ,"qty_delivered_manual" numeric
  ,"display_type" varchar
  ,"qty_delivered_method" varchar
  ,"untaxed_amount_invoiced" numeric
  ,"untaxed_amount_to_invoice" numeric
  ,"is_expense" bool
  ,"project_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_line');



drop foreign table if exists odoo12_sale_order_line_invoice_rel;


create FOREIGN TABLE odoo12_sale_order_line_invoice_rel("invoice_line_id" int4 NOT NULL
  ,"order_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_line_invoice_rel');



drop foreign table if exists odoo12_sale_order_option;


create FOREIGN TABLE odoo12_sale_order_option("id" int4 NOT NULL
  ,"order_id" int4
  ,"line_id" int4
  ,"name" text NOT NULL
  ,"product_id" int4 NOT NULL
  ,"price_unit" numeric NOT NULL
  ,"discount" numeric
  ,"uom_id" int4 NOT NULL
  ,"quantity" numeric NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_option');



drop foreign table if exists odoo12_sale_order_tag_rel;


create FOREIGN TABLE odoo12_sale_order_tag_rel("order_id" int4 NOT NULL
  ,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_tag_rel');



drop foreign table if exists odoo12_sale_order_template;


create FOREIGN TABLE odoo12_sale_order_template("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"note" text
  ,"number_of_days" int4
  ,"require_signature" bool
  ,"require_payment" bool
  ,"mail_template_id" int4
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template');



drop foreign table if exists odoo12_sale_order_template_line;


create FOREIGN TABLE odoo12_sale_order_template_line("id" int4 NOT NULL
  ,"sequence" int4
  ,"sale_order_template_id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"product_id" int4
  ,"price_unit" numeric NOT NULL
  ,"discount" numeric
  ,"product_uom_qty" numeric NOT NULL
  ,"product_uom_id" int4
  ,"display_type" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template_line');



drop foreign table if exists odoo12_sale_order_template_option;


create FOREIGN TABLE odoo12_sale_order_template_option("id" int4 NOT NULL
  ,"sale_order_template_id" int4 NOT NULL
  ,"name" text NOT NULL
  ,"product_id" int4 NOT NULL
  ,"price_unit" numeric NOT NULL
  ,"discount" numeric
  ,"uom_id" int4 NOT NULL
  ,"quantity" numeric NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template_option');



drop foreign table if exists odoo12_sale_order_transaction_rel;


create FOREIGN TABLE odoo12_sale_order_transaction_rel("sale_order_id" int4 NOT NULL
  ,"transaction_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_transaction_rel');



drop foreign table if exists odoo12_sale_payment_acquirer_onboarding_wizard;


create FOREIGN TABLE odoo12_sale_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
  ,"paypal_email_account" varchar
  ,"paypal_seller_account" varchar
  ,"paypal_pdt_token" varchar
  ,"stripe_secret_key" varchar
  ,"stripe_publishable_key" varchar
  ,"manual_name" varchar
  ,"journal_name" varchar
  ,"acc_number" varchar
  ,"manual_post_msg" text
  ,"payment_method" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_payment_acquirer_onboarding_wizard');



drop foreign table if exists odoo12_sale_product_configurator;


create FOREIGN TABLE odoo12_sale_product_configurator("id" int4 NOT NULL
  ,"product_template_id" int4 NOT NULL
  ,"pricelist_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_product_configurator');



drop foreign table if exists odoo12_sale_report;


create FOREIGN TABLE odoo12_sale_report("id" int4
  ,"product_id" int4
  ,"product_uom" int4
  ,"product_uom_qty" numeric
  ,"qty_delivered" numeric
  ,"qty_invoiced" numeric
  ,"qty_to_invoice" numeric
  ,"price_total" numeric
  ,"price_subtotal" numeric
  ,"untaxed_amount_to_invoice" numeric
  ,"untaxed_amount_invoiced" numeric
  ,"nbr" int8
  ,"name" varchar
  ,"date" timestamp
  ,"confirmation_date" timestamp
  ,"state" varchar
  ,"partner_id" int4
  ,"user_id" int4
  ,"company_id" int4
  ,"delay" float8
  ,"categ_id" int4
  ,"pricelist_id" int4
  ,"analytic_account_id" int4
  ,"team_id" int4
  ,"product_tmpl_id" int4
  ,"country_id" int4
  ,"commercial_partner_id" int4
  ,"weight" numeric
  ,"volume" float8
  ,"discount" numeric
  ,"discount_amount" numeric
  ,"order_id" int4
  ,"margin" numeric
  ,"warehouse_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_report');



drop foreign table if exists odoo12_share_line;


create FOREIGN TABLE odoo12_share_line("id" int4 NOT NULL
  ,"share_product_id" int4 NOT NULL
  ,"share_number" int4 NOT NULL
  ,"share_unit_price" numeric
  ,"effective_date" date
  ,"partner_id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'share_line');



drop foreign table if exists odoo12_share_line_update_info;


create FOREIGN TABLE odoo12_share_line_update_info("id" int4 NOT NULL
  ,"effective_date" date NOT NULL
  ,"share_line" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'share_line_update_info');



drop foreign table if exists odoo12_sms_send_sms;


create FOREIGN TABLE odoo12_sms_send_sms("id" int4 NOT NULL
  ,"recipients" varchar NOT NULL
  ,"message" text NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sms_send_sms');



drop foreign table if exists odoo12_snailmail_letter;


create FOREIGN TABLE odoo12_snailmail_letter("id" int4 NOT NULL
  ,"user_id" int4
  ,"model" varchar NOT NULL
  ,"res_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"report_template" int4
  ,"attachment_id" int4
  ,"color" bool
  ,"duplex" bool
  ,"state" varchar
  ,"info_msg" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'snailmail_letter');



drop foreign table if exists odoo12_stock_backorder_confirmation;


create FOREIGN TABLE odoo12_stock_backorder_confirmation("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_backorder_confirmation');



drop foreign table if exists odoo12_stock_change_product_qty;


create FOREIGN TABLE odoo12_stock_change_product_qty("id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"product_tmpl_id" int4 NOT NULL
  ,"new_quantity" numeric NOT NULL
  ,"lot_id" int4
  ,"location_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_change_product_qty');



drop foreign table if exists odoo12_stock_change_standard_price;


create FOREIGN TABLE odoo12_stock_change_standard_price("id" int4 NOT NULL
  ,"new_price" numeric NOT NULL
  ,"counterpart_account_id" int4
  ,"counterpart_account_id_required" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_change_standard_price');



drop foreign table if exists odoo12_stock_fixed_putaway_strat;


create FOREIGN TABLE odoo12_stock_fixed_putaway_strat("id" int4 NOT NULL
  ,"putaway_id" int4 NOT NULL
  ,"category_id" int4
  ,"fixed_location_id" int4 NOT NULL
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"product_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_fixed_putaway_strat');



drop foreign table if exists odoo12_stock_immediate_transfer;


create FOREIGN TABLE odoo12_stock_immediate_transfer("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_immediate_transfer');



drop foreign table if exists odoo12_stock_inventory;
create FOREIGN TABLE odoo12_stock_inventory("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"date" timestamp NOT NULL
  ,"state" varchar
  ,"company_id" int4 NOT NULL
  ,"location_id" int4 NOT NULL
  ,"product_id" int4
  ,"package_id" int4
  ,"partner_id" int4
  ,"lot_id" int4
  ,"filter" varchar NOT NULL
  ,"category_id" int4
  ,"exhausted" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"accounting_date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_inventory');



drop foreign table if exists odoo12_stock_inventory_line;


create FOREIGN TABLE odoo12_stock_inventory_line("id" int4 NOT NULL
  ,"inventory_id" int4
  ,"partner_id" int4
  ,"product_id" int4 NOT NULL
  ,"product_name" varchar
  ,"product_code" varchar
  ,"product_uom_id" int4 NOT NULL
  ,"product_qty" numeric
  ,"location_id" int4 NOT NULL
  ,"location_name" varchar
  ,"package_id" int4
  ,"prod_lot_id" int4
  ,"prodlot_name" varchar
  ,"company_id" int4
  ,"theoretical_qty" numeric
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_inventory_line');



drop foreign table if exists odoo12_stock_location;


create FOREIGN TABLE odoo12_stock_location("id" int4 NOT NULL
  ,"parent_left" int4
  ,"parent_right" int4
  ,"name" varchar NOT NULL
  ,"complete_name" varchar
  ,"active" bool
  ,"usage" varchar NOT NULL
  ,"location_id" int4
  ,"partner_id" int4
  ,"comment" text
  ,"posx" int4
  ,"posy" int4
  ,"posz" int4
  ,"company_id" int4
  ,"scrap_location" bool
  ,"return_location" bool
  ,"removal_strategy_id" int4
  ,"putaway_strategy_id" int4
  ,"barcode" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"valuation_in_account_id" int4
  ,"valuation_out_account_id" int4
  ,"parent_path" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location');



drop foreign table if exists odoo12_stock_location_path;


create FOREIGN TABLE odoo12_stock_location_path("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"company_id" int4
  ,"route_id" int4 NOT NULL
  ,"location_from_id" int4 NOT NULL
  ,"location_dest_id" int4 NOT NULL
  ,"delay" int4
  ,"picking_type_id" int4 NOT NULL
  ,"auto" varchar NOT NULL
  ,"propagate" bool
  ,"active" bool
  ,"warehouse_id" int4
  ,"route_sequence" int4
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_path');



drop foreign table if exists odoo12_stock_location_route;


create FOREIGN TABLE odoo12_stock_location_route("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"sequence" int4
  ,"product_selectable" bool
  ,"product_categ_selectable" bool
  ,"warehouse_selectable" bool
  ,"supplied_wh_id" int4
  ,"supplier_wh_id" int4
  ,"company_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"sale_selectable" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route');



drop foreign table if exists odoo12_stock_location_route_categ;


create FOREIGN TABLE odoo12_stock_location_route_categ("route_id" int4 NOT NULL
  ,"categ_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_categ');



drop foreign table if exists odoo12_stock_location_route_move;


create FOREIGN TABLE odoo12_stock_location_route_move("move_id" int4 NOT NULL
  ,"route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_move');



drop foreign table if exists odoo12_stock_location_route_stock_rules_report_rel;


create FOREIGN TABLE odoo12_stock_location_route_stock_rules_report_rel("stock_rules_report_id" int4 NOT NULL
  ,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_stock_rules_report_rel');



drop foreign table if exists odoo12_stock_move;


create FOREIGN TABLE odoo12_stock_move("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"priority" varchar
  ,"create_date" timestamp
  ,"date" timestamp NOT NULL
  ,"company_id" int4 NOT NULL
  ,"date_expected" timestamp NOT NULL
  ,"product_id" int4 NOT NULL
  ,"ordered_qty" numeric
  ,"product_qty" numeric
  ,"product_uom_qty" numeric NOT NULL
  ,"product_uom" int4 NOT NULL
  ,"product_packaging" int4
  ,"location_id" int4 NOT NULL
  ,"location_dest_id" int4 NOT NULL
  ,"partner_id" int4
  ,"picking_id" int4
  ,"note" text
  ,"state" varchar
  ,"price_unit" float8
  ,"origin" varchar
  ,"procure_method" varchar NOT NULL
  ,"scrapped" bool
  ,"group_id" int4
  ,"rule_id" int4
  ,"openupgrade_legacy_12_0_push_rule_id" int4
  ,"propagate" bool
  ,"picking_type_id" int4
  ,"inventory_id" int4
  ,"origin_returned_move_id" int4
  ,"restrict_partner_id" int4
  ,"warehouse_id" int4
  ,"additional" bool
  ,"reference" varchar
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"to_refund" bool
  ,"value" float8
  ,"remaining_qty" float8
  ,"remaining_value" float8
  ,"purchase_line_id" int4
  ,"created_purchase_line_id" int4
  ,"sale_line_id" int4
  ,"package_level_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move');



drop foreign table if exists odoo12_stock_move_line;
create FOREIGN TABLE odoo12_stock_move_line("id" int4 NOT NULL
  ,"picking_id" int4
  ,"move_id" int4
  ,"product_id" int4
  ,"product_uom_id" int4 NOT NULL
  ,"product_qty" numeric
  ,"product_uom_qty" numeric NOT NULL
  ,"ordered_qty" numeric
  ,"qty_done" numeric
  ,"package_id" int4
  ,"lot_id" int4
  ,"lot_name" varchar
  ,"result_package_id" int4
  ,"date" timestamp NOT NULL
  ,"owner_id" int4
  ,"location_id" int4 NOT NULL
  ,"location_dest_id" int4 NOT NULL
  ,"state" varchar
  ,"reference" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"package_level_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_line');



drop foreign table if exists odoo12_stock_move_line_consume_rel;


create FOREIGN TABLE odoo12_stock_move_line_consume_rel("consume_line_id" int4 NOT NULL
  ,"produce_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_line_consume_rel');



drop foreign table if exists odoo12_stock_move_move_rel;


create FOREIGN TABLE odoo12_stock_move_move_rel("move_orig_id" int4 NOT NULL
  ,"move_dest_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_move_rel');



drop foreign table if exists odoo12_stock_overprocessed_transfer;


create FOREIGN TABLE odoo12_stock_overprocessed_transfer("id" int4 NOT NULL
  ,"picking_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_overprocessed_transfer');



drop foreign table if exists odoo12_stock_package_destination;


create FOREIGN TABLE odoo12_stock_package_destination("id" int4 NOT NULL
  ,"picking_id" int4 NOT NULL
  ,"location_dest_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_package_destination');



drop foreign table if exists odoo12_stock_package_level;


create FOREIGN TABLE odoo12_stock_package_level("id" int4 NOT NULL
  ,"package_id" int4 NOT NULL
  ,"picking_id" int4
  ,"location_dest_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_package_level');



drop foreign table if exists odoo12_stock_picking;


create FOREIGN TABLE odoo12_stock_picking("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"openupgrade_legacy_12_0_activity_date_deadline" date
  ,"name" varchar
  ,"origin" varchar
  ,"note" text
  ,"backorder_id" int4
  ,"move_type" varchar NOT NULL
  ,"state" varchar
  ,"group_id" int4
  ,"priority" varchar
  ,"scheduled_date" timestamp
  ,"date" timestamp
  ,"date_done" timestamp
  ,"location_id" int4 NOT NULL
  ,"location_dest_id" int4 NOT NULL
  ,"picking_type_id" int4 NOT NULL
  ,"partner_id" int4
  ,"company_id" int4 NOT NULL
  ,"owner_id" int4
  ,"printed" bool
  ,"is_locked" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"sale_id" int4
  ,"message_main_attachment_id" int4
  ,"immediate_transfer" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking');



drop foreign table if exists odoo12_stock_picking_backorder_rel;


create FOREIGN TABLE odoo12_stock_picking_backorder_rel("stock_backorder_confirmation_id" int4 NOT NULL
  ,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_backorder_rel');



drop foreign table if exists odoo12_stock_picking_transfer_rel;


create FOREIGN TABLE odoo12_stock_picking_transfer_rel("stock_immediate_transfer_id" int4 NOT NULL
  ,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_transfer_rel');



drop foreign table if exists odoo12_stock_picking_type;


create FOREIGN TABLE odoo12_stock_picking_type("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"color" int4
  ,"sequence" int4
  ,"sequence_id" int4 NOT NULL
  ,"default_location_src_id" int4
  ,"default_location_dest_id" int4
  ,"code" varchar NOT NULL
  ,"return_picking_type_id" int4
  ,"show_entire_packs" bool
  ,"warehouse_id" int4
  ,"active" bool
  ,"use_create_lots" bool
  ,"use_existing_lots" bool
  ,"show_operations" bool
  ,"show_reserved" bool
  ,"barcode_nomenclature_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"barcode" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_type');



drop foreign table if exists odoo12_stock_production_lot;


create FOREIGN TABLE odoo12_stock_production_lot("id" int4 NOT NULL
  ,"message_last_post" timestamp
  ,"name" varchar NOT NULL
  ,"ref" varchar
  ,"product_id" int4 NOT NULL
  ,"product_uom_id" int4
  ,"create_date" timestamp
  ,"create_uid" int4
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_production_lot');



drop foreign table if exists odoo12_stock_quant;


create FOREIGN TABLE odoo12_stock_quant("id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"company_id" int4
  ,"location_id" int4 NOT NULL
  ,"lot_id" int4
  ,"package_id" int4
  ,"owner_id" int4
  ,"quantity" float8 NOT NULL
  ,"reserved_quantity" float8 NOT NULL
  ,"in_date" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quant');



drop foreign table if exists odoo12_stock_quant_package;


create FOREIGN TABLE odoo12_stock_quant_package("id" int4 NOT NULL
  ,"name" varchar
  ,"packaging_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"location_id" int4
  ,"company_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quant_package');



drop foreign table if exists odoo12_stock_quantity_history;


create FOREIGN TABLE odoo12_stock_quantity_history("id" int4 NOT NULL
  ,"compute_at_date" int4
  ,"date" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quantity_history');



drop foreign table if exists odoo12_stock_return_picking;


create FOREIGN TABLE odoo12_stock_return_picking("id" int4 NOT NULL
  ,"picking_id" int4
  ,"move_dest_exists" bool
  ,"original_location_id" int4
  ,"parent_location_id" int4
  ,"location_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_return_picking');



drop foreign table if exists odoo12_stock_return_picking_line;


create FOREIGN TABLE odoo12_stock_return_picking_line("id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"quantity" numeric NOT NULL
  ,"wizard_id" int4
  ,"move_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"to_refund" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_return_picking_line');



drop foreign table if exists odoo12_stock_route_product;


create FOREIGN TABLE odoo12_stock_route_product("route_id" int4 NOT NULL
  ,"product_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_route_product');



drop foreign table if exists odoo12_stock_route_warehouse;


create FOREIGN TABLE odoo12_stock_route_warehouse("route_id" int4 NOT NULL
  ,"warehouse_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_route_warehouse');



drop foreign table if exists odoo12_stock_rule;


create FOREIGN TABLE odoo12_stock_rule("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"group_propagation_option" varchar
  ,"group_id" int4
  ,"action" varchar NOT NULL
  ,"sequence" int4
  ,"company_id" int4
  ,"location_id" int4 NOT NULL
  ,"location_src_id" int4
  ,"route_id" int4 NOT NULL
  ,"procure_method" varchar NOT NULL
  ,"route_sequence" int4
  ,"picking_type_id" int4 NOT NULL
  ,"delay" int4
  ,"partner_address_id" int4
  ,"propagate" bool
  ,"warehouse_id" int4
  ,"propagate_warehouse_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"openupgrade_legacy_12_0_action" varchar
  ,"openupgrade_legacy_12_0_loc_path_id" int4
  ,"auto" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rule');



drop foreign table if exists odoo12_stock_rules_report;


create FOREIGN TABLE odoo12_stock_rules_report("id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"product_tmpl_id" int4 NOT NULL
  ,"product_has_variants" bool NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rules_report');



drop foreign table if exists odoo12_stock_rules_report_stock_warehouse_rel;


create FOREIGN TABLE odoo12_stock_rules_report_stock_warehouse_rel("stock_rules_report_id" int4 NOT NULL
  ,"stock_warehouse_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rules_report_stock_warehouse_rel');



drop foreign table if exists odoo12_stock_scheduler_compute;


create FOREIGN TABLE odoo12_stock_scheduler_compute("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_scheduler_compute');



drop foreign table if exists odoo12_stock_scrap;


create FOREIGN TABLE odoo12_stock_scrap("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"origin" varchar
  ,"product_id" int4 NOT NULL
  ,"product_uom_id" int4 NOT NULL
  ,"lot_id" int4
  ,"package_id" int4
  ,"owner_id" int4
  ,"move_id" int4
  ,"picking_id" int4
  ,"location_id" int4 NOT NULL
  ,"scrap_location_id" int4 NOT NULL
  ,"scrap_qty" numeric NOT NULL
  ,"state" varchar
  ,"date_expected" timestamp
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_scrap');



drop foreign table if exists odoo12_stock_traceability_report;


create FOREIGN TABLE odoo12_stock_traceability_report("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_traceability_report');



drop foreign table if exists odoo12_stock_track_confirmation;


create FOREIGN TABLE odoo12_stock_track_confirmation("id" int4 NOT NULL
  ,"inventory_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_track_confirmation');



drop foreign table if exists odoo12_stock_track_line;


create FOREIGN TABLE odoo12_stock_track_line("id" int4 NOT NULL
  ,"product_id" int4
  ,"tracking" varchar
  ,"wizard_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_track_line');



drop foreign table if exists odoo12_stock_warehouse;
create FOREIGN TABLE odoo12_stock_warehouse("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"company_id" int4 NOT NULL
  ,"partner_id" int4
  ,"view_location_id" int4 NOT NULL
  ,"lot_stock_id" int4 NOT NULL
  ,"code" varchar(5) NOT NULL
  ,"reception_steps" varchar NOT NULL
  ,"delivery_steps" varchar NOT NULL
  ,"wh_input_stock_loc_id" int4
  ,"wh_qc_stock_loc_id" int4
  ,"wh_output_stock_loc_id" int4
  ,"wh_pack_stock_loc_id" int4
  ,"mto_pull_id" int4
  ,"pick_type_id" int4
  ,"pack_type_id" int4
  ,"out_type_id" int4
  ,"in_type_id" int4
  ,"int_type_id" int4
  ,"crossdock_route_id" int4
  ,"reception_route_id" int4
  ,"delivery_route_id" int4
  ,"default_resupply_wh_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"buy_to_resupply" bool
  ,"buy_pull_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warehouse');



drop foreign table if exists odoo12_stock_warehouse_orderpoint;


create FOREIGN TABLE odoo12_stock_warehouse_orderpoint("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"warehouse_id" int4 NOT NULL
  ,"location_id" int4 NOT NULL
  ,"product_id" int4 NOT NULL
  ,"product_min_qty" numeric NOT NULL
  ,"product_max_qty" numeric NOT NULL
  ,"qty_multiple" numeric NOT NULL
  ,"group_id" int4
  ,"company_id" int4 NOT NULL
  ,"lead_days" int4
  ,"lead_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warehouse_orderpoint');



drop foreign table if exists odoo12_stock_warn_insufficient_qty_scrap;


create FOREIGN TABLE odoo12_stock_warn_insufficient_qty_scrap("id" int4 NOT NULL
  ,"scrap_id" int4
  ,"product_id" int4 NOT NULL
  ,"location_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warn_insufficient_qty_scrap');



drop foreign table if exists odoo12_stock_wh_resupply_table;


create FOREIGN TABLE odoo12_stock_wh_resupply_table("supplied_wh_id" int4 NOT NULL
  ,"supplier_wh_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_wh_resupply_table');



drop foreign table if exists odoo12_subscription_register;


create FOREIGN TABLE odoo12_subscription_register("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"register_number_operation" int4 NOT NULL
  ,"partner_id" int4 NOT NULL
  ,"partner_id_to" int4
  ,"date" date NOT NULL
  ,"quantity" int4
  ,"share_unit_price" numeric
  ,"share_product_id" int4 NOT NULL
  ,"share_to_product_id" int4
  ,"quantity_to" int4
  ,"share_to_unit_price" numeric
  ,"type" varchar
  ,"company_id" int4 NOT NULL
  ,"user_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'subscription_register');



drop foreign table if exists odoo12_subscription_request;


create FOREIGN TABLE odoo12_subscription_request("id" int4 NOT NULL
  ,"already_cooperator" bool
  ,"name" varchar NOT NULL
  ,"firstname" varchar
  ,"lastname" varchar
  ,"birthdate" date
  ,"gender" varchar
  ,"type" varchar
  ,"state" varchar NOT NULL
  ,"email" varchar NOT NULL
  ,"iban" varchar
  ,"partner_id" int4
  ,"share_product_id" int4 NOT NULL
  ,"ordered_parts" int4 NOT NULL
  ,"address" varchar NOT NULL
  ,"city" varchar NOT NULL
  ,"zip_code" varchar NOT NULL
  ,"country_id" int4 NOT NULL
  ,"phone" varchar
  ,"user_id" int4
  ,"skip_control_ng" bool
  ,"lang" varchar NOT NULL
  ,"date" date NOT NULL
  ,"company_id" int4 NOT NULL
  ,"is_company" bool
  ,"is_operation" bool
  ,"company_name" varchar
  ,"company_email" varchar
  ,"company_register_number" varchar
  ,"company_type" varchar
  ,"same_address" bool
  ,"activities_address" varchar
  ,"activities_city" varchar
  ,"activities_zip_code" varchar
  ,"activities_country_id" int4
  ,"contact_person_function" varchar
  ,"operation_request_id" int4
  ,"capital_release_request_date" date
  ,"source" varchar
  ,"data_policy_approved" bool
  ,"internal_rules_approved" bool
  ,"financial_risk_approved" bool
  ,"generic_rules_approved" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"vat" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'subscription_request');



drop foreign table if exists odoo12_summary_dept_rel;


create FOREIGN TABLE odoo12_summary_dept_rel("sum_id" int4 NOT NULL
  ,"dept_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'summary_dept_rel');



drop foreign table if exists odoo12_summary_emp_rel;
create FOREIGN TABLE odoo12_summary_emp_rel("sum_id" int4 NOT NULL
  ,"emp_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'summary_emp_rel');



drop foreign table if exists odoo12_survey_label;


create FOREIGN TABLE odoo12_survey_label("id" int4 NOT NULL
  ,"question_id" int4
  ,"question_id_2" int4
  ,"sequence" int4
  ,"value" varchar NOT NULL
  ,"quizz_mark" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_label');



drop foreign table if exists odoo12_survey_mail_compose_message;


create FOREIGN TABLE odoo12_survey_mail_compose_message("id" int4 NOT NULL
  ,"composition_mode" varchar
  ,"use_active_domain" bool
  ,"active_domain" text
  ,"is_log" bool
  ,"subject" varchar
  ,"notify" bool
  ,"auto_delete" bool
  ,"auto_delete_message" bool
  ,"template_id" int4
  ,"message_type" varchar NOT NULL
  ,"subtype_id" int4
  ,"mass_mailing_campaign_id" int4
  ,"mass_mailing_id" int4
  ,"mass_mailing_name" varchar
  ,"date" timestamp
  ,"body" text
  ,"parent_id" int4
  ,"model" varchar
  ,"res_id" int4
  ,"record_name" varchar
  ,"mail_activity_type_id" int4
  ,"email_from" varchar
  ,"author_id" int4
  ,"no_auto_thread" bool
  ,"message_id" varchar
  ,"reply_to" varchar
  ,"mail_server_id" int4
  ,"moderation_status" varchar
  ,"moderator_id" int4
  ,"layout" varchar
  ,"add_sign" bool
  ,"email_cc" varchar
  ,"email_to" varchar
  ,"mail_tracking_needs_action" bool
  ,"survey_id" int4 NOT NULL
  ,"public" varchar NOT NULL
  ,"multi_email" text
  ,"date_deadline" date
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"website_published" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_mail_compose_message');



drop foreign table if exists odoo12_survey_mail_compose_message_ir_attachments_rel;


create FOREIGN TABLE odoo12_survey_mail_compose_message_ir_attachments_rel("wizard_id" int4 NOT NULL
  ,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_mail_compose_message_ir_attachments_rel');



drop foreign table if exists odoo12_survey_mail_compose_message_res_partner_rel;


create FOREIGN TABLE odoo12_survey_mail_compose_message_res_partner_rel("wizard_id" int4 NOT NULL
  ,"partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_mail_compose_message_res_partner_rel');



drop foreign table if exists odoo12_survey_page;


create FOREIGN TABLE odoo12_survey_page("id" int4 NOT NULL
  ,"title" varchar NOT NULL
  ,"survey_id" int4 NOT NULL
  ,"sequence" int4
  ,"description" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_page');



drop foreign table if exists odoo12_survey_question;
create FOREIGN TABLE odoo12_survey_question("id" int4 NOT NULL
  ,"page_id" int4 NOT NULL
  ,"sequence" int4
  ,"question" varchar NOT NULL
  ,"description" text
  ,"type" varchar NOT NULL
  ,"matrix_subtype" varchar
  ,"column_nb" varchar
  ,"display_mode" varchar
  ,"comments_allowed" bool
  ,"comments_message" varchar
  ,"comment_count_as_answer" bool
  ,"validation_required" bool
  ,"validation_email" bool
  ,"validation_length_min" int4
  ,"validation_length_max" int4
  ,"validation_min_float_value" float8
  ,"validation_max_float_value" float8
  ,"validation_min_date" date
  ,"validation_max_date" date
  ,"validation_error_msg" varchar
  ,"constr_mandatory" bool
  ,"constr_error_msg" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_question');



drop foreign table if exists odoo12_survey_stage;


create FOREIGN TABLE odoo12_survey_stage("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"sequence" int4
  ,"closed" bool
  ,"fold" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_stage');



drop foreign table if exists odoo12_survey_survey;


create FOREIGN TABLE odoo12_survey_survey("id" int4 NOT NULL
  ,"message_main_attachment_id" int4
  ,"title" varchar NOT NULL
  ,"stage_id" int4
  ,"auth_required" bool
  ,"users_can_go_back" bool
  ,"description" text
  ,"color" int4
  ,"email_template_id" int4
  ,"thank_you_message" text
  ,"quizz_mode" bool
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_survey');



drop foreign table if exists odoo12_survey_user_input;


create FOREIGN TABLE odoo12_survey_user_input("id" int4 NOT NULL
  ,"survey_id" int4 NOT NULL
  ,"date_create" timestamp NOT NULL
  ,"deadline" timestamp
  ,"type" varchar NOT NULL
  ,"state" varchar
  ,"test_entry" bool
  ,"token" varchar NOT NULL
  ,"partner_id" int4
  ,"email" varchar
  ,"last_displayed_page_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_user_input');



drop foreign table if exists odoo12_survey_user_input_line;


create FOREIGN TABLE odoo12_survey_user_input_line("id" int4 NOT NULL
  ,"user_input_id" int4 NOT NULL
  ,"question_id" int4 NOT NULL
  ,"survey_id" int4
  ,"date_create" timestamp NOT NULL
  ,"skipped" bool
  ,"answer_type" varchar
  ,"value_text" varchar
  ,"value_number" float8
  ,"value_date" date
  ,"value_free_text" text
  ,"value_suggested" int4
  ,"value_suggested_row" int4
  ,"quizz_mark" float8
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'survey_user_input_line');



drop foreign table if exists odoo12_tax_adjustments_wizard;


create FOREIGN TABLE odoo12_tax_adjustments_wizard("id" int4 NOT NULL
  ,"reason" varchar NOT NULL
  ,"journal_id" int4 NOT NULL
  ,"date" date NOT NULL
  ,"debit_account_id" int4 NOT NULL
  ,"credit_account_id" int4 NOT NULL
  ,"amount" numeric NOT NULL
  ,"company_currency_id" int4
  ,"tax_id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'tax_adjustments_wizard');



drop foreign table if exists odoo12_team_favorite_user_rel;


create FOREIGN TABLE odoo12_team_favorite_user_rel("team_id" int4 NOT NULL
  ,"user_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'team_favorite_user_rel');



drop foreign table if exists odoo12_theme_ir_attachment;


create FOREIGN TABLE odoo12_theme_ir_attachment("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"key" varchar NOT NULL
  ,"url" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'theme_ir_attachment');



drop foreign table if exists odoo12_theme_ir_ui_view;


create FOREIGN TABLE odoo12_theme_ir_ui_view("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"key" varchar
  ,"type" varchar
  ,"priority" int4 NOT NULL
  ,"mode" varchar
  ,"active" bool
  ,"arch" text
  ,"arch_fs" varchar
  ,"inherit_id" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'theme_ir_ui_view');



drop foreign table if exists odoo12_theme_website_menu;


create FOREIGN TABLE odoo12_theme_website_menu("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"url" varchar
  ,"page_id" int4
  ,"new_window" bool
  ,"sequence" int4
  ,"parent_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'theme_website_menu');



drop foreign table if exists odoo12_theme_website_page;


create FOREIGN TABLE odoo12_theme_website_page("id" int4 NOT NULL
  ,"url" varchar
  ,"view_id" int4 NOT NULL
  ,"website_indexed" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'theme_website_page');



drop foreign table if exists odoo12_trial_balance_report_wizard;


create FOREIGN TABLE odoo12_trial_balance_report_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"target_move" varchar NOT NULL
  ,"hide_account_balance_at_0" bool
  ,"receivable_accounts_only" bool
  ,"payable_accounts_only" bool
  ,"show_partner_details" bool
  ,"not_only_one_unaffected_earnings_account" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"hierarchy_on" varchar NOT NULL
  ,"limit_hierarchy_level" bool
  ,"show_hierarchy_level" int4
  ,"hide_parent_hierarchy_level" bool
  ,"hide_account_at_0" bool
  ,"foreign_currency" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'trial_balance_report_wizard');



drop foreign table if exists odoo12_uom_category;


create FOREIGN TABLE odoo12_uom_category("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"measure_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'uom_category');



drop foreign table if exists odoo12_uom_uom;


create FOREIGN TABLE odoo12_uom_uom("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"category_id" int4 NOT NULL
  ,"factor" numeric NOT NULL
  ,"rounding" numeric NOT NULL
  ,"active" bool
  ,"uom_type" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"measure_type" varchar
  ,"timesheet_widget" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'uom_uom');



drop foreign table if exists odoo12_utm_campaign;


create FOREIGN TABLE odoo12_utm_campaign("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_campaign');



drop foreign table if exists odoo12_utm_medium;


create FOREIGN TABLE odoo12_utm_medium("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_medium');



drop foreign table if exists odoo12_utm_source;


create FOREIGN TABLE odoo12_utm_source("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_source');



drop foreign table if exists odoo12_validate_account_move;


create FOREIGN TABLE odoo12_validate_account_move("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'validate_account_move');



drop foreign table if exists odoo12_validate_subscription_request;


create FOREIGN TABLE odoo12_validate_subscription_request("id" int4 NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'validate_subscription_request');



drop foreign table if exists odoo12_vat_report_wizard;


create FOREIGN TABLE odoo12_vat_report_wizard("id" int4 NOT NULL
  ,"company_id" int4
  ,"date_range_id" int4
  ,"date_from" date NOT NULL
  ,"date_to" date NOT NULL
  ,"based_on" varchar NOT NULL
  ,"tax_detail" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'vat_report_wizard');



drop foreign table if exists odoo12_web_editor_converter_test;


create FOREIGN TABLE odoo12_web_editor_converter_test("id" int4 NOT NULL
  ,"char" varchar
  ,"integer" int4
  ,"float" float8
  ,"numeric" numeric
  ,"many2one" int4
  ,"binary" bytea
  ,"date" date
  ,"datetime" timestamp
  ,"selection" int4
  ,"selection_str" varchar
  ,"html" text
  ,"text" text
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_editor_converter_test');



drop foreign table if exists odoo12_web_editor_converter_test_sub;


create FOREIGN TABLE odoo12_web_editor_converter_test_sub("id" int4 NOT NULL
  ,"name" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_editor_converter_test_sub');



drop foreign table if exists odoo12_web_planner;


create FOREIGN TABLE odoo12_web_planner("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"menu_id" int4 NOT NULL
  ,"view_id" int4 NOT NULL
  ,"tooltip_planner" text
  ,"planner_application" varchar NOT NULL
  ,"active" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_planner');



drop foreign table if exists odoo12_web_tour_tour;


create FOREIGN TABLE odoo12_web_tour_tour("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"user_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_tour_tour');



drop foreign table if exists odoo12_website;


create FOREIGN TABLE odoo12_website("id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"domain" varchar
  ,"company_id" int4 NOT NULL
  ,"default_lang_id" int4 NOT NULL
  ,"default_lang_code" varchar
  ,"auto_redirect_lang" bool
  ,"social_twitter" varchar
  ,"social_facebook" varchar
  ,"social_github" varchar
  ,"social_linkedin" varchar
  ,"social_youtube" varchar
  ,"social_googleplus" varchar
  ,"social_instagram" varchar
  ,"google_analytics_key" varchar
  ,"google_management_client_id" varchar
  ,"google_management_client_secret" varchar
  ,"google_maps_api_key" varchar
  ,"user_id" int4 NOT NULL
  ,"cdn_activated" bool
  ,"cdn_url" varchar
  ,"cdn_filters" text
  ,"homepage_id" int4
  ,"favicon" bytea
  ,"theme_id" int4
  ,"specific_user_account" bool
  ,"auth_signup_uninvited" varchar
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"website_form_enable_metadata" bool
  ,"crm_default_team_id" int4
  ,"crm_default_user_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website');



drop foreign table if exists odoo12_website_country_group_rel;


create FOREIGN TABLE odoo12_website_country_group_rel("website_id" int4 NOT NULL
  ,"country_group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website_country_group_rel');



drop foreign table if exists odoo12_website_lang_rel;


create FOREIGN TABLE odoo12_website_lang_rel("website_id" int4 NOT NULL
  ,"lang_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website_lang_rel');



drop foreign table if exists odoo12_website_menu;


create FOREIGN TABLE odoo12_website_menu("id" int4 NOT NULL
  ,"parent_path" varchar
  ,"name" varchar NOT NULL
  ,"url" varchar
  ,"page_id" int4
  ,"new_window" bool
  ,"sequence" int4
  ,"website_id" int4
  ,"parent_id" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"theme_template_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website_menu');



drop foreign table if exists odoo12_website_page;


create FOREIGN TABLE odoo12_website_page("id" int4 NOT NULL
  ,"url" varchar
  ,"view_id" int4 NOT NULL
  ,"website_indexed" bool
  ,"date_publish" timestamp
  ,"header_overlay" bool
  ,"header_color" varchar
  ,"website_id" int4
  ,"is_published" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp
  ,"theme_template_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website_page');



drop foreign table if exists odoo12_website_redirect;


create FOREIGN TABLE odoo12_website_redirect("id" int4 NOT NULL
  ,"type" varchar NOT NULL
  ,"url_from" varchar NOT NULL
  ,"url_to" varchar NOT NULL
  ,"website_id" int4
  ,"active" bool
  ,"sequence" int4
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'website_redirect');



drop foreign table if exists odoo12_wizard_ir_model_menu_create;


create FOREIGN TABLE odoo12_wizard_ir_model_menu_create("id" int4 NOT NULL
  ,"menu_id" int4 NOT NULL
  ,"name" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_ir_model_menu_create');



drop foreign table if exists odoo12_wizard_multi_charts_accounts;


create FOREIGN TABLE odoo12_wizard_multi_charts_accounts("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"currency_id" int4 NOT NULL
  ,"only_one_chart_template" bool
  ,"chart_template_id" int4 NOT NULL
  ,"bank_account_code_prefix" varchar
  ,"cash_account_code_prefix" varchar
  ,"code_digits" int4 NOT NULL
  ,"sale_tax_id" int4
  ,"purchase_tax_id" int4
  ,"sale_tax_rate" float8
  ,"transfer_account_id" int4 NOT NULL
  ,"purchase_tax_rate" float8
  ,"complete_tax_set" bool
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_multi_charts_accounts');



drop foreign table if exists odoo12_wizard_open_tax_balances;


create FOREIGN TABLE odoo12_wizard_open_tax_balances("id" int4 NOT NULL
  ,"company_id" int4 NOT NULL
  ,"from_date" date NOT NULL
  ,"to_date" date NOT NULL
  ,"date_range_id" int4
  ,"target_move" varchar NOT NULL
  ,"create_uid" int4
  ,"create_date" timestamp
  ,"write_uid" int4
  ,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_open_tax_balances');

-- --------------------------------------------------------------------
DROP TABLE IF EXISTS data CASCADE;

create table data( data date, dies_mes smallint);
insert into data (data)
select  cast('2009-12-31' as date) + ((rn||' days')::INTERVAL)
from (
select row_number() over (order by (select 1)) as rn from odoo12_res_partner a, odoo12_res_partner b  limit 5000
) a;

 update data set dies_mes=
    DATE_PART('days',
        DATE_TRUNC('month', data.data)
        + '1 MONTH'::INTERVAL
        - '1 DAY'::INTERVAL
    );


-- Hores dedicades per grup analitic, data, treballador, etc.
CREATE OR REPLACE VIEW odoo12_project_tasks AS
  SELECT p.id,
    p.name,
    p.active,
    pt.name AS project_type,
    pt2.name AS task,
    pt2.date_start,
    pt2.date_end,
    ptt.name AS stage,
    a.name AS activity_name,
    a.date::timestamp without time zone,
    a.unit_amount AS hores,
    round(a.unit_amount * 60::double precision) AS minuts,
    e.name AS employee,
    aac.name AS account_name,
    rp.name AS partner_name
    ,aag."name" as group_name_lvl2, coalesce(aagp."name",aag."name")  as group_name_lvl1, aag.complete_name as group_complete_name
  FROM odoo12_project_project p
    LEFT JOIN odoo12_project_type pt ON pt.id = p.type_id
    LEFT JOIN odoo12_account_analytic_line a ON a.project_id = p.id
    LEFT JOIN odoo12_hr_employee e ON e.id = a.employee_id
    LEFT JOIN odoo12_project_task pt2 ON pt2.project_id = p.id AND pt2.id = a.task_id
    LEFT JOIN odoo12_project_task_type ptt ON pt2.stage_id = ptt.id
    LEFT JOIN odoo12_account_analytic_account aac ON p.analytic_account_id = aac.id
    LEFT JOIN odoo12_res_partner rp ON rp.id = aac.partner_id
    left join odoo12_account_analytic_group aag on aac.group_id =aag.id
    left join odoo12_account_analytic_group aagp on aag.parent_id=aagp.id
  WHERE 1 = 1;

drop FOREIGN table if exists hand_apuntes_contables ;
CREATE FOREIGN table  hand_apuntes_contables (
"Any" int,
MesInici int,
Mesfi int,
IngresDespesa varchar(200),
Concepte varchar(200),
GrupAnaliticLvl1 varchar(200),
GrupAnaliticLvl2 varchar(200),
Empresa varchar(200),
"import" numeric(18,2)
) SERVER csvfiles
OPTIONS ( filename '/home/coopdevs/csv/apuntes_contables.csv', format 'csv' , delimiter ';', header  'true');


drop view if exists v_apuntes_contables cascade;
-- Apunts comptables per grup analitic, data, empresa i jearquia de comptabilitat
create or replace view v_apuntes_contables as
  select
    aag."name" as group_name_lvl2, coalesce(aagp."name",aag."name")  as group_name_lvl1, aag.complete_name as group_complete_name
    , aaa.name as analyic_account_name,  aaa.create_date, rp."name"  as partner_name
    ,am.name as asiento_contable, am.ref, am.date::timestamp without time zone as date
    , rp.display_name as empresa
    ,aa.name , aa.code, aa.internal_type, aa.internal_group
    , aat."name"  as account_type
    , ag.name as anal_acc_group_name_lvl3, ag1."name"  as anal_acc_group_name_lvl2 , ag2.name  as anal_acc_group_name_lvl1
    ,aml.name as move_line_name, aml.quantity , aml.debit , aml.credit , aml.balance  --si balance < 0 es bo
    --, uu.name as uom_name
    ,am.date::timestamp-((date_part('year',am.date::timestamp)+1999)::varchar||' year')::interval as date_without_year
  from odoo12_account_move_line aml
    join odoo12_account_move am on aml.move_id =am.id
    left join odoo12_res_partner rp  on aml.partner_id =rp.id
    join odoo12_account_account aa on aml.account_id =aa.id
    join odoo12_account_account_type aat on aa.user_type_id =aat.id
    --left join uom_uom uu on aml.product_uom_id =uu.id
    join odoo12_account_group ag on aa.group_id =ag.id
    left join odoo12_account_group ag1 on ag.parent_id =ag1.id
    left join odoo12_account_group ag2 on ag1.parent_id =ag2.id
    left join odoo12_account_analytic_account aaa on aml.analytic_account_id =aaa.id
    left join odoo12_account_analytic_group aag on aaa.group_id =aag.id
    left join odoo12_account_analytic_group aagp on aag.parent_id=aagp.id
  union all
    select GrupAnaliticLvl2, GrupAnaliticLvl1, case when GrupAnaliticLvl1 is null then null else concat(GrupAnaliticLvl1,'/', GrupAnaliticLvl2) end, null, d.data, Empresa, null, null
        , d.data, Empresa, null, null, null, null, null, 'manual', Concepte, IngresDespesa, null, 1, 0, "import", -import
        ,d.data::timestamp-((date_part('year',d.data::timestamp)+1999)::varchar||' year')::interval as date_without_year
    from hand_apuntes_contables h
        join data d on date_trunc('month', d.data) between ("Any"*10000+MesInici*100+1)::varchar::date and ("Any"*10000+MesFi*100+1)::varchar::date
            and date_trunc('month', d.data)=d.data;

-- analytic line per empresa i treballador. Es veuen les hores i els imports assignats a odoo
drop view if exists v_analytic_line_group;
create or replace view v_analytic_line_group as
   select
   a.name as activity_name, a.date::timestamp without time zone as date
   ,case when um.name='Hour(s)' then  a.unit_amount else 0 end as hores
   ,case when um.name='Hour(s)' then  round(a.unit_amount*60) else 0 end  as minuts
   ,um.name as uom_name, a.amount
   , e.name as employee
   , aac."name" as account_name
   , rp."name" as partner_name
    ,aag."name" as group_name_lvl2, coalesce(aagp."name",aag."name")  as group_name_lvl1, aag.complete_name as group_complete_name
   from odoo12_account_analytic_line a
   	left join odoo12_hr_employee e on e.id=a.employee_id
   	left join odoo12_account_analytic_account aac on a.account_id =aac.id
   	left join odoo12_res_partner rp on rp.id=aac.partner_id
   	left join odoo12_account_analytic_group aag on aac.group_id =aag.id
   	left join odoo12_account_analytic_group aagp on aag.parent_id=aagp.id
   	left join odoo12_uom_uom um on a.product_uom_id = um.id;

-- comparativa entre hores i vendes per grup analitic
create or replace view v_group_balanc_hores as
    select coalesce(a.date,b.date)::timestamp without time zone as date, coalesce(a.partner_name,b.partner_name) as partner_name
    ,coalesce(a.group_name_lvl1,b.group_name_lvl1) as group_name_lvl1
    ,coalesce(a.group_name_lvl2,b.group_name_lvl2) as group_name_lvl2
    ,coalesce(a.group_complete_name,b.group_complete_name) as group_complete_name
    , hores, import
    from
    (
    select t.date, sum(t.hores) as hores, coalesce(t.partner_name, 'N/A') as partner_name
    , coalesce(t.group_name_lvl1,'N/A') as group_name_lvl1, coalesce(t.group_name_lvl2,'N/A') as group_name_lvl2
    , coalesce(t.group_complete_name,'N/A') as group_complete_name
    from odoo12_project_tasks t
    group by t.date,  t.partner_name, t.group_name_lvl1, t.group_name_lvl2, t.group_complete_name
    ) a
    full  join
    (
    select t.date, -sum(t.balance) as import, coalesce(t.empresa, 'N/A') as partner_name
    , coalesce(t.group_name_lvl1,'N/A') as group_name_lvl1, coalesce(t.group_name_lvl2,'N/A') as group_name_lvl2
    , coalesce(t.group_complete_name,'N/A') as group_complete_name
    from v_apuntes_contables t
    where  anal_acc_group_name_lvl1 IN ('Ventas e ingresos')
    group by t.date,  t.empresa, t.group_name_lvl1, t.group_name_lvl2, t.group_complete_name
    ) b on a.date=b.date and a.partner_name=b.partner_name and a.group_name_lvl1=b.group_name_lvl1
    	and a.group_name_lvl2=b.group_name_lvl2 and a.group_complete_name=b.group_complete_name;

-- hores per mes, grup analitic i account_name (per veure si es i+d)
drop view  v_hours_by_group_month cascade;
create or replace view v_hours_by_group_month as
SELECT date_trunc('month', date) as date, group_name_lvl2,group_name_lvl1, group_complete_name, account_name
    , partner_name
       ,sum(hores) AS hores
FROM odoo12_project_tasks
GROUP BY date_trunc('month', date), group_name_lvl2,group_name_lvl1, group_complete_name, account_name, partner_name;

-- hores totals per mes per poder calcular el ratio
create or replace view v_hours_by_month as
SELECT date_trunc('month', date) as date,
       sum(hores) AS hores
FROM odoo12_project_tasks
GROUP BY date_trunc('month', date);

-- ratio d'hores per mes, grup analitic i account name
drop view if exists  v_ratio_hours_by_month cascade;
create or replace view v_ratio_hours_by_month as
select m.date, mg.group_name_lvl2,group_name_lvl1, group_complete_name, account_name, partner_name, mg.hores/m.hores as ratio_hores
from v_hours_by_month m
	join v_hours_by_group_month mg on m.date=mg.date;

-- cost ponderat segons les hores dedicades al cost de personal
create or replace view v_cost_personal_mes as
 select a.date, r.group_name_lvl1, r.group_name_lvl2, r.group_complete_name, r.account_name, r.partner_name, a.balance*coalesce(ratio_hores,0) as cost_personal
 FROM v_apuntes_contables a
 	left join v_ratio_hours_by_month r on date_trunc('month',a.date)=r.date
 where a.anal_acc_group_name_lvl1='Compras y gastos' and a.anal_acc_group_name_lvl2='Gastos de personal';


-- Ventas e ingresos i Compras y gastos, separant el cost de personal segons el repartiment d'hores.
drop view if exists v_apuntes_contables_reviewed cascade;
create or replace view v_apuntes_contables_reviewed as
select
case
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Servicios de profesionales independientes'
		and partner_name ='Som Mobilitat SCCL' then 'Vertical Mobilitat'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa = '(Matomo) Paddle.com Market Ltd' then 'Business Intelligence'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa in ('Hetzner', 'Raintank Inc dba Grafana Labs')  then 'SysAdmin'
	else a.group_name_lvl2
 end as group_name_lvl2
,case
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Servicios de profesionales independientes'
		and partner_name ='Som Mobilitat SCCL' then 'Cooperatives de consumidores'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa = '(Matomo) Paddle.com Market Ltd' then 'Business Intelligence'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa in ('Hetzner', 'Raintank Inc dba Grafana Labs')  then 'SysAdmin'
	else a.group_name_lvl1
end as group_name_lvl1
,case
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Servicios de profesionales independientes'
		and partner_name ='Som Mobilitat SCCL' then 'Cooperatives de consumidores / Vertical Mobilitat'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa = '(Matomo) Paddle.com Market Ltd' then 'Business Intelligence'
	when anal_acc_group_name_lvl2='Servicios exteriores' and anal_acc_group_name_lvl1='Otros servicios'
		and empresa in ('Hetzner', 'Raintank Inc dba Grafana Labs')  then 'SysAdmin'
		else  a.group_complete_name
end as group_complete_name
 ,analyic_account_name,create_date,partner_name,asiento_contable,a.ref,a.date::timestamp without time zone as date,empresa,a.name,code::varchar(64),internal_type,internal_group,account_type,anal_acc_group_name_lvl1,anal_acc_group_name_lvl2,anal_acc_group_name_lvl3,move_line_name
,quantity
,debit
,credit
,balance
FROM v_apuntes_contables a
where (a.anal_acc_group_name_lvl1 in ('Ventas e ingresos', 'Compras y gastos') and a.anal_acc_group_name_lvl2<>'Gastos de personal')
union all
select
group_name_lvl2, group_name_lvl1, group_complete_name, account_name, date::timestamp without time zone as date, partner_name, 'N/A', 'N/A', date, 'N/A', 'N/A', 'N/A'
	, 'other', 'expense', 'Expenses', 'N/A', 'Gastos de personal', 'Compras y gastos', 'N/A', 1, cost_personal, 0, cost_personal
from v_cost_personal_mes;

-- ingressos, despeses i hores per grup analitic i account name (per a i+d), les hores de sou venen ponderades. (passat a taula)
drop  view if exists v_apuntes_contables_reviewed_hours;
create view v_apuntes_contables_reviewed_hours as
select group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date::timestamp without time zone as date, partner_name,sum(debit) as debit, sum(credit) as credit, sum(hores) as hores
from (
select group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date, partner_name, sum(debit) as debit, sum(credit) as credit, 0 as hores
from v_apuntes_contables_reviewed
group by group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name,date, partner_name
union all
select group_name_lvl2, group_name_lvl1, group_complete_name, account_name, date, partner_name, 0,0, sum(hores) as hores
from  odoo12_project_tasks
group by group_name_lvl2, group_name_lvl1, group_complete_name, account_name, date, partner_name
) a
group by group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date, partner_name;


drop  view  if exists v_apuntes_contables_year_acum cascade;
-- balanc d'intressos i despeses acumulats anualment
create or replace  view v_apuntes_contables_year_acum as
select group_name_lvl2,group_name_lvl1,group_complete_name,analyic_account_name,create_date,partner_name,asiento_contable,"ref",d.data::timestamp without time zone as data,empresa,"name",code,internal_type,internal_group,account_type,anal_acc_group_name_lvl1,anal_acc_group_name_lvl2,anal_acc_group_name_lvl3,move_line_name
,sum(quantity) as quantity,sum(debit) as debit, sum(credit) as credit,sum(balance) as balance
from data d
left join v_apuntes_contables a on date_trunc('year', d.data) =date_trunc('year', a.date) and d.data>=a.date
where (date_part('day', d.data)=d.dies_mes or d.data=current_date- interval '1 day' )
and d.data>='20200101'
group by group_name_lvl2,group_name_lvl1,group_complete_name,analyic_account_name,create_date,partner_name,asiento_contable,"ref",d.data,empresa,"name",code,internal_type,internal_group,account_type,anal_acc_group_name_lvl1,anal_acc_group_name_lvl2,anal_acc_group_name_lvl3,move_line_name;

-- balanc d'intressos i despeses mensuals
create or replace view v_apuntes_contables_agr as
select date
,anal_acc_group_name_lvl1 as cuenta_contable_lvl1
, anal_acc_group_name_lvl1||'-'|| anal_acc_group_name_lvl2 as cuenta_contable_lvl2
, anal_acc_group_name_lvl1||'-'|| anal_acc_group_name_lvl2||'-'|| anal_acc_group_name_lvl3  as cuenta_contable_lvl3
, anal_acc_group_name_lvl1||'-'|| anal_acc_group_name_lvl2||'-'|| anal_acc_group_name_lvl3||'-'|| code  as cuenta_contable_detalle
,sum(debit)as debit, sum(credit) as credit, sum(balance) as balance
from v_apuntes_contables d
where anal_acc_group_name_lvl1 in ('Compras y gastos','Ventas e ingresos')
group by  d.date, anal_acc_group_name_lvl3, anal_acc_group_name_lvl2, anal_acc_group_name_lvl1, code ;


drop view v_moves_not_bank;
create view v_moves_not_bank as
select am.name move_name, am.date move_date, am.amount as move_amount, am.move_type
,sum(aml.debit) as debit, sum(aml.credit) as credit, sum(aml.balance) as balance, aml.ref, aml.date_maturity, aml.date as move_line_date
, case when aa.code='465000' then null else rp.name end as partner_name
,aa.name as account_name, aa.code as account_code, aa.group_id
, aj."name" as journal_name, aj."type" as journal_type
, case when amount>0 then abs(sum(balance))/amount else 0 end as ratio
from odoo12_account_move am
join odoo12_account_move_line aml on am.id=aml.move_id
join odoo12_account_account aa on aml.account_id =aa.id
join odoo12_account_journal aj on am.journal_id =aj.id
left join odoo12_res_partner rp on case when aa.code='465000' then null else aml.partner_id end = rp.id
where 1=1
and aa.name not in ('Bank')
group by am.name, am.date, am.amount, am.move_type
, aml.ref, aml.date_maturity, aml.date,case when aa.code='465000' then null else rp.name end
,aa.name, aa.code, aa.group_id, aj.type
, aj."name" ;



drop FOREIGN table  hores_2t cascade;
CREATE FOREIGN table  hores_2t (
  "Comision" varchar(500), 
  "Actividades 2T 2022" varchar(500),
  "Cesar" int,
  "Dani P" int,
  "Dani Q" int,
  "Enrico" int,
  "Eugeni" int,
  "Jordi" int,
  "Kon" int,
  "Sergi" int,
  "Mar" int,
  "Lai" int
) SERVER csvfiles
OPTIONS ( filename '/home/coopdevs/csv/hores_2t.csv', format 'csv' , delimiter ';', header  'true');



drop view if exists v_hores2t;
create view v_hores2t as
select "Comision", "Actividades 2T 2022", 'Cesar' as persona, "Cesar" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Cesar" is not null
union all
select "Comision", "Actividades 2T 2022", 'Dani P', "Dani P" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Dani P" is not null
union all
select "Comision", "Actividades 2T 2022", 'Dani Q', "Dani Q" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Dani Q" is not null
union all
select "Comision", "Actividades 2T 2022", 'Enrico', "Enrico" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Enrico" is not null
union all
select "Comision", "Actividades 2T 2022", 'Eugeni', "Eugeni" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Eugeni" is not null
union all
select "Comision", "Actividades 2T 2022", 'Jordi', "Jordi" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Jordi" is not null
union all
select "Comision", "Actividades 2T 2022", 'Kon', "Kon" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Kon" is not null
union all
select "Comision", "Actividades 2T 2022", 'Sergi', "Sergi" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Sergi" is not null
union all
select "Comision", "Actividades 2T 2022", 'Mar', "Mar" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
from hores_2t
where  "Mar" is not null
union all
select "Comision", "Actividades 2T 2022", 'Lai', "Lai" as hores
	, case when "Comision" in ('SysAdmins','Comunicacio','RRHH','Comunidad','Seguimiento y debates','ODOO (I+D)','Gerencia') then 'Reproductiva'
		when  "Comision"='Vacances' then 'Vacances'
		else 'Productiva' end as grup_comisio
	from hores_2t
where  "Lai" is not null
;



drop view if exists v_fondo_maniobra_ratio_liquidez;
create view v_fondo_maniobra_ratio_liquidez as
select d.data, -sum(pasivo_corriente) as pasivo_corriente, sum(activo_corriente) as activo_corriente
, sum(activo_corriente)+sum(pasivo_corriente) as fondo_maniobra
,case when sum(pasivo_corriente)=0 then 1 else sum(activo_corriente)/sum(-pasivo_corriente) end as ratio_liquidez
from (
select aml.balance as pasivo_corriente, 0 as activo_corriente , aa.code , aml.date_maturity
from odoo12_account_move am
	join odoo12_account_move_line aml on am.id=aml.move_id
	join odoo12_account_account aa on aml.account_id =aa.id
where 1=1
and am.state not in('draft')
and aa.company_id =1
and (
aa.code like '585%' or
aa.code like '586%' or
aa.code like '587%' or
aa.code like '588%' or
aa.code like '589%' or
aa.code like '499%' or
aa.code like '529%' or
aa.code like '5105%' or
aa.code like '520%' or
aa.code like '527%' or
aa.code like '5125%' or
aa.code like '524%' or
aa.code like '1034%' or
aa.code like '1044%' or
aa.code like '190%' or
aa.code like '192%' or
aa.code like '194%' or
aa.code like '500%' or
aa.code like '501%' or
aa.code like '505%' or
aa.code like '506%' or
aa.code like '509%' or
aa.code like '5115%' or
aa.code like '5135%' or
aa.code like '5145%' or
aa.code like '521%' or
aa.code like '522%' or
aa.code like '523%' or
aa.code like '525%' or
aa.code like '526%' or
aa.code like '528%' or
aa.code like '5530%' or
aa.code like '5532%' or
aa.code like '555%' or
aa.code like '5565%' or
aa.code like '5566%' or
aa.code like '5595%' or
aa.code like '5598%' or
aa.code like '560%' or
aa.code like '561%' or
aa.code like '569%' or
aa.code like '551%' or
aa.code like '5525%' or
aa.code like '5103%' or
aa.code like '5104%' or
aa.code like '5113%' or
aa.code like '5114%' or
aa.code like '5123%' or
aa.code like '5124%' or
aa.code like '5133%' or
aa.code like '5134%' or
aa.code like '5143%' or
aa.code like '5144%' or
aa.code like '5563%' or
aa.code like '5564%' or
aa.code like '5523%' or
aa.code like '5524%' or
aa.code like '400%' or
aa.code like '401%' or
aa.code like '403%' or
aa.code like '404%' or
aa.code like '405%' or
aa.code like '406%' or
aa.code like '32581%' or
aa.code like '32582%' or
aa.code like '41%' or
aa.code like '438%' or
aa.code like '465%' or
aa.code like '466%' or
aa.code like '475%' or
aa.code like '476%' or
aa.code like '477%' or
aa.code like '485%' or
aa.code like '568%' or
aa.code like '502%' or
aa.code like '507%'
)
union all
select 0, aml.balance as activo_corriente, aa.code , aml.date_maturity
from odoo12_account_move am
	join odoo12_account_move_line aml on am.id=aml.move_id
	join odoo12_account_account aa on aml.account_id =aa.id
where 1=1
and am.state not in('draft')
and aa.company_id =1
and (aa.code like '580%' or
aa.code like '581%' or
aa.code like '582%' or
aa.code like '583%' or
aa.code like '584%' or
aa.code like '599%' or
aa.code like '30%' or
aa.code like '31%' or
aa.code like '32%' or
aa.code like '33%' or
aa.code like '34%' or
aa.code like '35%' or
aa.code like '36%' or
aa.code like '39%' or
aa.code like '407%' or
aa.code like '430%' or
aa.code like '431%' or
aa.code like '432%' or
aa.code like '433%' or
aa.code like '434%' or
aa.code like '435%' or
aa.code like '436%' or
aa.code like '437%' or
aa.code like '490%' or
aa.code like '493%' or
aa.code like '12381%' or
aa.code like '12382%' or
aa.code like '5580%' or
aa.code like '44%' or
aa.code like '460%' or
aa.code like '470%' or
aa.code like '471%' or
aa.code like '472%' or
aa.code like '473%' or
aa.code like '5531%' or
aa.code like '5533%' or
aa.code like '544%' or
aa.code like '5303%' or
aa.code like '5304%' or
aa.code like '5313%' or
aa.code like '5314%' or
aa.code like '5323%' or
aa.code like '5324%' or
aa.code like '5333%' or
aa.code like '5334%' or
aa.code like '5343%' or
aa.code like '5344%' or
aa.code like '5353%' or
aa.code like '5354%' or
aa.code like '5393%' or
aa.code like '5394%' or
aa.code like '593%' or
aa.code like '5943%' or
aa.code like '5944%' or
aa.code like '5953%' or
aa.code like '5954%' or
aa.code like '5305%' or
aa.code like '5315%' or
aa.code like '5325%' or
aa.code like '5335%' or
aa.code like '5345%' or
aa.code like '5355%' or
aa.code like '5395%' or
aa.code like '540%' or
aa.code like '541%' or
aa.code like '542%' or
aa.code like '543%' or
aa.code like '545%' or
aa.code like '546%' or
aa.code like '547%' or
aa.code like '548%' or
aa.code like '549%' or
aa.code like '5590%' or
aa.code like '5593%' or
aa.code like '565%' or
aa.code like '566%' or
aa.code like '5945%' or
aa.code like '5955%' or
aa.code like '597%' or
aa.code like '598%' or
aa.code like '480%' or
aa.code like '567%' or
aa.code like '57%'  or
--pbale !
aa.code like '5523%' or
aa.code like '5524%' or
aa.code like '551%' or
aa.code like '5525%'
)
) a
join data d on (date_part('day', d."data"+interval '1 day' )=1 or d.data=CURRENT_DATE)
	and date_trunc('day', d.data)>=a.date_maturity
where d.data<=CURRENT_DATE
group by d.data;
