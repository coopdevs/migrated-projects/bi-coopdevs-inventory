drop foreign table if exists test1_agg_vendes cascade;
create FOREIGN TABLE  test1_agg_vendes("id" int4 NOT NULL
	,"sales_date" date NOT NULL
	,"sales_group" varchar(200) not null
	,"sales_subgroup" varchar(200) NOT NULL
	,"volume" int NOT NULL
	,"unit_price" numeric(9,2) NOT NULL
	,"total_price" numeric(19,2) NOT NULL
)SERVER postgres_fdw_test1 OPTIONS (schema_name 'public', table_name 'agg_vendes');
