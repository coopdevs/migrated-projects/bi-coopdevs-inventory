/*
DROP TABLE IF EXISTS data CASCADE;

create table data( data date, dies_mes smallint);
insert into data (data)
select  cast('2009-12-31' as date) + ((rn||' days')::INTERVAL)
from (
select row_number() over (order by (select 1)) as rn from odoo_contract_contract limit 5000
) a;

 update data set dies_mes=
    DATE_PART('days',
        DATE_TRUNC('month', data.data)
        + '1 MONTH'::INTERVAL
        - '1 DAY'::INTERVAL
    );
*/

DROP VIEW IF EXISTS odoo_altes_socia CASCADE;
CREATE VIEW odoo_altes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS altes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(s.create_date AS date) AS data, count(*) AS altes_socia
FROM odoo_subscription_request s
	LEFT JOIN odoo_res_partner p ON s.partner_id = p.id
WHERE (s.type = 'new' OR s.type = 'subscription')
  AND (s.state = 'done' OR s.state = 'draft' OR s.state = 'paid')
  and s.create_date>='20210101'
GROUP BY CAST(s.create_date AS date);

DROP VIEW IF EXISTS odoo_baixes_socia CASCADE;
CREATE VIEW odoo_baixes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 227
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(effective_date AS date), count(*) AS count
FROM odoo_operation_request r
WHERE r.operation_type = 'sell_back' and effective_date>='20210101'
GROUP BY CAST(effective_date AS date);

DROP VIEW IF EXISTS odoo_altes_netes_socia CASCADE;
CREATE VIEW odoo_altes_netes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS altes_netes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT
CAST(coalesce (altes.create_date, baixes.effective_date) AS date) AS data,
SUM(coalesce(altes.altes,0) -coalesce(baixes.baixes,0)) AS netes
FROM
	(SELECT s.create_date AS create_date, count(*) AS altes
	FROM odoo_subscription_request s
		LEFT JOIN odoo_res_partner p ON s.partner_id = p.id
	WHERE (s.type = 'new' OR s.type = 'subscription')
	  AND (s.state = 'done' OR s.state = 'draft' OR s.state = 'paid') and s.create_date>='20210101'
	GROUP BY s.create_date
) AS altes
FULL JOIN
(
	SELECT o.effective_date AS effective_date, count(*) AS baixes
	FROM odoo_operation_request o
	WHERE o.operation_type = 'sell_back' and effective_date>='20210101'
	GROUP BY o.effective_date
) AS baixes
ON altes.create_date = baixes.effective_date
GROUP BY CAST(coalesce (altes.create_date, baixes.effective_date) AS date);

DROP VIEW IF EXISTS odoo_projeccio_altes_socia CASCADE;
CREATE VIEW odoo_projeccio_altes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
GROUP BY data, dies_mes;



DROP VIEW IF EXISTS odoo_activacions_mobil CASCADE;
CREATE VIEW odoo_activacions_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS activacions_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 672
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_start AS date) AS date_start, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 1
  AND note IS NULL
	and date_start>='20210101'
GROUP BY CAST(date_start AS date);


DROP VIEW IF EXISTS odoo_baixes_mobil CASCADE;
CREATE VIEW odoo_baixes_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 337
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_end AS date) AS date_end, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 1
  AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
  and date_end>='20210101'
GROUP BY CAST(date_end AS date);


DROP VIEW IF EXISTS odoo_projeccio_altes_mobil CASCADE;
CREATE VIEW odoo_projeccio_altes_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 672
GROUP BY data, dies_mes;

DROP VIEW IF EXISTS odoo_activacions_fibra CASCADE;
CREATE VIEW odoo_activacions_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS activacions_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 671
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_start AS date) AS date_start, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 3
  AND note is null
  and date_start>='20210101'
GROUP BY CAST(date_start AS date);


DROP VIEW IF EXISTS odoo_baixes_fibra CASCADE;
CREATE VIEW odoo_baixes_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 392
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_end AS date) AS date_end, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 3
    AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
	and date_end>='20210101'
GROUP BY CAST(date_end AS date) ;


DROP VIEW IF EXISTS odoo_projeccio_altes_fibra CASCADE;
CREATE VIEW odoo_projeccio_altes_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 671
GROUP BY data, dies_mes;



DROP VIEW IF EXISTS odoo_pet_contracte CASCADE;
CREATE VIEW odoo_pet_contracte as
SELECT data, SUM(i.amount)/dies_mes AS pet_contracte
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 670
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
   SELECT cast(l.create_date as date) AS create_date,
   count(*) AS count
FROM odoo_crm_lead_line l
LEFT JOIN odoo_crm_lead c ON l.lead_id = c.id
WHERE (l.ticket_number IS NOT NULL AND (l.ticket_number <> '' OR l.ticket_number IS NULL)
AND c.create_date >='2021-01-01'
    AND l.create_date >= timestamp with time zone '2021-01-11 00:00:00.000Z'
)
GROUP BY cast(l.create_date as date);

DROP VIEW IF EXISTS odoo_indicadors_socies_mobil_fibra CASCADE;
CREATE VIEW odoo_indicadors_socies_mobil_fibra AS
SELECT data, date_part('YEAR', data) as any, date_part('MONTH', data) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
date_part('WEEK', data) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte
FROM (
	SELECT data
	, altes_socia, 0 AS baixes_socia, 0 AS altes_netes_socia, 0 AS projeccio_altes_socia
	, 0 AS activacions_mobil, 0 AS baixes_mobil, 0 AS projeccio_altes_mobil
	, 0 AS activacions_fibra, 0 AS baixes_fibra, 0 AS projeccio_altes_fibra
	, 0 AS pet_contracte
	FROM odoo_altes_socia x
	UNION ALL
	SELECT data, 0, x.baixes_socia,0,0,0,0,0,0,0,0,0
	FROM odoo_baixes_socia x
	UNION ALL
	SELECT data, 0, 0, altes_netes_socia,0,0,0,0,0,0,0,0
	FROM odoo_altes_netes_socia
	UNION ALL
	SELECT data, 0,0,0, x.projeccio_altes_socia,0,0,0,0,0,0,0
	FROM odoo_projeccio_altes_socia x
	UNION ALL
	SELECT data,0,0,0,0, activacions_mobil,0,0,0,0,0,0
	FROM odoo_activacions_mobil
	UNION ALL
	SELECT data ,0,0,0,0,0, baixes_mobil,0,0,0,0,0
	FROM odoo_baixes_mobil
	UNION ALL
	SELECT data, 0,0,0,0,0,0,projeccio_altes_mobil,0,0,0,0
	FROM odoo_projeccio_altes_mobil
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0, activacions_fibra,0,0,0
	FROM odoo_activacions_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0, baixes_fibra,0,0
	FROM odoo_baixes_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0,0, projeccio_altes_fibra,0
	FROM odoo_projeccio_altes_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0,0,0,pet_contracte
	FROM odoo_pet_contracte
) s
GROUP BY data;

DROP VIEW IF EXISTS odoo_indicadors_mes_socies_mobil_fibra CASCADE;
CREATE VIEW odoo_indicadors_mes_socies_mobil_fibra AS
SELECT cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte
	,	date_trunc('year', data)::timestamp without time zone as data_inici_any
	, min(data)::timestamp without time zone as data_inici_mes, data::timestamp without time zone as data
FROM odoo_indicadors_socies_mobil_fibra
GROUP BY date_part('YEAR', data), date_part('MONTH', data),	date_trunc('year', data)::timestamp without time zone, data::timestamp without time zone ;


DROP VIEW IF EXISTS odoo_indicadors_dia_socies_mobil_fibra_no_projeccio CASCADE;
CREATE VIEW odoo_indicadors_dia_socies_mobil_fibra_no_projeccio AS
SELECT data::timestamp without time zone as data,cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
cast(date_part('WEEK', data) as smallint) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra
	, SUM(pet_contracte) as pet_contracte
	,(cast('1900-01-01' as date) + ((date_part('Month',date_trunc('month', data))-1) || ' months')::INTERVAL)::timestamp without time zone as data_normalitzada_mes
 ,(cast('1900-01-01' as date) + ((date_part('week',date_trunc('week', data))-1) || ' week')::INTERVAL)::timestamp without time zone  as data_normalitzada_setmana
FROM odoo_indicadors_socies_mobil_fibra
GROUP BY data::timestamp without time zone, data;


DROP VIEW IF EXISTS odoo_indicadors_mes_socies_mobil_fibra_mes_tancat CASCADE;
CREATE VIEW odoo_indicadors_mes_socies_mobil_fibra_mes_tancat AS
SELECT  cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte,
	date_trunc('year', data)::DATE as data
FROM odoo_indicadors_socies_mobil_fibra
where date_part('Month', data)<=date_part('Month',date_trunc('month', current_date-interval '1 month'))
GROUP BY date_part('YEAR', data), date_part('MONTH', data), date_trunc('year', data) ;

DROP VIEW IF EXISTS odoo_indicadors_dia_socies_mobil_fibra_no_projeccio_mes_tancat CASCADE;
CREATE VIEW odoo_indicadors_dia_socies_mobil_fibra_no_projeccio_mes_tancat AS
SELECT
(cast('1900-01-01' as date) + ((date_part('Month',date_trunc('month', data))-1) || ' months')::INTERVAL)::DATE as data_normalitzada_mes,
(cast('1900-01-01' as date) + ((date_part('week',date_trunc('week', data))-1) || ' week')::INTERVAL)::DATE  as data_normalitzada_setmana,
data, cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
date_part('WEEK', data) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra
	, SUM(pet_contracte) as pet_contracte
FROM odoo_indicadors_socies_mobil_fibra
where date_part('Month', data)<=date_part('Month',date_trunc('month', current_date-interval '1 month'))
GROUP BY data;
